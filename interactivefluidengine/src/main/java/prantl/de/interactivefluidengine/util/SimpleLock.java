package prantl.de.interactivefluidengine.util;

import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

/**
 * Created by Lukas on 29.08.17.
 */

public class SimpleLock implements Lock {
    private boolean isLocked = false;
    @Override
    public synchronized void lock() {

        while (isLocked) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        isLocked = true;
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {

    }

    @Override
    public boolean tryLock() {
        return false;
    }

    @Override
    public boolean tryLock(long l, TimeUnit timeUnit) throws InterruptedException {
        return false;
    }

    @Override
    public synchronized void unlock() {
        isLocked = false;
    }

    @NonNull
    @Override
    public java.util.concurrent.locks.Condition newCondition() {
        return null;
    }
};
