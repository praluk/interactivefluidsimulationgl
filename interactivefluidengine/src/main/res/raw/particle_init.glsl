#version 310 es

#define MAX_TEX_SIZE 1024u
#define SURFACE 0.5

precision mediump float;
precision mediump sampler3D;
precision mediump image2D;
precision mediump image3D;

layout(rgba16f, binding = 0, location = 0) uniform readonly image3D phi;
layout(rgba16f, binding = 1, location = 1) uniform readonly image3D oldPhi;
layout(rgba16f, binding = 2, location = 2) uniform readonly image3D norAndCurv;

layout(rgba16f, binding = 3, location = 3) uniform writeonly image2D particlePos;
layout(rgba16f, binding = 4, location = 4) uniform writeonly image2D particleVel;
layout(rgba16f, binding = 5, location = 5) uniform writeonly image3D oldPhiOut;
layout(binding = 0) uniform atomic_uint bufferHead;

layout(std140, binding = 1) uniform properties
{
    uint particleCount;
    float live_step;
    float gravity;
    float buoyancy;
    float acc_damping;
    float speed_factor;
    float curv_thres;
    float curv_range;
    float energy_thres;
    float energy_range;
    int max_count;
    float max_scale;
};

layout(std140, binding = 2) uniform dyn_properties
{
    ivec3 dim;
    float dt;
    ivec3 boundary;
};


float clampIntensity(float x, float thres, float range) {
	return clamp((x - thres) / range, 0.f, 1.f);
}

float ndimQRndSequence(uint p, uint bits) {
	float res = 0.0;
	uint _p = p;

	while (bits > 0u)
	{
		res += float(bits % p) / float(_p);
		bits /= p;
		_p *= p;
	}
	return res;
}

vec3 hammersley3D(uint i, uint N) {
	return vec3(float(i) / float(N), ndimQRndSequence(2u, i), ndimQRndSequence(3u, i));
}

layout (local_size_x = 4, local_size_y = 4, local_size_z = 4) in;
void main() {
    if( int(gl_GlobalInvocationID.x) < dim.x &&
        int(gl_GlobalInvocationID.y) < dim.y &&
        int(gl_GlobalInvocationID.z) < dim.z ){

        if( int(gl_GlobalInvocationID.x) < boundary.x || int(gl_GlobalInvocationID.x) >= dim.x - boundary.x ||
            int(gl_GlobalInvocationID.y) < boundary.y || int(gl_GlobalInvocationID.y) >= dim.y - boundary.y ||
            int(gl_GlobalInvocationID.z) < boundary.z || int(gl_GlobalInvocationID.z) >= dim.z - boundary.z ){

            return;
        }

        float fluid = imageLoad(phi, ivec3(gl_GlobalInvocationID)).x;

        if(abs(fluid) < SURFACE){
            float step = 1.0 / float(imageSize(phi).y);
            float fluidMag = (imageLoad(oldPhi, ivec3(gl_GlobalInvocationID)).x - fluid) * step / dt;
            if(fluidMag > 0.0){
                vec4 norCurv = imageLoad(norAndCurv, ivec3(gl_GlobalInvocationID));

                uint partCnt = uint(dt * float(max_count) *
                    clampIntensity(norCurv.w, curv_thres, curv_range) *
                    clampIntensity(0.5 * fluidMag * fluidMag, energy_thres, energy_range));

                for(uint i = 0u; i < partCnt; i++){
                    uint idx = atomicCounterIncrement(bufferHead);

                    float scale = max_scale*float(idx%5u)*0.2;

                    vec3 e0 = normalize(cross(vec3(1,0,0), norCurv.xyz));
                    vec3 e1 = normalize(cross(e0, norCurv.xyz));

                    vec3 rnd = hammersley3D(i, partCnt);
                    rnd.xy = 2.0 * rnd.xy - 1.0;

                    if(dot(rnd.xy, rnd.xy) > 1.f) continue;

                    vec3 vel = (e0 * rnd.x + e1 * rnd.y + norCurv.xyz * rnd.z) * fluidMag * speed_factor;
                    vec3 pos = (vec3(gl_GlobalInvocationID) + 0.5) / vec3(dim) - 0.5 + vel * dt;

                    idx = idx % particleCount;

                    ivec2 imgIdx = ivec2(idx % MAX_TEX_SIZE, idx / MAX_TEX_SIZE);
                    imageStore(particlePos, imgIdx, vec4(pos, scale));
                    imageStore(particleVel, imgIdx, vec4(vel, 0));
                }
            }
        }
        imageStore(oldPhiOut, ivec3(gl_GlobalInvocationID), vec4(fluid,0,0,0));
    }
}