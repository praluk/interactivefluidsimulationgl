package fluidsim.de.interactivefluiddrop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.opengl.GLES31;
import android.opengl.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import java.sql.Time;
import java.util.concurrent.CountDownLatch;

import prantl.de.interactivefluidengine.RenderEngine;
import prantl.de.interactivefluidengine.resources.Texture;
import prantl.de.interactivefluidengine.resources.Texture3D;
import prantl.de.interactivefluidengine.resources.Texture4D;
import prantl.de.interactivefluidengine.scene.Camera;
import prantl.de.interactivefluidengine.scene.Component.SolidRenderComponent;
import prantl.de.interactivefluidengine.scene.Component.ParticlePhysicComponent;
import prantl.de.interactivefluidengine.scene.Component.ParticleRenderComponent;
import prantl.de.interactivefluidengine.scene.Component.Transform;
import prantl.de.interactivefluidengine.scene.Component.VolumeRenderComponent;
import prantl.de.interactivefluidengine.scene.Entity;
import prantl.de.interactivefluidengine.util.AccumTimer;
import prantl.de.interactivefluidengine.util.GLMath;
import prantl.de.interactivefluidengine.util.IO;
import prantl.de.interactivefluidengine.util.MeshGenerator;
import prantl.de.interactivefluidengine.util.SimpleLock;
import prantl.de.interactivefluidengine.util.SynchableThread;
import prantl.de.interactivefluidengine.util.TensorFlowHandler;

public class HQDropActivity extends Activity implements RenderEngine.Updateable, View.OnTouchListener {

    private enum SimState {
        START, RUN, IDLE
    }

    // entities of scene
    private Camera mCamera;
    private Transform mCameraTransform;
    private Entity mWater;
    private Entity mParticleSystem;
    private Entity mTouchPlane;
    private Entity mBasin;

    private TensorFlowHandler mTensorflowHandler;

    // resources
    private Texture4D mMainPhi;
    private Texture4D mIdlePhi;
    private Texture3D mRenderPhi;
    private Texture4D[] mDefo = new Texture4D[14];
    private Texture4D mMainDefo;

    // helper textures
    private Texture4D corr0;
    private Texture4D corr1;
    private Texture4D tmp0;
    private Texture4D tmp1;
    private Texture4D nnCorr;

    private Texture3D blend0;
    private Texture3D blend1;
    private Texture3D idleTmp;

    private int mDimT;
    private int mIdleDimT;
    private float mDownScale;

    // UI elements
    private ProgressDialog mProgressDialog;

    private SeekBar mParamSlider0;
    private SeekBar mParamSlider1;
    private SeekBar mParamSlider2;

    private TextView mParamValue0;
    private TextView mParamValue1;
    private TextView mParamValue2;

    private Button mStartButton;

    private Switch mNNSwitch = null;

    private float[] mInitParam = new float[]{ 0.5f, 0.5f, 0.5f };
    private float[] mParameters = new float[3];
    private float mTouchBoundary = 1.f;

    private TextView mFPSLabel;

    // time measurement
    private AccumTimer mFrameTimer = new AccumTimer(500.f);

    private AccumTimer mDefoTimer;
    private AccumTimer mAdvectionTimer;

    private float mAnimationVel = 0.03f;

    // camera settings
    private float mCameraDistance = 2.2f;
    private float mLastTouchX = -1;
    private float mLastTouchY = -1;
    float mDist = 0;
    private float mAzimut = 30;
    private float mInclination = 50;
    private float mRotFactor = 0.14f;

    private SimState mState = SimState.IDLE;
    private boolean mDefoReady = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        mParamSlider0 = (SeekBar) findViewById(R.id.seekBar0);
        mParamSlider1 = (SeekBar) findViewById(R.id.seekBar1);
        mParamSlider2 = (SeekBar) findViewById(R.id.seekBar2);

        mParamSlider0.setProgress((int) (mInitParam[0] * 100));
        mParamSlider1.setProgress((int) (mInitParam[1] * 100));
        mParamSlider2.setProgress((int) (mInitParam[2] * 100));

        mParamSlider0.getThumb().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
        mParamSlider0.getProgressDrawable().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
        mParamSlider1.getThumb().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
        mParamSlider1.getProgressDrawable().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
        mParamSlider2.getThumb().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
        mParamSlider2.getProgressDrawable().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);

        mFPSLabel = (TextView) findViewById(R.id.fps);
        mParamValue0 = (TextView) findViewById(R.id.value0);
        mParamValue1 = (TextView) findViewById(R.id.value1);
        mParamValue2 = (TextView) findViewById(R.id.value2);

        mParamValue0.setText((mParamSlider0.getProgress() / 100.f) + "");
        mParamValue1.setText((mParamSlider1.getProgress() / 100.f) + "");
        mParamValue2.setText((mParamSlider2.getProgress() / 100.f) + "");

        mStartButton = (Button) findViewById(R.id.startButton);

        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDefoReady) {
                    if (mState == SimState.IDLE) {
                        mDefoReady = false;
                        mStartButton.setText(R.string.stop);
                        mParameters[0] = mParamSlider0.getProgress() / 100.f;
                        mParameters[1] = mParamSlider1.getProgress() / 100.f;
                        mParameters[2] = mParamSlider2.getProgress() / 100.f;
                        blendDeformations();
                    } else if (mState == SimState.RUN) {
                        t = mDimT - startBlendFrames;
                        mStartButton.setText(R.string.start);
                    }
                }
            }
        });

        mNNSwitch = (Switch) findViewById(R.id.nnSwitch);

        findViewById(R.id.app_title).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.paper_link)));
                startActivity(browserIntent);
            }
        });

        mParamSlider0.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                mParamValue0.setText((i / 100.f) + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mParamSlider1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                mParamValue1.setText((i / 100.f) + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mParamSlider2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                mParamValue2.setText((i / 100.f) + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setTitle("Loading...");
        mProgressDialog.setMessage("Loading data from SD-Card, please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(mDefo.length + 2);

        // camera contains renderer
        mCamera = (Camera) findViewById(R.id.fullscreen_content);
        mCameraTransform = mCamera.getTransform();

        updateCam();

        mCamera.setSceneUpdateCallback(this);

        mCamera.addLight(new float[]{1, 2, -2}, new float[]{1, 1, 1});

        IO.setContext(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mCamera.onResume();

        mLastTouchX = -1;
        mLastTouchY = -1;
        mHoldDrop = false;

        mStartButton.setText(R.string.start);
        mState = SimState.IDLE;
        mDefoReady = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        mCamera.onPause();
    }

    private void getParameters(final float[] params, final int[] idx){

        idx[0] = params[0] >= 0.5f ? 0 : 1;
        idx[1] = idx[0] * 2 + 2 + (params[1] >= 0.5f ? 0 : 1);
        idx[2] = idx[1] * 2 + 2 + (params[2] >= 0.5f ? 0 : 1);

        params[0] = Math.abs(params[0] * 2.f - 1.f);
        params[1] = Math.abs(params[1] * 2.f - 1.f);
        params[2] = Math.abs(params[2] * 2.f - 1.f);
    }

    private void blendDeformations() {
        mCamera.queueEvent(new Runnable() {
            @Override
            public void run() {

                int[] idx = new int[3];
                float[] params = mParameters.clone();
                boolean useNN = mNNSwitch == null || mNNSwitch.isChecked();

                if(useNN) {
                    mTensorflowHandler.runNeuralNetwork(params);
                    params = mParameters.clone();
                }

                if(RenderEngine.TIME_MEASUREMENT) mDefoTimer.start();
                getParameters(params, idx);

                mDefo[idx[0]].advectCombine(tmp0, mDefo[idx[1]], corr0, params[0], params[1]);
                tmp0.advectCombine(tmp1, mDefo[idx[2]], corr0, corr1, params[2]);

                if(useNN){
                    tmp1.forwardAdvect(tmp0, corr1);
                    tmp0.advectCombine(mMainDefo, nnCorr, 1, 1.f/3.f);
                } else {
                    tmp1.forwardAdvect(mMainDefo, corr1);
                }

                //Texture4D tex = new Texture4D(GLES31.GL_RGBA16F, 50,50,50,50);
                //mMainPhi.advect(tex, mMainDefo, 1.0f);
                //IO.store4DTextureToFile(tex, String.format("hq_drop_%03d_%03d_%03d.uni", (int)(mParameters[0] * 100), (int)(mParameters[1] * 100), (int)(mParameters[2] * 100)), true);
                mState = SimState.START;
                mDefoReady = true;

                if(RenderEngine.TIME_MEASUREMENT) mDefoTimer.stopAndEval(new AccumTimer.OnTimeAccum() {
                    @Override
                    public void timeAccum(float accumTime) {
                        Log.d("Avg Defo Creation Time", accumTime + " ms");
                    }
                });
            }
        });
    }

    private void blendDeformations(final int tRange) {
        SynchableThread synchableThread = new SynchableThread(new CountDownLatch(1)) {
            @Override
            public void _run() {
                if(!mCamera.isEnabled()) return;

                boolean useNN = mNNSwitch == null || mNNSwitch.isChecked();

                int[] idx = new int[3];
                float[] params = mParameters.clone();

                if(useNN) {
                    mTensorflowHandler.runNeuralNetwork(params);
                    params = mParameters.clone();
                }

                getParameters(params, idx);

                mDefo[idx[0]].advectCombine(tmp0, mDefo[idx[1]], corr0, params[0], params[1], 0, tRange);
                tmp0.advectCombine(tmp1, mDefo[idx[2]], corr0, corr1, params[2], 0, tRange);

                if(useNN){
                    tmp1.forwardAdvect(tmp0, corr1, 0, tRange);
                    tmp0.advectCombine(mMainDefo, nnCorr, 1, 0.4f, 0, tRange);
                } else {
                    tmp1.forwardAdvect(mMainDefo, corr1, 0, tRange);
                }

                mState = SimState.START;
            }
        };

        mCamera.queueEvent(synchableThread);

        try {
            synchableThread.GetEndSignal().await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    Thread initThread;
    private class LoadTextureThread extends SynchableThread {
        Texture4D mTex = null;
        String mPath;

        public LoadTextureThread(String path) {
            super(new CountDownLatch(1));
            mPath = path;
        }

        @Override
        public void _run() {
            try{
                mTex = IO.load4DTextureFromFile(mPath + ".uni", true);
            } catch(RuntimeException e){
                e.printStackTrace();
            }
        }

        public Texture4D getTex() {
            return mTex;
        }
    }

    private Texture4D load4DTexture(String path) {
        final LoadTextureThread thread = new LoadTextureThread(path);

        mCamera.post(new Runnable() {
            @Override
            public void run() {
                mCamera.queueEvent(thread);
            }
        });

        try {
            thread.GetEndSignal().await();
        } catch (InterruptedException e) {
            Log.e("HQDropActivity", "Thread interrupted");
            e.printStackTrace();
        }
        Texture4D tex = thread.getTex();
        if(tex == null || initThread != Thread.currentThread()){
            throw new RuntimeException("Error Creating Texture " + path + ".uni");
        }
        return thread.getTex();
    }

    @Override
    public void awake() {
        mCamera.setEnabled(false);

        mCamera.setSkyMap(IO.loadCubeTextureFromFile(new String[]{
                "posx",
                "negx",
                "posy",
                "negy",
                "posz",
                "negz"
        }, false));

        mDefoTimer = new AccumTimer(10, mCamera);
        mAdvectionTimer = new AccumTimer(100, mCamera);

        initThread = new Thread(new Runnable() {
            @Override
            public void run() {
                mCamera.post(new Runnable() {
                    @Override
                    public void run() {
                        mProgressDialog.show();
                        mProgressDialog.setProgress(0);
                    }
                });
                IO.awaitPermission();

                try {
                    mMainPhi = load4DTexture("ref_0_0_0");
                    mProgressDialog.setProgress(1);
                    mIdlePhi = load4DTexture("liq_t12_r060_x050y050z010_idle90");
                    mProgressDialog.setProgress(2);

                    for (int i = 0; i < mDefo.length; i++) {
                        mDefo[i] = load4DTexture(String.format("n_liqdr11_%02d", i));
                        mProgressDialog.setProgress(3 + i);
                    }
                } catch(RuntimeException e){
                    e.printStackTrace();
                    return;
                }

                final Thread actThread = Thread.currentThread();

                mCamera.post(new Runnable() {
                    @Override
                    public void run() {
                        mCamera.queueEvent(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    int[] mainDim = mMainPhi.getDim();
                                    int[] defoDim = mDefo[0].getDim();

                                    mDimT = mainDim[3];
                                    t = mDimT;
                                    mIdleDimT = mIdlePhi.getDim()[3];
                                    mDownScale = (float)defoDim[0] / mainDim[0];

                                    mMainDefo = new Texture4D(GLES31.GL_RGBA16F, defoDim[0], defoDim[1], defoDim[2], defoDim[3]);
                                    mRenderPhi = new Texture3D(GLES31.GL_RGBA16F, mainDim[0], mainDim[1], mainDim[2]);

                                    corr0 = new Texture4D(GLES31.GL_RGBA16F, defoDim[0], defoDim[1], defoDim[2], defoDim[3]);
                                    corr1 = new Texture4D(GLES31.GL_RGBA16F, defoDim[0], defoDim[1], defoDim[2], defoDim[3]);
                                    tmp0 = new Texture4D(GLES31.GL_RGBA16F, defoDim[0], defoDim[1], defoDim[2], defoDim[3]);
                                    tmp1 = new Texture4D(GLES31.GL_RGBA16F, defoDim[0], defoDim[1], defoDim[2], defoDim[3]);

                                    blend0 = new Texture3D(GLES31.GL_RGBA16F, mainDim[0], mainDim[1], mainDim[2]);
                                    blend1 = new Texture3D(GLES31.GL_RGBA16F, mainDim[0], mainDim[1], mainDim[2]);
                                    idleTmp = new Texture3D(GLES31.GL_RGBA16F, mainDim[0], mainDim[1], mainDim[2]);

                                    mRenderPhi.setBoundary(new int[]{1, 1, 1});

                                    mTensorflowHandler = new TensorFlowHandler("4d_defx_freezed_hq_graph.pb", "in_param", new String[]{"out_param", "out_defx"}, new int[]{10, 10, 10, 10});
                                    nnCorr = mTensorflowHandler.getTexture();

                                    VolumeRenderComponent vrc = new VolumeRenderComponent(mRenderPhi, mCamera);
                                    //vrc.setMaterial(new float[]{1, 1, 1, 0.1f}, new float[]{0.2f, 0.1f, 0, 0.9f}, new float[]{1, 1, 1, 0.1f}, new float[]{1, 1, 1, 0.1f}, new float[]{1, 1, 1, 0.02f}, 32);

                                    mWater = new Entity();
                                    mWater.setRenderComponent(vrc);
                                    mWater.getTransform().setPosition(0, 0.25f, 0);

                                    ParticlePhysicComponent pc = new ParticlePhysicComponent(mRenderPhi, vrc.getNorAndCurvHandle(), 10000);
                                    pc.setBoundary(new int[]{2, 2, 2});

                                    ParticleRenderComponent particleRender = new ParticleRenderComponent(pc.getParticlePos(), pc.getParticleVel(), pc.getParticleCount(), mCamera);
                                    //particleRender.setMaterial(new float[]{0.8f, 0.6f, 0.5f, 0.2f}, new float[]{1, 1, 1, 0.1f});

                                    mParticleSystem = new Entity();
                                    mParticleSystem.getTransform().setPosition(0, 0.25f, 0);
                                    mParticleSystem.setPhysicComponent(pc);
                                    mParticleSystem.setRenderComponent(particleRender);

                                    mCamera.setOnTouchListener(HQDropActivity.this);
                                    if(actThread == initThread) mCamera.setEnabled(true);
                                } catch(RuntimeException e){
                                    e.printStackTrace();
                                    return;
                                }
                            }
                        });
                    }
                });

                mProgressDialog.dismiss();
            }
        });
        initThread.start();

        mTouchPlane = new Entity();
        mTouchPlane.getTransform().setPosition(0, 0.25f, 0);
        mTouchPlane.getTransform().setScale(0.5f, 0.5f, 0.5f);

        SolidRenderComponent basinRender = new SolidRenderComponent(IO.loadMeshFromFile("basin_grass", false), mCamera);
        basinRender.setTexture(IO.loadTextureFromFile("grass", false));
        basinRender.setNormalTexture(IO.loadTextureFromFile("normal_grass", false));
        basinRender.setMaterial(new float[]{1, 1, 1, 0.f}, new float[]{1.f, 1.f, 1.f, .85f}, new float[]{1, 1, 1, 0.05f}, new float[]{1, 1, 1, 0.1f}, 32);

        mBasin = new Entity();
        mBasin.setRenderComponent(basinRender);
        mBasin.getTransform().setPosition(0, -0.25f, 0.f);
        mBasin.getTransform().setScale(0.98f, 0.98f, 0.98f);
    }

    public void idleLoop(Texture3D result, float time) {

        time = time % (mIdleDimT - startBlendFrames) + startBlendFrames;
        if (time > mIdleDimT - startBlendFrames) {
            mIdlePhi.getSlice(blend0, time);
            mIdlePhi.getSlice(blend1, time - mIdleDimT + startBlendFrames);
            blend0.blend(result, blend1, 1 - (time - mIdleDimT + startBlendFrames) / startBlendFrames);
        } else {
            mIdlePhi.getSlice(result, time);
        }
    }

    float t;
    float startT = 0;
    float idleT = 0;

    final static float offsetT = 3;
    final static float offsetParticleT = 10;
    final static int startBlendFrames = 20;
    final static int endBlendFrames = 10;
    final static float scaleStep = 0.04f;

    boolean mHoldDrop = false;

    @Override
    public void update(float dt) {

        mFrameTimer.update(dt, new AccumTimer.OnTimeAccum() {
                    @Override
                    public void timeAccum(final float accumTime) {
                        mFPSLabel.post(new Runnable() {
                            @Override
                            public void run() {
                                mFPSLabel.setText((float) (int) ((1000.0 / accumTime) * 10) / 10 + " FPS");
                            }
                        });
                    }
                });

        dt *= mAnimationVel;

        t += dt;

        // t reached point where the state change to idle
        if (t >= mDimT - endBlendFrames && mState == SimState.RUN) {
            startT = 0;
            idleT = 0;
            mState = SimState.IDLE;
            mStartButton.post(new Runnable() {
                @Override
                public void run() {
                    mStartButton.setText(R.string.start);
                }
            });
        }

        switch (mState) {
            case IDLE:
                idleT += dt;
                if (t < mDimT) {
                    idleLoop(idleTmp, idleT);
                    mMainPhi.advect(blend0, mMainDefo, 1, t);
                    blend0.blend(mRenderPhi, idleTmp, (mDimT - t) / endBlendFrames);
                } else {
                    idleLoop(mRenderPhi, idleT);
                }
                break;

            case START:
                t = offsetT;
                startT += dt;
                if (startT < startBlendFrames) {
                    idleT += dt;
                    idleLoop(idleTmp, idleT);
                    mMainPhi.advect(blend0, mMainDefo, 1, t);
                    idleTmp.blend(mRenderPhi, blend0, 1 - startT / startBlendFrames);
                } else {
                    mMainPhi.advect(mRenderPhi, mMainDefo, 1, t);
                    if (mDefoReady && !mHoldDrop) mState = SimState.RUN;
                }
                break;

            case RUN:
                if(RenderEngine.TIME_MEASUREMENT) mAdvectionTimer.start();
                mMainPhi.advect(mRenderPhi, mMainDefo, 1, t);
                if(RenderEngine.TIME_MEASUREMENT) mAdvectionTimer.stopAndTryEval(new AccumTimer.OnTimeAccum() {
                    @Override
                    public void timeAccum(float accumTime) {
                        Log.d("Avg SDF Creation Time", accumTime + " ms");
                    }
                });
                break;
        }

        ((ParticlePhysicComponent) mParticleSystem.getPhysicComponent()).genParticles(t > offsetParticleT);
    }

    boolean convertParameters(float x, float y) {
        // transform touchpoint from camera to touchplane
        float[] vm = new float[16];
        Matrix.multiplyMM(vm, 0, mTouchPlane.getTransform().getWorldToLocal(), 0, mCamera.getTransform().getLocalToWorld(), 0);

        float[] viewPos = new float[4];
        float[] viewDir = new float[4];

        Matrix.multiplyMV(viewPos, 0, vm, 0, new float[]{0, 0, 0, 1}, 0);
        Matrix.multiplyMV(viewDir, 0, vm, 0, new float[]{x, y, -1, 0}, 0);

        viewDir = GLMath.normalize(viewDir);

        float projX = (viewPos[0] + viewDir[0] * viewPos[1] / Math.abs(viewDir[1])) * (mTouchBoundary + 0.5f) + 0.5f;
        float projY = (viewPos[2] + viewDir[2] * viewPos[1] / Math.abs(viewDir[1])) * (mTouchBoundary + 0.5f) + 0.5f;

        if (projX < -mTouchBoundary || projX > mTouchBoundary + 1 || projY < -mTouchBoundary || projY > mTouchBoundary + 1)
            return false;

        if (projX > 1) projX = 1;
        else if (projX < 0) projX = 0;

        if (projY > 1) projY = 1;
        else if (projY < 0) projY = 0;

        mParameters[0] = projX;
        mParameters[1] = projY;

        return true;
    }

    private void updateCam(){
        if (mAzimut < -180) mAzimut += 360;
        else if (mAzimut > 180) mAzimut -= 360;
        if (mInclination < 10) mInclination = 10;
        else if (mInclination > 65-mCamera.getZoom()*20) mInclination = 65-mCamera.getZoom()*20;

        mCameraTransform.setPosition(
                mCameraDistance * (float) Math.sin(Math.toRadians(mInclination)) * (float) Math.sin(Math.toRadians(mAzimut)),
                mCameraDistance * (float) Math.cos(Math.toRadians(mInclination)),
                mCameraDistance * (float) Math.sin(Math.toRadians(mInclination)) * (float) Math.cos(Math.toRadians(mAzimut)));
        mCameraTransform.lookAt(0, 0, 0);
    }

    private float distance(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float)Math.sqrt(x * x + y * y);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if(event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN){
            if (!mHoldDrop) {
                mDist = distance(event);
                if (mDist > 10) mLastTouchX = -1;
                return true;
            }
        }
        if(event.getActionMasked() == MotionEvent.ACTION_POINTER_UP){
            mDist = 0;
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_MOVE) {
            float touchX = event.getX();
            float touchY = event.getY();

            if (mLastTouchX != -1) {
                mAzimut += (mLastTouchX - touchX) * mRotFactor;
                mInclination += (mLastTouchY - touchY) * mRotFactor;

                updateCam();

                mLastTouchX = touchX;
                mLastTouchY = touchY;
            } else if(mDist > 10) {
                float newDist = distance(event);
                if(newDist > 10){
                    float scale = mDist / newDist * mCamera.getZoom();
                    mDist = newDist;
                    if(scale > 1.5f) scale = 1.5f;
                    else if(scale < 0.5f) scale = 0.5f;
                    mCamera.setZoom(scale);

                    updateCam();
                }
            } else {
                convertParameters((touchX - v.getWidth() / 2) / v.getHeight() * 2, -touchY / v.getHeight() * 2 + 1);
            }
            return true;
        }

        if (event.getAction() == MotionEvent.ACTION_UP) {
            mLastTouchX = -1;
            mLastTouchY = -1;

            mHoldDrop = false;
            return true;
        }

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            float touchX = event.getX();
            float touchY = event.getY();

            if(mState == SimState.IDLE){
                if(mDefoReady && convertParameters((touchX - v.getWidth() / 2) / v.getHeight() * 2, -touchY/v.getHeight() * 2 + 1)){
                    mParameters[2] = mParamSlider2.getProgress() / 100.f;

                    mHoldDrop = true;
                    mDefoReady = false;
                    mStartButton.setText(R.string.stop);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            boolean scaleUp = true;
                            for(int i = 0; i < 1000 && mHoldDrop; i++) {
                                blendDeformations((int)(startBlendFrames * mDownScale));
                                if(!scaleUp){
                                    mParameters[2] -= scaleStep;
                                    if (mParameters[2] < 0.f) {
                                        mParameters[2] = 0.f;
                                        scaleUp = true;
                                    }
                                } else {
                                    mParameters[2] += scaleStep;
                                    if (mParameters[2] > 1.f) {
                                        mParameters[2] = 1.f;
                                        scaleUp = false;
                                    }
                                }
                            }
                            mHoldDrop = false;
                            blendDeformations();
                        }
                    }).start();
                    return true;
                }
            }
            mLastTouchX = touchX;
            mLastTouchY = touchY;
            return true;
        }

        return true;
    }

}
