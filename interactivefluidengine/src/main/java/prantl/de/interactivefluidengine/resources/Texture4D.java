package prantl.de.interactivefluidengine.resources;

import android.opengl.GLES31;
import android.util.Log;

import java.nio.FloatBuffer;

import prantl.de.interactivefluidengine.scene.Camera;
import prantl.de.interactivefluidengine.util.GLHelper;
import prantl.de.interactivefluidengine.util.GLMath;
import prantl.de.interactivefluidengine.util.IO;

/**
 * Created by Lukas on 09.06.17.
 */

public class Texture4D extends VolumeTexture {
    int[] mDim = new int[4];
    int[] mArraySize = new int[3];

    public Texture4D(int textureFormat, int x, int y, int z, int t){
        super(textureFormat);
        mDim[0] = x;
        mDim[1] = y;
        mDim[2] = z;
        mDim[3] = t;

        bindTexture();

        // TODO: use real max tex size
        // distribute 4th dimension over all dimensions
        int[] space = new int[]{GLHelper.MAX_TEX_SIZE/mDim[0], GLHelper.MAX_TEX_SIZE/mDim[1], GLHelper.MAX_TEX_SIZE/mDim[2]};
        mArraySize[1] = 1;
        mArraySize[2] = 1;
        if(mDim[3] > space[0]){
            mArraySize[0] = space[0];
            if(mDim[3] > space[0] * space[1]){
                mArraySize[1] = space[1];
                if(mDim[3] > space[0] * space[1] * space[2]){
                    Log.e("texture4D", "4D texture file to big!");
                    return;
                }
                mArraySize[2] = GLMath.roundUp(mDim[3], (space[0] * space[1]));
            } else {
                mArraySize[1] = GLMath.roundUp(mDim[3], space[0]);
            }
        } else {
            mArraySize[0] = mDim[3];
        }

        // allocate texture storage
        GLES31.glTexStorage3D(GLES31.GL_TEXTURE_3D, 1, mTextureFormat, mDim[0] * mArraySize[0], mDim[1] * mArraySize[1], mDim[2] * mArraySize[2]);
        GLHelper.CheckGLError();

        unbindTexture();
    }

    public final void fillGrid(float x){
        bindTexture();
        if(isPhiGrid()) {
            FloatBuffer fb = FloatBuffer.allocate(mDim[0] * mDim[1] * mDim[2]);
            for(int i = 0; i < mDim[0] * mDim[1] * mDim[2]; i++) {
                fb.put(x);
            }
            for(int i = 0; i < mDim[3]; i++){
                fb.rewind();
                final int[] off = getIntegralOffset(i);
                GLES31.glTexSubImage3D(GLES31.GL_TEXTURE_3D, 0, off[0], off[1], off[2], mDim[0], mDim[1], mDim[2], GLES31.GL_RED, GLES31.GL_FLOAT, fb);
            }
            GLHelper.CheckGLError();
        } else {
            Log.e("fill grid", "wrong texture!");
        }
        unbindTexture();
    }

    public final void fillGrid(float x, float y, float z, float w){
        bindTexture();
        if(isVecGrid()) {
            FloatBuffer fb = FloatBuffer.allocate(mDim[0] * mDim[1] * mDim[2] * 4);
            for(int i = 0; i < mDim[0] * mDim[1] * mDim[2]; i++) {
                fb.put(x);
                fb.put(y);
                fb.put(z);
                fb.put(w);
            }
            for(int i = 0; i < mDim[3]; i++){
                fb.rewind();
                final int[] off = getIntegralOffset(i);
                GLES31.glTexSubImage3D(GLES31.GL_TEXTURE_3D, 0, off[0], off[1], off[2], mDim[0], mDim[1], mDim[2], GLES31.GL_RGBA, GLES31.GL_FLOAT, fb);
            }
            GLHelper.CheckGLError();
        } else {
            Log.e("fill grid", "wrong texture!");
        }
        unbindTexture();
    }

    @Override
    public final int[] getArraySize(){
        return mArraySize;
    }

    @Override
    public int[] getBoundary() {
        return mBoundary;
    }

    @Override
    public void setBoundary(int[] boundary) {
        mBoundary = boundary;
    }

    @Override
    public int[] getTextureSize() {
        int[] texSize = new int[3];
        for(int i = 0; i < 3; i++) texSize[i] = mDim[i] * mArraySize[i];
        return texSize;
    }

    @Override
    public float[] getOffset(final int t){
        float[] offset = new float[3];
        offset[0] = (float)(t % mArraySize[0]) / mArraySize[0];
        offset[1] = (float)(t / mArraySize[0] % mArraySize[1]) / mArraySize[1];
        offset[2] = (float)(t / (mArraySize[0] * mArraySize[1]) % mArraySize[2]) / mArraySize[2];
        return offset;
    }

    public int[] getIntegralOffset(final int t){
        int[] offset = new int[3];
        offset[0] = (t % mArraySize[0]) * mDim[0];
        offset[1] = (t / mArraySize[0] % mArraySize[1]) * mDim[1];
        offset[2] = (t / (mArraySize[0] * mArraySize[1]) % mArraySize[2]) * mDim[2];
        return offset;
    }

    public final void getSlice(Texture3D result, float timestep){
        m3DSliceProgramHandle.useProgram();

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[TEXTURE_LOC.bind]);
        bindTexture();

        GLES31.glUniform1i(TEXTURE_LOC.handle, TEXTURE_LOC.bind);

        GLES31.glBindImageTexture(RESULT_LOC.bind, result.mTextureHandle, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);
        GLHelper.CheckGLError();

        int[] dim = result.getDim();
        setPropBuffer(0, timestep, 0, dim, 0, result.mBoundary, 0, 0);
        GLHelper.CheckGLError();

        GLES31.glDispatchCompute(GLMath.roundUp(dim[0], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[1], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[2], GLHelper.DISPATCH_GROUP_SIZE));

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[TEXTURE_LOC.bind]);
        unbindTexture();

        GLES31.glBindImageTexture(RESULT_LOC.bind, 0, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);

        GLES31.glMemoryBarrier(GLES31.GL_COMPUTE_SHADER_BIT);
        GLHelper.CheckGLError();
    }

    @Override
    public int[] getDim() {
        return mDim;
    }

    @Override
    public void bindTexture() {
        GLES31.glBindTexture(GLES31.GL_TEXTURE_3D, mTextureHandle);
    }

    @Override
    public void unbindTexture() {
        GLES31.glBindTexture(GLES31.GL_TEXTURE_3D, 0);
    }
}
