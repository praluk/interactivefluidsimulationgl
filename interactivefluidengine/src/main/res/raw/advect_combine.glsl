#version 310 es

precision mediump float;
precision mediump image3D;
precision mediump sampler3D;

layout(binding = 0, location = 0) uniform sampler3D phi;
layout(binding = 1, location = 1) uniform sampler3D defo;
layout(rgba16f, binding = 0, location = 3) uniform writeonly image3D res;
layout(rgba16f, binding = 1, location = 4) uniform writeonly image3D corrOut;

layout(std140, binding = 1) uniform properties
{
    float alpha;
    float t_off0;
    float t_off1;
    float ratio;
    ivec4 dim;
    ivec4 boundary;
    float fac0;
    float fac1;
};
vec3 getOffset(int time, ivec3 aSize){
    vec3 off;
    off.x = float(int(time) % aSize.x);
    off.y = float(int(time) / aSize.x % aSize.y);
    off.z = float(int(time) / (aSize.x * aSize.y) % aSize.z);
    return off;
}

// helper for sampling texture, aSize is the array size of the 3D array
vec4 sampleTexture(sampler3D tex, vec4 pos, ivec3 aSize){
    vec3 offset = getOffset(int(pos.w), aSize) / vec3(aSize);
    vec3 nextOffset = getOffset(int(pos.w) + 1, aSize) / vec3(aSize);
    return texture(tex, pos.xyz + offset) * (1.0 - fract(pos.w)) + texture(tex, pos.xyz + nextOffset) * fract(pos.w);
}

layout (local_size_x = 4, local_size_y = 4, local_size_z = 4) in;
void main() {
    ivec4 index;
    index.x = int(gl_GlobalInvocationID.x) % dim.x;
    index.y = int(gl_GlobalInvocationID.y);
    index.z = int(gl_GlobalInvocationID.z);
    index.w = int(gl_GlobalInvocationID.x) / dim.x;
    if( index.x < dim.x &&
        index.y < dim.y &&
        index.z < dim.z &&
        index.w < dim.w ){

        ivec3 resTexSize = imageSize(res);
        if( index.x < boundary.x || index.x >= dim.x - boundary.x ||
            index.y < boundary.y || index.y >= dim.y - boundary.y ||
            index.z < boundary.z || index.z >= dim.z - boundary.z ||
            index.w < boundary.w || index.w >= dim.w - boundary.w ){

            imageStore(res, index.xyz + ivec3(getOffset(index.w, resTexSize / dim.xyz)) * dim.xyz, vec4(0));
            return;
        }
        ivec3 phiTexSize = textureSize(phi, 0);
        ivec3 defoTexSize = textureSize(defo, 0);

        // timestep without 0.5 offset, because time sliced => values are on the cell points of the 4th dimension
        float timestep = float(index.w) + t_off0;

        // deformation vector setup
        // normalized 3d tex position in slice (centered), 0.5 offset because values are in the cell center
        vec3 pos = clamp((vec3(index.xyz) + 0.5) * ratio, vec3(0.5), vec3(dim.xyz) * ratio - 0.5) / vec3(defoTexSize);

        vec4 v = sampleTexture(defo, vec4(pos, timestep * ratio), defoTexSize / ivec3(vec3(dim.xyz) * ratio)) / ratio;

        // phi vector setup
        // normalized 3d position in slice (centered)
        pos = clamp(vec3(index.xyz) + 0.5 - v.xyz * alpha, vec3(0.5), vec3(dim.xyz) - 0.5) / vec3(phiTexSize);
        // reconstruction of new time
        timestep = clamp(timestep - v.w * alpha, 0.0, float(dim.w - 1));

        // sampling tex + corr values
        vec4 adv_v = sampleTexture(phi, vec4(pos, timestep), phiTexSize / dim.xyz);

        // saving combined values into res and corr
        imageStore(res, index.xyz + ivec3(getOffset(index.w, resTexSize / dim.xyz)) * dim.xyz, adv_v * fac0 + v * fac1);
        imageStore(corrOut, index.xyz + ivec3(getOffset(index.w, resTexSize / dim.xyz)) * dim.xyz, adv_v * (1.0 - fac0) + v * (1.0 - fac1));
    }
}