package prantl.de.interactivefluidengine.scene.Component;

import android.opengl.GLES31;
import android.opengl.Matrix;
import android.util.Pair;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import prantl.de.interactivefluidengine.RenderEngine;
import prantl.de.interactivefluidengine.resources.Mesh;
import prantl.de.interactivefluidengine.resources.Shader;
import prantl.de.interactivefluidengine.resources.Texture;
import prantl.de.interactivefluidengine.resources.Texture3D;
import prantl.de.interactivefluidengine.resources.Texture4D;
import prantl.de.interactivefluidengine.resources.VolumeTexture;
import prantl.de.interactivefluidengine.scene.Camera;
import prantl.de.interactivefluidengine.util.GLHelper;
import prantl.de.interactivefluidengine.util.GLMath;
import prantl.de.interactivefluidengine.util.IO;
import prantl.de.interactivefluidengine.util.Location;
import prantl.de.interactivefluidengine.util.MeshGenerator;

/**
 * Created by Lukas on 30.05.17.
 */

public class VolumeRenderComponent extends IRenderComponent {

    // static uniform positions
    private static final int MV_HANDLE = 2;
    private static final int CAM_POS_HANDLE = 3;

    private static final Location VOLUME_LOC = new Location(0, 0);
    private static final Location NOR_CURV_LOC = new Location(1, 1);
    private static final Location GRADIENT_LOC = new Location(1, 1);
    private static final Location MATERIAL_BUFFER_LOC = new Location(1, 1);
    private static final Location PROP_BUFFER_LOC = new Location(3, 2);

    private static final Location DEST_IMAGE_LOC = new Location(2, 0);

    // texture containing sdf data
    private final VolumeTexture mVolume;
    private final Texture3D mGradient;
    private final Texture3D mNormalAndCurvature;

    private static Mesh mDomain;

    private static RenderTargetHandle mRenderTargetHandle;
    private static int mRenderTargetDisplace = 0;

    private static Shader mGradientProg;
    private static Shader mCurvatureProg;
    private static Shader mRaytracerProg;

    private final int mMaterial;
    private final int mPropBuffer;

    private static final int MATERIAL_ELEM_SIZE = 24;
    private static final int MATERIAL_BYTE_SIZE = 96;

    private static final int PROP_BYTE_SIZE = 96;

    private static Vector<IRenderComponent> mRenderComponents = new Vector<>();

    public VolumeRenderComponent(final VolumeTexture volumeTexture, final Camera camera){
        super(camera);

        mVolume = volumeTexture;

        int[] dim = mVolume.getDim();

        // TODO: use some kind of clone function (for resources/textures)
        mNormalAndCurvature = new Texture3D(GLES31.GL_RGBA16F, dim[0], dim[1], dim[2]);
        mGradient = new Texture3D(GLES31.GL_RGBA16F, dim[0], dim[1], dim[2]);

        final int[] ubo = new int[2];
        GLES31.glGenBuffers(2, ubo, 0);
        mMaterial = ubo[0];
        mPropBuffer = ubo[1];

        setMaterial(new float[]{1, 1, 1, 0.5f}, new float[]{0, 0, 0, 0}, new float[]{1, 1, 1, 0.2f}, new float[]{1, 1, 1, 0.1f}, new float[]{1, 1, 1, 0.02f}, 32, 50.f);
    }

    public static void init(){
        ArrayList<Pair<Integer, String>> shaders = new ArrayList<>();
        shaders.add(Pair.create(GLES31.GL_VERTEX_SHADER, "raytracer_vert"));
        shaders.add(Pair.create(GLES31.GL_FRAGMENT_SHADER, "raytracer_frag"));
        mRaytracerProg = IO.loadShaderFromFile(shaders, false);

        shaders.clear();
        shaders.add(Pair.create(GLES31.GL_COMPUTE_SHADER, "gradient"));
        mGradientProg = IO.loadShaderFromFile(shaders, false);

        shaders.clear();
        shaders.add(Pair.create(GLES31.GL_COMPUTE_SHADER, "curvature"));
        mCurvatureProg = IO.loadShaderFromFile(shaders, false);

        mDomain = MeshGenerator.GetCubeMesh();
        mRenderTargetHandle = new RenderTargetHandle();

        mRenderComponents.clear();
    }

    public Texture3D getNorAndCurvHandle(){
        return mNormalAndCurvature;
    }

    public static int getRendererColor(){
        return mRenderTargetHandle.mRenderTargetColor;
    }

    public static int getRendererDepth(){
        return mRenderTargetHandle.mRenderTargetDepth;
    }

    public static int getRendererDisplace() { return mRenderTargetDisplace; }

    public static void resize(int width, int height){
        mRenderTargetHandle.resize(width, height);

        final int[] rtv = new int[1];
        if(mRenderTargetDisplace > 0){
            rtv[0] = mRenderTargetDisplace;
            GLES31.glDeleteFramebuffers(1, rtv, 0);
        }

        GLES31.glBindFramebuffer(GLES31.GL_FRAMEBUFFER, mRenderTargetHandle.mRenderTarget);

        GLES31.glGenTextures(1, rtv, 0);
        mRenderTargetDisplace = rtv[0];
        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, mRenderTargetDisplace);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_MAG_FILTER, GLES31.GL_NEAREST);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_MIN_FILTER, GLES31.GL_NEAREST);
        GLES31.glTexStorage2D(GLES31.GL_TEXTURE_2D, 1, GLES31.GL_RGBA8, width, height);

        GLES31.glFramebufferTexture2D(GLES31.GL_FRAMEBUFFER, GLES31.GL_COLOR_ATTACHMENT1, GLES31.GL_TEXTURE_2D, mRenderTargetDisplace, 0);

        GLHelper.CheckGLError();

        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, 0);
        GLES31.glBindFramebuffer(GLES31.GL_FRAMEBUFFER, 0);
    }

    float mT = 0;

    public void setT(float t){
        mT = t;
    }

    protected void setPropBuffer(float[] off, float[] nextOff, float offFrac, int[] arraySize, int[] dim){
        ByteBuffer init = ByteBuffer.allocate(PROP_BYTE_SIZE).order(ByteOrder.nativeOrder());
        for(int i = 0; i < 3; i++) init.putFloat(off[i]);
        init.putFloat(0);
        for(int i = 0; i < 3; i++) init.putFloat(nextOff[i]);
        init.putFloat(offFrac);
        for(int i = 0; i < 3; i++) init.putFloat(1.f/arraySize[i]);
        init.putFloat(0);
        for(int i = 0; i < 3; i++) init.putFloat(1.f/dim[i]);
        init.putFloat(0);
        for(int i = 0; i < 3; i++) init.putInt(dim[i]);
        init.putFloat(0);
        init.rewind();

        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, mPropBuffer);
        GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, PROP_BUFFER_LOC.bind, mPropBuffer);
        GLES31.glBufferData(GLES31.GL_UNIFORM_BUFFER, PROP_BYTE_SIZE, init, GLES31.GL_DYNAMIC_DRAW);
        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, 0);

        GLHelper.CheckGLError();
    }

    public void setMaterial(float[] reflCol, float[] diffCol, float[] specCol, float[] ambientCol, float[] curvCol, int shinniness, float maxCurv){
        ByteBuffer init = ByteBuffer.allocate(MATERIAL_BYTE_SIZE).order(ByteOrder.nativeOrder());
        for(int i = 0; i < 4; i++) init.putFloat(reflCol[i]);
        for(int i = 0; i < 4; i++) init.putFloat(diffCol[i]);
        for(int i = 0; i < 4; i++) init.putFloat(specCol[i]);
        for(int i = 0; i < 4; i++) init.putFloat(ambientCol[i]);
        for(int i = 0; i < 4; i++) init.putFloat(curvCol[i]);
        init.putInt(shinniness);
        init.putFloat(maxCurv);
        init.rewind();

        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, mMaterial);
        GLES31.glBufferData(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BYTE_SIZE, init, GLES31.GL_DYNAMIC_DRAW);
        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, 0);
    }

    public static void bindFramebuffer(){
        GLES31.glBindFramebuffer(GLES31.GL_FRAMEBUFFER, mRenderTargetHandle.mRenderTarget);
        GLES31.glDrawBuffers(2, new int[]{GLES31.GL_COLOR_ATTACHMENT0, GLES31.GL_COLOR_ATTACHMENT1}, 0);
        GLES31.glClear(GLES31.GL_DEPTH_BUFFER_BIT | GLES31.GL_COLOR_BUFFER_BIT);
    }

    public static void drawAll(){

        for(IRenderComponent vrc : mRenderComponents){
            VolumeRenderComponent v = (VolumeRenderComponent) vrc;
            if(v.isEnabled()) {
                v.setPropBuffer(v.mVolume.getOffset((int)v.mT), v.mVolume.getOffset((int)v.mT+1), v.mT - (int)v.mT, v.mVolume.getArraySize(), v.mVolume.getDim());
                GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BUFFER_LOC.bind, v.mMaterial);

                mGradientProg.useProgram();
                v.calcGradient();

                mCurvatureProg.useProgram();
                v.calcNorAndCurv();

                mRaytracerProg.useProgram();
                GLES31.glUniform1i(RenderEngine.SKY_MAP_LOC.handle, RenderEngine.SKY_MAP_LOC.bind);
                v.draw();

                GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BUFFER_LOC.bind, 0);
                GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, PROP_BUFFER_LOC.bind, 0);
            }
        }
        /*
        mGradientProg.useProgram();
        for(IRenderComponent vrc : mRenderComponents){
            VolumeRenderComponent v = (VolumeRenderComponent) vrc;
            if(vrc.isEnabled()) {
                v.calcGradient();
            }
        }

        mCurvatureProg.useProgram();
        for(IRenderComponent vrc : mRenderComponents){
            if(vrc.isEnabled()) {
                ((VolumeRenderComponent) vrc).calcNorAndCurv();
            }
        }

        mRaytracerProg.useProgram();

        GLES31.glUniform1i(RenderEngine.SKY_MAP_LOC.handle, RenderEngine.SKY_MAP_LOC.bind);
        GLHelper.CheckGLError();

        //GLES31.glBindFramebuffer(GLES31.GL_FRAMEBUFFER, mRenderTargetHandle.mRenderTarget);
        //GLES31.glDrawBuffers(2, new int[]{GLES31.GL_COLOR_ATTACHMENT0, GLES31.GL_COLOR_ATTACHMENT1}, 0);
        //GLES31.glClear(GLES31.GL_DEPTH_BUFFER_BIT | GLES31.GL_COLOR_BUFFER_BIT);
        for(IRenderComponent vrc : mRenderComponents){
            if(vrc.isEnabled()) {
                vrc.draw();
            }
        }
        //GLES31.glBindFramebuffer(GLES31.GL_FRAMEBUFFER, 0);*/
    }

    private void calcGradient(){

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[VOLUME_LOC.bind]);
        mVolume.bindTexture();
        GLES31.glUniform1i(VOLUME_LOC.handle, VOLUME_LOC.bind);

        GLES31.glBindImageTexture(DEST_IMAGE_LOC.bind, mGradient.getTextureHandle(), 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);
        GLHelper.CheckGLError();

        int[] dim = mVolume.getDim();
        //setPropBuffer(mVolume.getOffset((int)mT), mVolume.getOffset((int)mT+1), mT - (int)mT, mVolume.getArraySize(), dim);

        GLES31.glDispatchCompute(GLMath.roundUp(dim[0], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[1], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[2], GLHelper.DISPATCH_GROUP_SIZE));

        mVolume.unbindTexture();
        GLES31.glBindImageTexture(DEST_IMAGE_LOC.bind, 0, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);
        //GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BUFFER_LOC.bind, 0);
        //GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, PROP_BUFFER_LOC.bind, 0);
    }

    private void calcNorAndCurv(){

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[VOLUME_LOC.bind]);
        mVolume.bindTexture();
        GLES31.glUniform1i(VOLUME_LOC.handle, VOLUME_LOC.bind);

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[GRADIENT_LOC.bind]);
        mGradient.bindTexture();
        GLES31.glUniform1i(GRADIENT_LOC.handle, GRADIENT_LOC.bind);

        GLES31.glBindImageTexture(DEST_IMAGE_LOC.bind, mNormalAndCurvature.getTextureHandle(), 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);

        int[] dim = mVolume.getDim();
        //setPropBuffer(mVolume.getOffset((int)mT), mVolume.getOffset((int)mT+1), mT - (int)mT, mVolume.getArraySize(), dim);

        //GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BUFFER_LOC.bind, mMaterial);

        GLES31.glDispatchCompute(GLMath.roundUp(dim[0], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[1], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[2], GLHelper.DISPATCH_GROUP_SIZE));

        mVolume.unbindTexture();
        mGradient.unbindTexture();
        GLES31.glBindImageTexture(DEST_IMAGE_LOC.bind, 0, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);
        //GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BUFFER_LOC.bind, 0);
        //GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, PROP_BUFFER_LOC.bind, 0);
    }

    @Override
    protected void draw(){

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[VOLUME_LOC.bind]);
        mVolume.bindTexture();
        GLES31.glUniform1i(VOLUME_LOC.handle, VOLUME_LOC.bind);

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[NOR_CURV_LOC.bind]);
        mNormalAndCurvature.bindTexture();
        GLES31.glUniform1i(NOR_CURV_LOC.handle, NOR_CURV_LOC.bind);

        float[] mm = new float[16];
        Matrix.multiplyMM(mm, 0, mCamera.getTransform().getWorldToLocal(), 0, mEntity.getTransform().getLocalToWorld(), 0);
        GLES31.glUniformMatrix4fv(MV_HANDLE, 1, false, mm, 0);

        Matrix.multiplyMM(mm, 0, mEntity.getTransform().getWorldToLocal(), 0, mCamera.getTransform().getLocalToWorld(), 0);
        float[] camPos = new float[4];
        Matrix.multiplyMV(camPos, 0, mm, 0, new float[]{0, 0, 0, 1}, 0);

        GLES31.glUniform3f(CAM_POS_HANDLE, camPos[0], camPos[1], camPos[2]);

        //setPropBuffer(mVolume.getOffset((int)mT), mVolume.getOffset((int)mT+1), mT - (int)mT, mVolume.getArraySize(), dim);

        //GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BUFFER_LOC.bind, mMaterial);

        GLHelper.CheckGLError();

        mDomain.draw();

        mVolume.unbindTexture();
        mNormalAndCurvature.unbindTexture();
        //GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BUFFER_LOC.bind, 0);
        //GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, PROP_BUFFER_LOC.bind, 0);
    }

    @Override
    protected Vector<IRenderComponent> getIRenderComponentVector() {
        return mRenderComponents;
    }
}
