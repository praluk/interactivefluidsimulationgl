package fluidsim.de.interactivefluiddrop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.opengl.GLES31;
import android.opengl.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Map;
import java.util.concurrent.CountDownLatch;

import prantl.de.interactivefluidengine.PhysicEngine;
import prantl.de.interactivefluidengine.RenderEngine;
import prantl.de.interactivefluidengine.resources.Mesh;
import prantl.de.interactivefluidengine.resources.Texture;
import prantl.de.interactivefluidengine.resources.Texture3D;
import prantl.de.interactivefluidengine.resources.Texture4D;
import prantl.de.interactivefluidengine.scene.Camera;
import prantl.de.interactivefluidengine.scene.Component.SolidRenderComponent;
import prantl.de.interactivefluidengine.scene.Component.ParticlePhysicComponent;
import prantl.de.interactivefluidengine.scene.Component.ParticleRenderComponent;
import prantl.de.interactivefluidengine.scene.Component.Transform;
import prantl.de.interactivefluidengine.scene.Component.VolumeRenderComponent;
import prantl.de.interactivefluidengine.scene.Entity;
import prantl.de.interactivefluidengine.util.AccumTimer;
import prantl.de.interactivefluidengine.util.GLMath;
import prantl.de.interactivefluidengine.util.IO;
import prantl.de.interactivefluidengine.util.MeshGenerator;
import prantl.de.interactivefluidengine.util.SimpleLock;
import prantl.de.interactivefluidengine.util.SynchableThread;
import prantl.de.interactivefluidengine.util.TensorFlowHandler;

public class WaterfallActivity extends Activity implements RenderEngine.Updateable, View.OnTouchListener {

    private enum SimState {
        RUN, PAUSE
    }

    // entities of scene
    private Camera mCamera;
    private Transform mCameraTransform;
    private Entity mWater;
    private Entity mParticleSystem;
    private ParticlePhysicComponent mParticlePhysic;
    private Entity mFloor;
    private Entity mMainBase;
    private Entity mCornerBase;
    private Entity mWall;
    private Entity mBaseWall;
    private Entity mInArrow;
    private Entity mOutArrow;

    private TensorFlowHandler mTensorflowHandler;

    // resources
    private Texture4D mMainPhi;
    private Texture3D mRenderPhi;
    private Texture4D[] mDefo = new Texture4D[3];
    private Texture4D mMainDefo;

    // helper textures
    private Texture4D corr0;
    private Texture4D corr1;
    private Texture4D tmp0;
    private Texture4D tmp1;
    private Texture4D nnCorr;

    private Texture3D blend0;
    private Texture3D blend1;

    private int mDimT;

    // UI elements
    private ProgressDialog mProgressDialog;

    private SeekBar mParamSlider0;
    private SeekBar mParamSlider1;
    private SeekBar mParamSlider2;

    private TextView mParamValue0;
    private TextView mParamValue1;
    private TextView mParamValue2;

    private Button mStartButton;

    private Switch mNNSwitch = null;

    private float[] mParameters = new float[]{0.5f, 0.5f, 0.5f};

    private TextView mFPSLabel;

    // time measurement
    private AccumTimer mFrameTimer = new AccumTimer(500.f);

    private AccumTimer mDefoTimer;
    private AccumTimer mAdvectionTimer;

    // camera settings
    private float mCameraDistance = 2.2f;
    private float mLastTouchX = -1;
    private float mLastTouchY = -1;
    float mDist = 0;
    private float mAzimut = 120;
    private float mInclination = 50;
    private float mRotFactor = 0.14f;

    private SimState mState = SimState.RUN;

    private SimpleLock defoLock = new SimpleLock();
    private boolean isCalcingDefo = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_waterfall);

        mParamSlider0 = (SeekBar) findViewById(R.id.seekBar0);
        mParamSlider1 = (SeekBar) findViewById(R.id.seekBar1);
        mParamSlider2 = (SeekBar) findViewById(R.id.seekBar2);

        mParamSlider0.setProgress((int) (mParameters[0] * 100));
        mParamSlider1.setProgress((int) (mParameters[1] * 100));
        mParamSlider2.setProgress((int) (mParameters[2] * 100));

        mParamSlider0.getThumb().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
        mParamSlider0.getProgressDrawable().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
        mParamSlider1.getThumb().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
        mParamSlider1.getProgressDrawable().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
        mParamSlider2.getThumb().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
        mParamSlider2.getProgressDrawable().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);

        mFPSLabel = (TextView) findViewById(R.id.fps);
        mParamValue0 = (TextView) findViewById(R.id.value0);
        mParamValue1 = (TextView) findViewById(R.id.value1);
        mParamValue2 = (TextView) findViewById(R.id.value2);

        mParamValue0.setText((mParamSlider0.getProgress() / 100.f) + "");
        mParamValue1.setText((mParamSlider1.getProgress() / 100.f) + "");
        mParamValue2.setText((mParamSlider2.getProgress() / 100.f) + "");

        mStartButton = (Button) findViewById(R.id.startButton);

        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mState == SimState.PAUSE) {
                    mState = SimState.RUN;
                    mStartButton.setText(R.string.pause);
                } else if (mState == SimState.RUN) {
                    mState = SimState.PAUSE;
                    mStartButton.setText(R.string.start);
                }
            }
        });

        mNNSwitch = (Switch) findViewById(R.id.nnSwitch);

        if(mNNSwitch != null) {
            mNNSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    launchBlendDeformation();
                }
            });
        }

        findViewById(R.id.app_title).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.paper_link)));
                startActivity(browserIntent);
            }
        });

        mParamSlider0.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                mParamValue0.setText((i / 100.f) + "");
                mParameters[0] = seekBar.getProgress() / 100.f;
                launchBlendDeformation();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                forcedLaunchBlendDeformation();
            }
        });
        mParamSlider1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                mParamValue1.setText((i / 100.f) + "");
                mParameters[1] = 1 - seekBar.getProgress() / 100.f;
                launchBlendDeformation();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                forcedLaunchBlendDeformation();
            }
        });
        mParamSlider2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                mParamValue2.setText((i / 100.f) + "");
                mParameters[2] = seekBar.getProgress() / 100.f;
                launchBlendDeformation();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                forcedLaunchBlendDeformation();
            }
        });

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setTitle("Loading...");
        mProgressDialog.setMessage("Loading data from SD-Card, please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(mDefo.length + 1);

        // camera contains renderer
        mCamera = (Camera) findViewById(R.id.fullscreen_content);
        mCameraTransform = mCamera.getTransform();

        updateCam();

        mCamera.setSceneUpdateCallback(this);

        mCamera.addLight(new float[]{1, 2, -2}, new float[]{1, 1, 1});

        IO.setContext(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mCamera.onResume();

        mLastTouchX = -1;
        mLastTouchY = -1;

        mStartButton.setText(R.string.pause);
        mState = SimState.RUN;
        isCalcingDefo = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        mCamera.onPause();
    }

    private void blendDeformations() {
        mCamera.queueEvent(new Runnable() {
            @Override
            public void run() {

                final float[] params = mParameters.clone();
                boolean useNN = mNNSwitch == null || mNNSwitch.isChecked();

                updateBases(params);

                if(useNN) {
                    mTensorflowHandler.runNeuralNetwork(params);
                }

                if(RenderEngine.TIME_MEASUREMENT) mDefoTimer.start();

                mDefo[0].advectCombine(tmp0, mDefo[1], corr0, params[0], params[1]);
                tmp0.advectCombine(tmp1, mDefo[2], corr0, corr1, params[2]);

                if(useNN){
                    tmp1.forwardAdvect(tmp0, corr1);
                    tmp0.advectCombine(mMainDefo, nnCorr, 1, 5.f/12.f);
                } else {
                    tmp1.forwardAdvect(mMainDefo, corr1);
                }

                defoLock.lock();
                isCalcingDefo = false;
                defoLock.unlock();

                if(RenderEngine.TIME_MEASUREMENT) mDefoTimer.stopAndTryEval(new AccumTimer.OnTimeAccum() {
                    @Override
                    public void timeAccum(float accumTime) {
                        Log.d("Avg Defo Creation Time", accumTime + " ms");
                    }
                });
            }
        });
    }

    private void launchBlendDeformation()
    {
        defoLock.lock();
        if(!isCalcingDefo) {
            defoLock.unlock();
            isCalcingDefo = true;
            blendDeformations();
        }
        defoLock.unlock();
    }

    private void forcedLaunchBlendDeformation()
    {
        new Thread(new Runnable() {
            public void run() {

                defoLock.lock();
                while(isCalcingDefo)
                {
                    defoLock.unlock();
                    if(mState == SimState.PAUSE) return;
                    try {
                        Thread.sleep(10);
                    } catch(InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    defoLock.lock();
                }
                isCalcingDefo = true;
                defoLock.unlock();

                blendDeformations();
            }
        }).start();
    }

    private class LoadTextureThread extends SynchableThread {
        Texture4D mTex = null;
        String mPath;

        public LoadTextureThread(String path) {
            super(new CountDownLatch(1));
            mPath = path;
        }

        @Override
        public void _run() {
            try{
                mTex = IO.load4DTextureFromFile(mPath + ".uni", true);
            } catch(RuntimeException e){
                e.printStackTrace();
            }
        }

        public Texture4D getTex() {
            return mTex;
        }
    }

    Thread initThread;
    private Texture4D load4DTexture(String path) {
        final LoadTextureThread thread = new LoadTextureThread(path);

        mCamera.post(new Runnable() {
            @Override
            public void run() {
                mCamera.queueEvent(thread);
            }
        });

        try {
            thread.GetEndSignal().await();
        } catch (InterruptedException e) {
            Log.e("WaterfallActivity", "Thread interrupted");
            e.printStackTrace();
        }
        Texture4D tex = thread.getTex();
        if(tex == null && initThread != Thread.currentThread()){
            throw new RuntimeException("Error Creating Texture " + path + ".uni");
        }
        return thread.getTex();
    }

    @Override
    public void awake() {
        mCamera.setEnabled(false);

        mDefoTimer = new AccumTimer(10, mCamera);
        mAdvectionTimer = new AccumTimer(100, mCamera);

        initThread = new Thread(new Runnable() {
            @Override
            public void run() {
                mCamera.post(new Runnable() {
                    @Override
                    public void run() {
                        mProgressDialog.show();
                        mProgressDialog.setProgress(0);
                    }
                });
                IO.awaitPermission();

                try {
                    mMainPhi = load4DTexture("n_newidlev1");
                    mProgressDialog.setProgress(1);

                    for (int i = 0; i < mDefo.length; i++) {
                        mDefo[i] = load4DTexture("n_wallt8_conc_" + i);
                        mProgressDialog.setProgress(2 + i);
                    }
                } catch(RuntimeException e){
                    e.printStackTrace();
                    return;
                }

                final Thread actThread = Thread.currentThread();

                mCamera.post(new Runnable() {
                    @Override
                    public void run() {
                        mCamera.queueEvent(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    int[] mainDim = mMainPhi.getDim();
                                    int[] defoDim = mDefo[0].getDim();

                                    mDimT = mainDim[3] - 2;
                                    t = mDimT;

                                    mMainDefo = new Texture4D(GLES31.GL_RGBA16F, defoDim[0], defoDim[1], defoDim[2], defoDim[3]);
                                    mRenderPhi = new Texture3D(GLES31.GL_RGBA16F, mainDim[0], mainDim[1], mainDim[2]);

                                    corr0 = new Texture4D(GLES31.GL_RGBA16F, defoDim[0], defoDim[1], defoDim[2], defoDim[3]);
                                    corr1 = new Texture4D(GLES31.GL_RGBA16F, defoDim[0], defoDim[1], defoDim[2], defoDim[3]);
                                    tmp0 = new Texture4D(GLES31.GL_RGBA16F, defoDim[0], defoDim[1], defoDim[2], defoDim[3]);
                                    tmp1 = new Texture4D(GLES31.GL_RGBA16F, defoDim[0], defoDim[1], defoDim[2], defoDim[3]);

                                    blend0 = new Texture3D(GLES31.GL_RGBA16F, mainDim[0], mainDim[1], mainDim[2]);
                                    blend1 = new Texture3D(GLES31.GL_RGBA16F, mainDim[0], mainDim[1], mainDim[2]);

                                    mRenderPhi.setBoundary(new int[]{1, 1, 1});

                                    mTensorflowHandler = new TensorFlowHandler("wall_defx_freezed_graph.pb", "in_param", new String[]{"out_param", "out_defx"}, new int[]{15, 15, 15, 15});
                                    nnCorr = mTensorflowHandler.getTexture();

                                    VolumeRenderComponent vrc = new VolumeRenderComponent(mRenderPhi, mCamera);
                                    vrc.setMaterial(new float[]{1, 1, 1, 0.4f}, new float[]{0, 0, 0, 0}, new float[]{1, 1, 1, 0.4f}, new float[]{1, 1, 1, 0.1f}, new float[]{1, 1, 1, 0.02f}, 32, 50.f);
                                    mWater = new Entity();
                                    mWater.setRenderComponent(vrc);
                                    mWater.getTransform().setPosition(0, 0.25f, 0);

                                    mParticlePhysic = new ParticlePhysicComponent(mRenderPhi, vrc.getNorAndCurvHandle(), 10000);
                                    mParticlePhysic.setBoundary(new int[]{2, 2, 2});
                                    mParticlePhysic.setBuoyancy(-PhysicEngine.GRAVITY * 0.1f);
                                    mParticlePhysic.setEnergyProperties(0.f, .2f);
                                    mParticlePhysic.setCurvProperties(5, 5);

                                    mParticleSystem = new Entity();
                                    mParticleSystem.getTransform().setPosition(0, 0.25f, 0);
                                    mParticleSystem.setPhysicComponent(mParticlePhysic);
                                    mParticleSystem.setRenderComponent(new ParticleRenderComponent(mParticlePhysic.getParticlePos(), mParticlePhysic.getParticleVel(), mParticlePhysic.getParticleCount(), mCamera));

                                    blendDeformations();
                                    mCamera.setOnTouchListener(WaterfallActivity.this);
                                    if(actThread == initThread) mCamera.setEnabled(true);
                                } catch (RuntimeException e) {
                                    e.printStackTrace();
                                    return;
                                }
                            }
                        });
                    }
                });

                mProgressDialog.dismiss();
            }
        });
        initThread.start();

        SolidRenderComponent floorRender = new SolidRenderComponent(MeshGenerator.GetPlaneMesh(), mCamera);
        floorRender.setTexture(IO.loadTextureFromFile("wood", false));
        floorRender.setNormalTexture(IO.loadTextureFromFile("normal_wood", false));
        floorRender.setMaterial(new float[]{1, 1, 1, 0}, new float[]{1, 1, 1, 0.7f}, new float[]{1, 1, 1, 0.2f}, new float[]{1, 1, 1, 0.1f}, 16);

        mFloor = new Entity();
        mFloor.setRenderComponent(floorRender);
        mFloor.getTransform().setPosition(0, -0.25f, 0);
        mFloor.getTransform().setScale(4, 4, 4);

        Texture boxTex = IO.loadTextureFromFile("red_plastic", false);
        Texture boxNor = IO.loadTextureFromFile("normal_fiber", false);

        Map<String, Mesh> stairs = IO.loadMeshesFromFile("stairs", false);

        float downScale = 0.95f;

        mMainBase = new Entity();
        mMainBase.setRenderComponent(new SolidRenderComponent(stairs.get("main"), mCamera));
        ((SolidRenderComponent) mMainBase.getRenderComponent()).setTexture(boxTex);
        ((SolidRenderComponent) mMainBase.getRenderComponent()).setNormalTexture(boxNor);
        mMainBase.getTransform().setScale(downScale, downScale, downScale);

        mCornerBase = new Entity();
        mCornerBase.setRenderComponent(new SolidRenderComponent(stairs.get("corner"), mCamera));
        ((SolidRenderComponent) mCornerBase.getRenderComponent()).setTexture(boxTex);
        ((SolidRenderComponent) mCornerBase.getRenderComponent()).setNormalTexture(boxNor);
        mCornerBase.getTransform().setScale(downScale, downScale, downScale);

        mWall = new Entity();
        mWall.setRenderComponent(new SolidRenderComponent(stairs.get("wall"), mCamera));
        ((SolidRenderComponent) mWall.getRenderComponent()).setTexture(boxTex);
        ((SolidRenderComponent) mWall.getRenderComponent()).setNormalTexture(boxNor);
        mWall.getTransform().setPosition(0, -0.25f, -0.55f);

        mBaseWall = new Entity();
        mBaseWall.setRenderComponent(new SolidRenderComponent(stairs.get("baseWall"), mCamera));
        ((SolidRenderComponent) mBaseWall.getRenderComponent()).setTexture(boxTex);
        ((SolidRenderComponent) mBaseWall.getRenderComponent()).setNormalTexture(boxNor);
        mBaseWall.getTransform().setScale(downScale, downScale, downScale);
        mBaseWall.getTransform().setPosition(0, -0.25f, 0);

        SolidRenderComponent arrowRender = new SolidRenderComponent(MeshGenerator.GetPlaneMesh(), mCamera);
        arrowRender.setTexture(IO.loadTextureFromFile("arrow", false));
        arrowRender.setNormalTexture(IO.loadTextureFromFile("normal_alu", false));
        mInArrow = new Entity();
        mInArrow.setRenderComponent(arrowRender);
        mInArrow.getTransform().rotate(90, 0, 1, 0);
        mInArrow.getTransform().setScale(0.5f, 0.5f, 0.5f);

        arrowRender = new SolidRenderComponent(MeshGenerator.GetPlaneMesh(), mCamera);
        arrowRender.setTexture(IO.loadTextureFromFile("arrow", false));
        arrowRender.setNormalTexture(IO.loadTextureFromFile("normal_alu", false));
        mOutArrow = new Entity();
        mOutArrow.setRenderComponent(arrowRender);
        mOutArrow.getTransform().setPosition(0.65f, 0.1f, -0.25f);
        mOutArrow.getTransform().rotate(90, 0, 1, 0);
        mOutArrow.getTransform().setScale(0.5f, 0.5f, 0.5f);
    }

    public void updateBases(float[] alpha){
        if(mMainBase != null) {
            float baseH = 0.45f + 0.3f * (1 - alpha[1]);
            float cornerH = baseH * 0.9f * alpha[0];
            float wallPos = 0.3f + 0.4f * alpha[2];

            mMainBase.getTransform().setPosition(0.f, baseH - 1.25f, 0.f);
            mCornerBase.getTransform().setPosition(0.f, cornerH - 1.251f, 0.f);
            mWall.getTransform().setScale(1.f, baseH + 0.25f, wallPos);

            mInArrow.getTransform().setPosition(-0.65f, baseH, -0.25f);
        }
    }

    float t;
    final static int blendFrames = 10;

    @Override
    public void update(float dt) {

        mFrameTimer.update(dt, new AccumTimer.OnTimeAccum() {
            @Override
            public void timeAccum(final float accumTime) {
                mFPSLabel.post(new Runnable() {
                    @Override
                    public void run() {
                        mFPSLabel.setText((float) (int) ((1000.0 / accumTime) * 10) / 10 + " FPS");
                    }
                });
            }
        });

        dt /= 100;

        mParticlePhysic.genParticles(!isCalcingDefo);

        if (mState == SimState.RUN) {
            if(RenderEngine.TIME_MEASUREMENT) mAdvectionTimer.start();
            t = (t + dt) % (mDimT - blendFrames);
            if(t < blendFrames){
                mMainPhi.advect(blend0, mMainDefo, 1, t + mDimT - blendFrames);
                mMainPhi.advect(blend1, mMainDefo, 1, t);
                blend0.blend(mRenderPhi, blend1, 1 - t / blendFrames);
            } else {
                mMainPhi.advect(mRenderPhi, mMainDefo, 1, t);
            }
            mParticlePhysic.setEnabled(true);
            if(RenderEngine.TIME_MEASUREMENT) mAdvectionTimer.stopAndTryEval(new AccumTimer.OnTimeAccum() {
                @Override
                public void timeAccum(float accumTime) {
                    Log.d("Avg SDF Creation Time", accumTime + " ms");
                }
            });
        } else {
            mParticlePhysic.setEnabled(false);
        }
    }

    private void updateCam(){
        if (mAzimut < -180) mAzimut += 360;
        else if (mAzimut > 180) mAzimut -= 360;
        if (mInclination < 10) mInclination = 10;
        else if (mInclination > 70-mCamera.getZoom()*20) mInclination = 70-mCamera.getZoom()*20;

        mCameraTransform.setPosition(
                mCameraDistance * (float) Math.sin(Math.toRadians(mInclination)) * (float) Math.sin(Math.toRadians(mAzimut)),
                mCameraDistance * (float) Math.cos(Math.toRadians(mInclination)),
                mCameraDistance * (float) Math.sin(Math.toRadians(mInclination)) * (float) Math.cos(Math.toRadians(mAzimut)));
        mCameraTransform.lookAt(0, 0, 0);
    }

    private float distance(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float)Math.sqrt(x * x + y * y);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if(event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN){
            mDist = distance(event);
            if (mDist > 10) mLastTouchX = -1;
            return true;
        }
        if(event.getActionMasked() == MotionEvent.ACTION_POINTER_UP){
            mDist = 0;
            return true;
        }

        if (event.getAction() == MotionEvent.ACTION_MOVE) {
            float touchX = event.getX();
            float touchY = event.getY();

            if (mLastTouchX != -1) {
                mAzimut += (mLastTouchX - touchX) * mRotFactor;
                mInclination += (mLastTouchY - touchY) * mRotFactor;

                updateCam();

                mLastTouchX = touchX;
                mLastTouchY = touchY;
            } else if(mDist > 10) {
                float newDist = distance(event);
                if(newDist > 10){
                    float scale = mDist / newDist * mCamera.getZoom();
                    mDist = newDist;
                    if(scale > 1.5f) scale = 1.5f;
                    else if(scale < 0.5f) scale = 0.5f;
                    mCamera.setZoom(scale);

                    updateCam();
                }
            }
            return true;
        }

        if (event.getAction() == MotionEvent.ACTION_UP) {
            mLastTouchX = -1;
            mLastTouchY = -1;
            return true;
        }

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            mLastTouchX = event.getX();
            mLastTouchY = event.getY();
            return true;
        }

        return true;
    }

}
