package prantl.de.interactivefluidengine.resources;

import android.graphics.Bitmap;
import android.opengl.GLES31;
import android.opengl.GLUtils;

import prantl.de.interactivefluidengine.util.GLHelper;
import prantl.de.interactivefluidengine.util.IO;

/**
 * Created by Lukas on 30.06.17.
 */

public class Texture implements ITexture {
    int[] mDim = new int[2];
    int mTextureHandle;

    public Texture(Bitmap bitmap){
        mDim[0] = bitmap.getWidth();
        mDim[1] = bitmap.getHeight();

        final int[] t = new int[1];
        GLES31.glGenTextures(1, t, 0);
        mTextureHandle = t[0];

        bindTexture();

        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_MIN_FILTER, GLES31.GL_LINEAR);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_MAG_FILTER, GLES31.GL_LINEAR);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_WRAP_R, GLES31.GL_CLAMP_TO_EDGE);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_WRAP_S, GLES31.GL_CLAMP_TO_EDGE);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_WRAP_T, GLES31.GL_CLAMP_TO_EDGE);
        GLHelper.CheckGLError();

        if(mTextureHandle == 0){
            throw new RuntimeException("Error loading texture.");
        }

        GLUtils.texImage2D(GLES31.GL_TEXTURE_2D, 0, bitmap, 0);

        unbindTexture();
    }

    public Texture(int textureFormat, int x, int y){
        mDim[0] = x;
        mDim[1] = y;

        final int[] t = new int[1];
        GLES31.glGenTextures(1, t, 0);
        mTextureHandle = t[0];

        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, mTextureHandle);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_MIN_FILTER, GLES31.GL_LINEAR);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_MAG_FILTER, GLES31.GL_LINEAR);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_WRAP_R, GLES31.GL_CLAMP_TO_EDGE);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_WRAP_S, GLES31.GL_CLAMP_TO_EDGE);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_WRAP_T, GLES31.GL_CLAMP_TO_EDGE);

        // allocate texture storage
        GLES31.glTexStorage2D(GLES31.GL_TEXTURE_2D, 1, textureFormat, mDim[0], mDim[1]);
        GLHelper.CheckGLError();

        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, 0);
    }

    @Override
    public int getTextureHandle(){
        return mTextureHandle;
    }

    @Override
    public int getTextureFormat() {
        return 0;
    }

    @Override
    public int[] getDim() {
        return mDim;
    }

    @Override
    public int[] getBoundary() {
        // TODO
        return null;
    }

    @Override
    public void setBoundary(int[] boundary) {
        // TODO
    }

    @Override
    public void bindTexture() {
        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, mTextureHandle);
    }

    @Override
    public void unbindTexture() {
        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, 0);
    }
}
