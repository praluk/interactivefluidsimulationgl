//
//	Implementation of a custom transpose convolution layer. Testing this to see how hard it will be to implement in 4D
//	
//	Written by Boris Bonev, 21.10.2016
//	TU Munich
//

//#include "tensorflow/core/framework/tensor_shape.h"
//#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/framework/op_kernel.h"
#include <unsupported/Eigen/CXX11/Tensor>
//#include "tfkernels.h"
//#include "fileio.h"
//#include "grid.h"

//using tensorflow::string;

using namespace tensorflow;

// to shorten syntax
using shape_inference::DimensionHandle;
using shape_inference::InferenceContext;
using shape_inference::ShapeHandle;

//
//	Custom Deconvolution Operator
//

REGISTER_OP("Deconv4dOp")
	.Attr("verbose: bool = False")
	.Attr("strides: list(int) = [1, 1, 1, 1, 1, 1]")
	.Input("value: float")
	.Input("filter: float")
	.Output("output: float");

class Deconv4dOp : public OpKernel {

	public:
	
	explicit Deconv4dOp(OpKernelConstruction* context) : OpKernel(context) {
		// Get the attributes
		OP_REQUIRES_OK(context, context->GetAttr("verbose", &verbose_));
		OP_REQUIRES_OK(context, context->GetAttr("strides", &strides_));
	}

	void Compute(OpKernelContext* context) override {

		if (verbose_) std::clog << "Starting Deconv4d Compute " << std::endl;
		
		// Grab the input
		const Tensor& value = context->input(0);
		const Tensor& filter = context->input(1);

    //OP_REQUIRES(context, TensorShapeUtils::IsVector(output_shape.shape()), errors::InvalidArgument("Deconv4dOp expects a 1-D output_shape vector."));

		// Determine dimensions of the input tensor
		const int batch_size = value.dim_size(0);
		const int x_in = value.dim_size(1);
		const int y_in = value.dim_size(2);
		const int z_in = value.dim_size(3);
		const int t_in = value.dim_size(4);
		const int features_in = value.dim_size(5);

		// Determine the dimensions of the filter kernels
		const int x_filter = filter.dim_size(0);
		const int y_filter = filter.dim_size(1);
		const int z_filter = filter.dim_size(2);
		const int t_filter = filter.dim_size(3);
		const int filter_channels_out = filter.dim_size(4);
		const int filter_channels_in = filter.dim_size(5);

		// Get strides
		const int sx = strides_[1];
		const int sy = strides_[2];
		const int sz = strides_[3];
		const int st = strides_[4];
		
		// Get the Eigen tensors with correct dimensions
		auto input = value.shaped<float, 6>({batch_size, x_in, y_in, z_in, t_in, features_in});
		auto weights = filter.shaped<float, 6>({x_filter, y_filter, z_filter, t_filter, filter_channels_out, filter_channels_in});

		// Calculate output dimensions
		const int x_out = (x_in - 1)*sx + x_filter;
		const int y_out = (y_in - 1)*sy + y_filter;
		const int z_out = (z_in - 1)*sz + z_filter;
		const int t_out = (t_in - 1)*st + t_filter;
		if (verbose_) std::clog << "Output dims are " << x_out << "x" << y_out << "x" << z_out << "x" << t_out << std::endl;

		// Create the output tensor
		Tensor* output_ = NULL;
		OP_REQUIRES_OK(context, context->allocate_output(0, TensorShape({batch_size, x_out, y_out, z_out, t_out, filter_channels_out}), &output_));
		//std::clog << "here " << std::endl;
		auto output = output_->shaped<float, 6>({batch_size, x_out, y_out, z_out, t_out, filter_channels_out});

		output.setZero();


		int q, r, s, t;
		// Iterate over batches
		for (int b = 0; b < batch_size; b++) {
			//std::clog << "Processing batch " << b << std::endl;
			// Iterate over input channels
			for (int chi = 0; chi < filter_channels_in; chi++) {
				// Iterate over output channels
				for (int cho = 0; cho < filter_channels_out; cho++) {

					// Iterate over inputs
					for (int i = 0; i < x_in; i++) {
						for (int j = 0; j < y_in; j++) {
							for (int k = 0; k < z_in; k++) {
								for (int l = 0; l < t_in; l++) {

									// Iterate over filter weights
									for (int m = 0; m < x_filter; m++) {
										for (int n = 0; n < y_filter; n++) {
											for (int o = 0; o < z_filter; o++) {
												for (int p = 0; p < t_filter; p++) {
													q = i*sx + m;
													r = j*sy + n;
													s = k*sz + o;
													t = l*st + p;
													output(b,q,r,s,t,cho) += weights(m,n,o,p,cho,chi) * input(b,i,j,k,l,chi);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}

	private:

		TensorShape output_shape_;
		bool verbose_;
		std::vector<int32> strides_;
};

REGISTER_KERNEL_BUILDER(Name("Deconv4dOp").Device(DEVICE_CPU), Deconv4dOp);

//
//	Calculates the backprop delta of the Custom Deconvolution Operator
//	this is not the gradient! it is the delta!
//

REGISTER_OP("Deconv4dGrad")
	.Attr("verbose: bool = False")
	.Attr("strides: list(int) = [1, 1, 1, 1, 1, 1]")
	.Input("value: float")
	.Input("filter: float")
	.Input("ini_grad: float")
	.Output("grad_inputs: float")
	.Output("grad_filters: float");

class Deconv4dGrad : public OpKernel {

	public:
	
	explicit Deconv4dGrad(OpKernelConstruction* context) : OpKernel(context) {
		// Get the attributes
		OP_REQUIRES_OK(context, context->GetAttr("verbose", &verbose_));
		OP_REQUIRES_OK(context, context->GetAttr("strides", &strides_));
	}

	void Compute(OpKernelContext* context) override {
		
		if (verbose_) std::clog << "Starting Deconv4dGrad Compute " << std::endl;

		// Grab the input
		const Tensor& value = context->input(0);
		const Tensor& filter = context->input(1);
		// This is used to weigh the gradients??
		// see https://www.tensorflow.org/versions/r0.11/api_docs/python/train.html#gradient-computation
		// tf.gradients uses this.
		const Tensor& ini_grad_ = context->input(2);

		// Determine dimensions of the input tensor
		const int batch_size = value.dim_size(0);
		const int x_in = value.dim_size(1);
		const int y_in = value.dim_size(2);
		const int z_in = value.dim_size(3);
		const int t_in = value.dim_size(4);
		const int features_in = value.dim_size(5);

		// Determine the dimensions of the filter kernels
		const int x_filter = filter.dim_size(0);
		const int y_filter = filter.dim_size(1);
		const int z_filter = filter.dim_size(2);
		const int t_filter = filter.dim_size(3);
		const int filter_channels_out = filter.dim_size(4);
		const int filter_channels_in = filter.dim_size(5);

		// Get strides
		const int sx = strides_[1];
		const int sy = strides_[2];
		const int sz = strides_[3];
		const int st = strides_[4];
		
		// Calculate output dimensions
		const int x_out = (x_in - 1)*sx + x_filter;
		const int y_out = (y_in - 1)*sy + y_filter;
		const int z_out = (z_in - 1)*sz + z_filter;
		const int t_out = (t_in - 1)*st + t_filter;
		if (verbose_) std::clog << "Output dims are " << x_out << "x" << y_out << "x" << z_out << "x" << t_out << std::endl;

		// check the dimensions align
		OP_REQUIRES(context, filter_channels_in==features_in, errors::InvalidArgument("Dimensions don't align'."));

		OP_REQUIRES(context, ini_grad_.dim_size(0)==batch_size, errors::InvalidArgument("Dimensions don't align'."));
		OP_REQUIRES(context, ini_grad_.dim_size(1)==x_out, errors::InvalidArgument("Dimensions don't align'."));
		OP_REQUIRES(context, ini_grad_.dim_size(2)==y_out, errors::InvalidArgument("Dimensions don't align'."));
		OP_REQUIRES(context, ini_grad_.dim_size(3)==z_out, errors::InvalidArgument("Dimensions don't align'."));
		OP_REQUIRES(context, ini_grad_.dim_size(4)==t_out, errors::InvalidArgument("Dimensions don't align'."));
		OP_REQUIRES(context, ini_grad_.dim_size(5)==filter_channels_out, errors::InvalidArgument("Dimensions don't align'."));
		
		// Get the Eigen tensors with correct dimensions
		auto input = value.shaped<float, 6>({batch_size, x_in, y_in, z_in, t_in, features_in});
		auto weights = filter.shaped<float, 6>({x_filter, y_filter, z_filter, t_filter, filter_channels_out, filter_channels_in});
		auto ini_grad = ini_grad_.shaped<float, 6>({batch_size, x_out, y_out, z_out, t_out, filter_channels_out});

		// Create the output tensors
		//std::clog << "Gradient dims are " << x_filter*y_filter*filter_channels_in*filter_channels_out << ", " << batch_size*x_out*y_out*filter_channels_out << std::endl;

		Tensor* grad_1_ = NULL;
		Tensor* grad_2_ = NULL;
		OP_REQUIRES_OK(context, context->allocate_output(0, TensorShape({batch_size, x_in, y_in, z_in, t_in, features_in}), &grad_1_));
		OP_REQUIRES_OK(context, context->allocate_output(1, TensorShape({x_filter, y_filter, z_filter, t_filter, filter_channels_out, filter_channels_in}), &grad_2_));
		// wrt Inputs
		auto grad_1 = grad_1_->shaped<float, 6>({batch_size, x_in, y_in, z_in, t_in, features_in});
		// wrt Filter Weights
		auto grad_2 = grad_2_->shaped<float, 6>({x_filter, y_filter, z_filter, t_filter, filter_channels_out, filter_channels_in});

		//std::clog << "dim 0 " << grad_2_->dim_size(0) << std::endl;
		//std::clog << "dim 1 " << grad_2_->dim_size(1) << std::endl;

		grad_1.setZero();
		grad_2.setZero();

		// write the gradient
		int q,r,s,t;
		// Iterate over batches
		for (int b = 0; b < batch_size; b++) {
			//std::clog << "Processing batch " << b << std::endl;
			// Iterate over input channels
			for (int chi = 0; chi < filter_channels_in; chi++) {
				// Iterate over output channels
				for (int cho = 0; cho < filter_channels_out; cho++) {

					// Iterate over inputs
					for (int i = 0; i < x_in; i++) {
						for (int j = 0; j < y_in; j++) {
							for (int k = 0; k < z_in; k++) {
								for (int l = 0; l < t_in; l++) {
									// Iterate over filter weights
									for (int m = 0; m < x_filter; m++) {
										for (int n = 0; n < y_filter; n++) {
											for (int o = 0; o < z_filter; o++) {
												for (int p = 0; p < t_filter; p++) {
													q = i*sx + m;
													r = j*sy + n;
													s = k*sz + o;
													t = l*st + p;

													grad_1(b,i,j,k,l,chi) += ini_grad(b,q,r,s,t,cho)*weights(m,n,o,p,cho,chi);
													grad_2(m,n,o,p,cho,chi) += ini_grad(b,q,r,s,t,cho)*input(b,i,j,k,l,chi);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private:

		bool verbose_;
		std::vector<int32> strides_;
};

REGISTER_KERNEL_BUILDER(Name("Deconv4dGrad").Device(DEVICE_CPU), Deconv4dGrad);


// EOL
