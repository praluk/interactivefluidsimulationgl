package fluidsim.de.interactivefluiddrop.Downloader;

import com.google.android.vending.expansion.downloader.impl.DownloaderService;

/**
 * Created by Lukas on 09.09.17.
 */

public class AssetDownloaderService extends DownloaderService {
    @Override
    public String getPublicKey() {
        return "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAy/j6iwb0Rzd5LKHXc+zmCghmDGJB2YaSRHdFJ0RkHeSAwXzmNRrwkhz0+06h24v85E6lo0i6i5nKF4a9TEXoy1Nz5TarSPEvw1u5Gz659ddTldm2pCJNOvT8T84YnXLZVaIgqmYsjy+j7RvvBu0sBuNozBNJOYnAj6Qgy9FuaeqYZk3iaMZyFXLtRwRnbBzDJAW0blF3ul38MEjc4Dn11t0PyHJPVqc7X2x0zyxqUAB6Hz7jzS8RiZVerG27HxcgkuMbNRrI83htt6uFQRqCRKdx0iX7kZnQc9HXJJYTASKWaYhQA6vuaGXa6CPEBUz6BePWiRKd96zYdMmZ0wHahQIDAQAB";
    }

    @Override
    public byte[] getSALT() {
        return new byte[] { 8, -98, 83, 119, -10, 6, 50, 57, 124, -88, 8, 90, 72, -114, 72, 71, 15, 18, -118, 67 };
    }

    @Override
    public String getAlarmReceiverClassName() {
        return AssetAlarmReceiver.class.getName();
    }
}
