#version 310 es

layout(location = 0) in vec4 pos;

layout(location = 0) uniform mat4 mv;

layout(std140, binding = 0) uniform cam_data
{
    mat4 proj;
};


out vec3 uv;

void main() {
   uv = pos.xyz;
   gl_Position = (proj * vec4(mat3(mv) * pos.xyz, 1)).xyww;
}
