package prantl.de.interactivefluidengine.scene.Component;

import java.util.Vector;

import prantl.de.interactivefluidengine.scene.Camera;
import prantl.de.interactivefluidengine.scene.Entity;

/**
 * Created by Lukas on 30.05.17.
 */

// a physiccomponent foreach compute shader used for physics
public abstract class IPhysicComponent {
    protected Entity mEntity;
    protected boolean mEnabled = true;

    public IPhysicComponent() {
        getIPhysicComponentVector().add(this);
    }

    public void setEntity(Entity entity) { mEntity = entity; }

    public void setEnabled(boolean enabled){ mEnabled = enabled; }
    public boolean isEnabled(){ return mEnabled; }

    public abstract void update(float dt);
    protected abstract Vector<IPhysicComponent> getIPhysicComponentVector();
}
