# Neural Liquids

![drop](img/drop_screen.png)
![hq-drop](img/hq-drop_screen.png)
![waterfall](img/waterfall_screen.png)

Probably the first Android Engine that can compute and display 3D liquid simulations on smartphones in real-time. The project uses a pre-computed, reduced model, and is a tech demo of the scientific paper ["Pre-computed Liquid Spaces with Generative Neural Networks and Optical Flow"](https://wwwcg.in.tum.de/research/ercstg-realflow/realflow-real-time-liquids/) from the Technical University of Munich, in cooperation with Nils Thuerey and Boris Bonev.
 
Additionally there is available a sample implementation called "Neural Liquid Drop" (also available on the [Play Store](https://play.google.com/store/apps/details?id=fluidsim.de.interactivedrop))

**Author**: [Lukas Prantl](http://lukas.prantl.it), **License**: MIT