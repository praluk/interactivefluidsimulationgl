#version 310 es

precision highp float;
precision mediump sampler3D;

#define MAX_LIGHT_COUNT 10
#define M_PI 3.1415926535897932384626433832795

struct LightData{
    vec3 pos;
    vec3 col;
};

layout(location = 0, binding = 0) uniform sampler3D phi;
layout(location = 1, binding = 1) uniform sampler3D norAndCurv;

layout(location = 2) uniform mat4 mv;
layout(location = 3) uniform vec3 cam;

layout(std140, binding = 0) uniform cam_data
{
    mat4 proj;
    int lightCount;
    LightData light[MAX_LIGHT_COUNT];
};

layout(location = 32, binding = 10) uniform samplerCube sky;

layout(std140, binding = 1) uniform blinn_phong_data
{
    vec4 reflCol;
    vec4 diffCol;
    vec4 specCol;
    vec4 ambientCol;
    vec4 curvCol;
    int shinniness;
    float maxCurvature;
} material;

layout(std140, binding = 2) uniform properties
{
    vec3 offset;
    vec3 nextOffset;
    float offsetFrac;
    vec3 downScale;
    vec3 resolution;
    ivec3 dim;
};

in vec3 uv;
in vec3 view_pos;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 displace;

const float refrI0 = 1.0; // air
const float refrI1 = 1.3333; // water
const float refrFactor = refrI0 / refrI1;
const float reflCoeff = pow((refrI0 - refrI1) / (refrI0 + refrI1), 2.0);

float samplePhi(vec3 pos){
    return texture(phi, pos*downScale + offset).r * (1.0 - offsetFrac) + texture(phi, pos*downScale + nextOffset).r * offsetFrac;
}

vec3 gradient(vec3 pos){
    vec3 grad;
    grad.x = (samplePhi(vec3(pos.x + resolution.x/2.0, pos.y, pos.z)) - samplePhi(vec3(pos.x - resolution.x/2.0, pos.y, pos.z))) / resolution.x;
    grad.y = (samplePhi(vec3(pos.x, pos.y + resolution.y/2.0, pos.z)) - samplePhi(vec3(pos.x, pos.y - resolution.y/2.0, pos.z))) / resolution.y;
    grad.z = (samplePhi(vec3(pos.x, pos.y, pos.z + resolution.z/2.0)) - samplePhi(vec3(pos.x, pos.y, pos.z - resolution.z/2.0))) / resolution.z;
    return grad;
}

float fresnel(vec3 dir, vec3 nor){
    return reflCoeff + (1.0 - reflCoeff) * (1.0 - pow(dot(nor, -dir), 5.0));
}

vec4 sampleSkyMap(vec3 dir){
    return vec4(texture(sky, dir).rgb, 1.0);
}

vec4 blinnPhong(vec3 pos, vec3 vDir, vec3 nor){
    vec4 col = vec4(0, 0, 0, 0);
    for(int i = 0; i < lightCount; i++){
        vec3 lightDir = normalize(light[i].pos - pos);
        vec3 refl = reflect(vDir, nor);

        float spec = (float(material.shinniness) + 2.0) / 6.2831853072 * pow(clamp(dot(refl, lightDir), 0.0, 1.0), float(material.shinniness));
        float diff = clamp(dot(nor, lightDir), 0.0, 1.0);

        col +=
            (vec4(material.reflCol.xyz * sampleSkyMap(refl).xyz, 1.0) * material.reflCol.w * fresnel(vDir, nor)  +
            vec4(material.specCol.xyz * spec, 1.0) * material.specCol.w +
            vec4(material.diffCol.xyz * diff, 1.0) * material.diffCol.w +
            vec4(material.ambientCol.xyz, 1.0) * material.ambientCol.w) * vec4(light[i].col, 1.0);
    }

	return col;
}

//const float exposure = 1.0;
//const float gamma = 1.0;// /2.2;

void main() {
    vec3 dir = normalize(uv - 0.5 - cam);
    float cam_dist = -view_pos.z;

    float stepSize = length(dir * resolution);
    float actV = 0.0;
    float oldV = 0.0;

    bool surface = false;

    float depth = 10.0;
    outColor = vec4(0);

    vec3 pIn = uv;
    vec3 s;

    for(s = uv;
        s.x >= 0.0 && s.x <= 1.0 &&
        s.y >= 0.0 && s.y <= 1.0 &&
        s.z >= 0.0 && s.z <= 1.0; s += dir * resolution){
        actV = samplePhi(s);
        if(actV < 0.0)
        {
            if(surface){
                outColor = mix(outColor, material.diffCol, material.diffCol.w);
            } else {
                s += actV / (oldV - actV) * dir * resolution;
                pIn = s;

                vec4 nor_curv = texture(norAndCurv, s);

                depth = cam_dist + actV / (oldV - actV) * stepSize;

                outColor = blinnPhong((mv * vec4(s - 0.5, 1)).xyz, dir, nor_curv.xyz) + vec4(material.curvCol.rgb, 1) * material.curvCol.w * nor_curv.w;
                surface = true;

                dir = refract(dir, nor_curv.xyz, refrFactor);
            }
        }
        oldV = actV;
        if(!surface) cam_dist += stepSize;
    }

    if(!surface){
        discard;
    }

    vec4 pDir = proj * mv * vec4(pIn - 0.5, 1);
    vec4 pDisp = proj * mv * vec4(s - 0.5, 1);
    displace = (pDisp / pDisp.w - pDir / pDir.w + 0.1) * 5.0;

    displace.zw = vec2(acos(dir.z), (atan(dir.y, dir.x) + M_PI) / 2.0) / M_PI;

    gl_FragDepth = depth / 10.0;

   //vec3 mapped = vec3(1.0) - exp(-abs(outColor.rgb) * exposure);
   //outColor.rgb = pow(abs(mapped), vec3(gamma));
}