//
// Created by Lukas Prantl on 05.11.17.
//

#ifndef INTERACTIVEFLUIDSIMULATIONGL_GL_TIMER_H
#define INTERACTIVEFLUIDSIMULATIONGL_GL_TIMER_H

#include <string.h>
#include <jni.h>

#include <GLES3/gl31.h>
#include <EGL/egl.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void (GL_APIENTRYP PFNGLGENQUERIESEXTPROC) (GLsizei n, GLuint *ids);
typedef void (GL_APIENTRYP PFNGLDELETEQUERIESEXTPROC) (GLsizei n, const GLuint *ids);
//typedef GLboolean (GL_APIENTRYP PFNGLISQUERYEXTPROC) (GLuint id);
typedef void (GL_APIENTRYP PFNGLBEGINQUERYEXTPROC) (GLenum target, GLuint id);
typedef void (GL_APIENTRYP PFNGLENDQUERYEXTPROC) (GLenum target);
//typedef void (GL_APIENTRYP PFNGLGETQUERYIVEXTPROC) (GLenum target, GLenum pname, GLint *params);
typedef void (GL_APIENTRYP PFNGLGETQUERYOBJECTUIVEXTPROC) (GLuint id, GLenum pname, GLuint *params);

PFNGLGENQUERIESEXTPROC glGenQueriesEXT = (PFNGLGENQUERIESEXTPROC) eglGetProcAddress("glGenQueriesEXT");
PFNGLBEGINQUERYEXTPROC glBeginQueryEXT = (PFNGLBEGINQUERYEXTPROC) eglGetProcAddress("glBeginQueryEXT");
PFNGLENDQUERYEXTPROC glEndQueryEXT = (PFNGLENDQUERYEXTPROC) eglGetProcAddress("glEndQueryEXT");
PFNGLGETQUERYOBJECTUIVEXTPROC glGetQueryObjectuivEXT = (PFNGLGETQUERYOBJECTUIVEXTPROC) eglGetProcAddress("glGetQueryObjectuivEXT");
PFNGLDELETEQUERIESEXTPROC glDeleteQueriesEXT = (PFNGLDELETEQUERIESEXTPROC) eglGetProcAddress("glDeleteQueriesEXT");

#define TIME_ELAPSED_EXT 0x88BF

JNIEXPORT int JNICALL
Java_prantl_de_interactivefluidengine_util_AccumTimer_createTimeQuery( JNIEnv* env, jobject thiz );

JNIEXPORT void JNICALL
Java_prantl_de_interactivefluidengine_util_AccumTimer_beginTimeQuery( JNIEnv* env, jobject thiz, int q );

JNIEXPORT void JNICALL
Java_prantl_de_interactivefluidengine_util_AccumTimer_endTimeQuery( JNIEnv* env, jobject thiz, int q );

JNIEXPORT int JNICALL
Java_prantl_de_interactivefluidengine_util_AccumTimer_getTime( JNIEnv* env, jobject thiz, int q );

JNIEXPORT void JNICALL
Java_prantl_de_interactivefluidengine_util_AccumTimer_deleteTimeQuery( JNIEnv* env, jobject thiz, int q );

#ifdef __cplusplus
}
#endif

#endif //INTERACTIVEFLUIDSIMULATIONGL_GL_TIMER_H
