package prantl.de.interactivefluidengine.util;

import android.opengl.GLES31;

import prantl.de.interactivefluidengine.resources.Texture4D;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.nio.FloatBuffer;

/**
 * Created by Lukas on 18.07.17.
 */

public class TensorFlowHandler {
    static {
        try{
            System.loadLibrary("tensorflow");
            System.out.println("Loaded tensorflow");
        }catch(UnsatisfiedLinkError e){
            //nothing to do
            System.out.println("Couldn't load tensorflow");
            System.out.println(e.getMessage());
        }
    }

    private final TensorFlowInferenceInterface mInterface;

    private final String mInputName;
    private final String[] mOutputNames;

    private final Texture4D mTexture;

    private final int[] mDim;

    public TensorFlowHandler(final String graphPath, final String inputName, final String[] outputNames, final int[] dim){
        mInputName = inputName;
        mOutputNames = outputNames;
        mDim = dim;

        mInterface = new TensorFlowInferenceInterface();
        mInterface.initializeTensorFlow(IO.getAssets(), "file:///android_asset/" + graphPath);

        mTexture = new Texture4D(GLES31.GL_RGBA16F, dim[0], dim[1], dim[2], dim[3]);
    }

    public Texture4D getTexture(){ return mTexture; }

    public Texture4D runNeuralNetwork(final float[] alpha){
        float result[] = new float[mDim[0] * mDim[1] * mDim[2] * mDim[3] * 4];

        mInterface.fillNodeFloat(mInputName, new int[]{1, alpha.length}, alpha);
        mInterface.runInference(mOutputNames);
        mInterface.readNodeFloat(mOutputNames[0], alpha);
        mInterface.readNodeFloat(mOutputNames[1], result);

        // TODO: find a more efficient way!
        FloatBuffer fb = FloatBuffer.allocate(mDim[0] * mDim[1] * mDim[2] * 4);

        // TODO: move this to Texture4D?!
        mTexture.bindTexture();
        for(int i = 0; i < mDim[3]; i++){
            final int[] off = mTexture.getIntegralOffset(i);

            fb.put(result, i * mDim[0] * mDim[1] * mDim[2] * 4, mDim[0] * mDim[1] * mDim[2] * 4);
            fb.rewind();

            GLES31.glTexSubImage3D(GLES31.GL_TEXTURE_3D, 0, off[0], off[1], off[2], mDim[0], mDim[1], mDim[2], GLES31.GL_RGBA, GLES31.GL_FLOAT, fb);
        }
        mTexture.unbindTexture();

        return mTexture;
    }
}
