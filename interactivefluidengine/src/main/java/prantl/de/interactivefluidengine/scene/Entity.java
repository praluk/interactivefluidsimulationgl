package prantl.de.interactivefluidengine.scene;

import prantl.de.interactivefluidengine.scene.Component.IPhysicComponent;
import prantl.de.interactivefluidengine.scene.Component.IRenderComponent;
import prantl.de.interactivefluidengine.scene.Component.Transform;

/**
 * Created by Lukas on 30.05.17.
 */

// represents an entity in the world
public class Entity {
    private Transform mTransform;
    private IPhysicComponent mPhysicComponent;
    private IRenderComponent mRenderComponent;

    public Entity(){
        mTransform = new Transform();
    }

    public void setPhysicComponent(IPhysicComponent physicComponent){
        mPhysicComponent = physicComponent;
        mPhysicComponent.setEntity(this);
    }

    public void setRenderComponent(IRenderComponent renderComponent){
        mRenderComponent = renderComponent;
        mRenderComponent.setEntity(this);
    }

    public void setEnabled(boolean enabled){
        if(mRenderComponent != null) mRenderComponent.setEnabled(enabled);
        if(mPhysicComponent != null) mPhysicComponent.setEnabled(enabled);
    }

    public boolean isEnabled(){
        if(mRenderComponent != null && mRenderComponent.isEnabled()) return true;
        if(mPhysicComponent != null && mPhysicComponent.isEnabled()) return true;
        return false;
    }

    public Transform getTransform(){
        return mTransform;
    }

    public IPhysicComponent getPhysicComponent(){
        return mPhysicComponent;
    }

    public IRenderComponent getRenderComponent(){
        return mRenderComponent;
    }
}
