#version 310 es

precision mediump float;
precision mediump image3D;
precision lowp sampler3D;

layout(binding = 0, location = 0) uniform sampler3D phi;
layout(rgba16f, binding = 0, location = 3) uniform writeonly image3D res;

layout(std140, binding = 1) uniform properties
{
    float alpha;
    float t_off0;
    float t_off1;
    float ratio;
    ivec4 dim;
    ivec4 boundary;
    float fac0;
    float fac1;
};

vec3 getOffset(int time, ivec3 aSize){
    vec3 off;
    off.x = float(int(time) % aSize.x);
    off.y = float(int(time) / aSize.x % aSize.y);
    off.z = float(int(time) / (aSize.x * aSize.y) % aSize.z);
    return off;
}

// helper for sampling texture, aSize is the array size of the 3D array
vec4 sampleTexture(sampler3D tex, vec4 pos, ivec3 aSize){
    vec3 offset = getOffset(int(pos.w), aSize) / vec3(aSize);
    vec3 nextOffset = getOffset(int(pos.w) + 1, aSize) / vec3(aSize);
    return texture(tex, pos.xyz + offset) * (1.0 - fract(pos.w)) + texture(tex, pos.xyz + nextOffset) * fract(pos.w);
}

layout (local_size_x = 4, local_size_y = 4, local_size_z = 4) in;
void main() {
    if( int(gl_GlobalInvocationID.x) < dim.x &&
        int(gl_GlobalInvocationID.y) < dim.y &&
        int(gl_GlobalInvocationID.z) < dim.z ){

        ivec3 phiTexSize = textureSize(phi, 0);
        if( int(gl_GlobalInvocationID.x) < boundary.x || int(gl_GlobalInvocationID.x) >= dim.x - boundary.x ||
            int(gl_GlobalInvocationID.y) < boundary.y || int(gl_GlobalInvocationID.y) >= dim.y - boundary.y ||
            int(gl_GlobalInvocationID.z) < boundary.z || int(gl_GlobalInvocationID.z) >= dim.z - boundary.z ){

            imageStore(res, ivec3(gl_GlobalInvocationID), vec4(1));
            return;
        }

        vec3 pos = (vec3(gl_GlobalInvocationID.xyz) + 0.5) / vec3(phiTexSize);

        imageStore(res, ivec3(gl_GlobalInvocationID), sampleTexture(phi, vec4(pos, t_off0), phiTexSize / dim.xyz));
    }

}
