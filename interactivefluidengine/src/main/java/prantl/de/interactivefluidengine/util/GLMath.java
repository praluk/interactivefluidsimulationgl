package prantl.de.interactivefluidengine.util;

import android.opengl.Matrix;

/**
 * Created by Lukas on 10.06.17.
 */

public class GLMath {

    public static float[] cross(final float[] v0, final float[] v1){
        float[] r = new float[3];
        r[0] = v0[1] * v1[2] - v0[2] * v1[1];
        r[1] = v0[2] * v1[0] - v0[0] * v1[2];
        r[2] = v0[0] * v1[1] - v0[1] * v1[0];
        return r;
    }

    public static float dot(final float[] v0, final float[] v1) {
        return v0[0] * v1[0] + v0[1] * v1[1] + v0[2] * v1[2];
    }

    public static float length(final float[] v){
        return (float)Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
    }

    public static float[] normalize(final float[] v){
        float[] n = v.clone();
        float l = length(v);
        n[0] /= l;
        n[1] /= l;
        n[2] /= l;
        return n;
    }

    public static float[] lookAt(float lookX, float lookY, float lookZ, float posX, float posY, float posZ, float upX, float upY, float upZ){
        float[] m = new float[16];
        Matrix.setIdentityM(m, 0);

        float[] back = normalize(new float[]{posX - lookX, posY - lookY, posZ - lookZ});
        float[] up = new float[]{0, 1, 0};
        float[] right = normalize(cross(up, back));
        up = normalize(cross(back, right));

        for(int i = 0; i < 3; i++) m[i] = right[i];
        for(int i = 0; i < 3; i++) m[i+4] = up[i];
        for(int i = 0; i < 3; i++) m[i+8] = back[i];
        return m;
    }

    public static int roundUp(int num, int divisor) {
        return (num + divisor - 1) / divisor;
    }

    public static boolean rayQuadIntersection(float[] colPos, float[] viewDir, float[] viewPos, float[] p0, float[] p1, float[] p2){
        float[] e1 = new float[]{p1[0] - p0[0], p1[1] - p0[1], p1[2] - p0[2] };
        float[] e2 = new float[]{p2[0] - p0[0], p2[1] - p0[1], p2[2] - p0[2] };

        float[] p = cross(viewDir, e2);
        float det = dot(e1, p);

        if(Math.abs(det) < 10e-5f) return false;

        det = 1 / det;

        float[] dist = new float[]{viewPos[0] - p0[0], viewPos[1] - p0[1], viewPos[2] - p0[2] };

        colPos[0] = dot(dist, p) * det;
        if(colPos[0] < 0 || colPos[0] > 1) return false;

        p = cross(dist, e1);

        colPos[1] = dot(viewDir, p) * det;
        if(colPos[1] < 0 || colPos[1] > 1) return false;

        colPos[2] = dot(e2, p) * det + viewPos[2];

        if(colPos[2] < 0) return false;

        return true;
    }
}
