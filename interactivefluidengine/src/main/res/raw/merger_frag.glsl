#version 310 es

precision mediump float;
precision mediump sampler2D;

const float epsilon = 1e-5;
#define M_PI 3.1415926535897932384626433832795

layout(binding = 0, location = 0) uniform sampler2D solidColor;
layout(binding = 1, location = 1) uniform sampler2D volumeColor;
layout(binding = 2, location = 2) uniform sampler2D volumeDepth;
layout(binding = 3, location = 3) uniform sampler2D volumeDisplace;

layout(location = 32, binding = 10) uniform samplerCube sky;

vec4 sampleSkyMap(vec3 dir){
    return vec4(texture(sky, dir).rgb, 1.0);
}

in vec2 uv;
out vec4 outColor;

void main() {
    vec4 volumeCol = texture(volumeColor, uv);
    if(volumeCol.w < epsilon) discard;

    vec4 volDispl = texture(volumeDisplace, uv);

    vec2 displCoord = uv + (volDispl.xy / 5.0 - 0.1);

    if(texture(solidColor, displCoord).w < epsilon){
        vec3 dir;
        volDispl.zw *= M_PI;
        volDispl.w = volDispl.w * 2.0 - M_PI;
        dir.x = sin(volDispl.z)*cos(volDispl.w);
        dir.y = sin(volDispl.z)*sin(volDispl.w);
        dir.z = cos(volDispl.z);

        outColor = mix(volumeCol, sampleSkyMap(dir), 1.0 - volumeCol.w);
    } else {
        outColor = mix(volumeCol, vec4(texture(solidColor, displCoord).rgb, 1.0), 1.0 - volumeCol.w);
    }
    if(outColor.w > 0.0) outColor/=outColor.w;

    gl_FragDepth = texture(volumeDepth, uv).x;
}