package prantl.de.interactivefluidengine.util;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Lukas on 09.06.17.
 */

public abstract class SynchableThread extends Thread {
    CountDownLatch mEndSignal;
    public SynchableThread(CountDownLatch endSignal){
        mEndSignal = endSignal;
    }
    final public CountDownLatch GetEndSignal() { return mEndSignal; }
    public abstract void _run();
    @Override
    final public void run(){
        _run();
        mEndSignal.countDown();
    }
}
