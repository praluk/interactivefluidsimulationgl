package prantl.de.interactivefluidengine.resources;

import android.opengl.GLES31;
import android.util.Log;
import android.util.Pair;

import java.util.ArrayList;

import prantl.de.interactivefluidengine.scene.Camera;
import prantl.de.interactivefluidengine.util.IO;

/**
 * Created by Lukas on 09.06.17.
 */

public class Shader implements IResource {
    private int mProgramHandle;

    public Shader(final ArrayList<Pair<Integer, String>> programs){
        mProgramHandle = GLES31.glCreateProgram();

        for(Pair<Integer, String> s : programs){
            int shaderHandle = GLES31.glCreateShader(s.first);

            GLES31.glShaderSource(shaderHandle, s.second);
            GLES31.glCompileShader(shaderHandle);

            final int[] compileStatus = new int[1];
            GLES31.glGetShaderiv(shaderHandle, GLES31.GL_COMPILE_STATUS, compileStatus, 0);
            if (compileStatus[0] == 0) {
                Log.e("shader", GLES31.glGetShaderInfoLog(shaderHandle));
                GLES31.glDeleteShader(shaderHandle);
                shaderHandle = 0;
            }
            if (shaderHandle == 0) {
                throw new RuntimeException("Error creating shader.");
            }

            GLES31.glAttachShader(mProgramHandle, shaderHandle);
        }

        GLES31.glLinkProgram(mProgramHandle);

        // Get the link status.
        final int[] linkStatus = new int[1];
        GLES31.glGetProgramiv(mProgramHandle, GLES31.GL_LINK_STATUS, linkStatus, 0);

        // If the link failed, delete the program.
        if (linkStatus[0] == 0)
        {
            Log.e("program", GLES31.glGetProgramInfoLog(mProgramHandle));
            GLES31.glDeleteProgram(mProgramHandle);
            mProgramHandle = 0;
        }

        if (mProgramHandle == 0)
        {
            throw new RuntimeException("Error creating program.");
        }
    }

    public void destroy(){
        GLES31.glDeleteProgram(mProgramHandle);
    }

    public void useProgram(){
        GLES31.glUseProgram(mProgramHandle);
    }
}
