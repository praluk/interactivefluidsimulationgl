#version 310 es

// TODO: also for 4D?

precision highp float;
precision mediump image3D;
precision mediump sampler3D;

#define SURFACE_EPS 2.0

layout(binding = 0, location = 0) uniform sampler3D phi;
layout(binding = 1, location = 1) uniform sampler3D gradient;
layout(rgba16f, binding = 0, location = 2) uniform writeonly image3D res;

layout(std140, binding = 1) uniform blinn_phong_data
{
    vec4 reflCol;
    vec4 diffCol;
    vec4 specCol;
    vec4 ambientCol;
    vec4 curvCol;
    int shinniness;
    float maxCurvature;
};

layout(std140, binding = 2) uniform properties
{
    vec3 offset;
    vec3 nextOffset;
    float offsetFrac;
    vec3 downScale;
    vec3 resolution;
    ivec3 dim;
};

float samplePhi(vec3 pos){
    return texture(phi, pos*downScale + offset).x * (1.0 - offsetFrac) + texture(phi, pos*downScale + nextOffset).x * offsetFrac;
}

mat3 hessian(vec3 pos){
    mat3 hess;
    hess[0] = (texture(gradient, vec3(pos.x + resolution.x/2.0, pos.y, pos.z)).xyz - texture(gradient, vec3(pos.x - resolution.x/2.0, pos.y, pos.z)).xyz) / resolution.x;
    hess[1] = (texture(gradient, vec3(pos.x, pos.y + resolution.y/2.0, pos.z)).xyz - texture(gradient, vec3(pos.x, pos.y - resolution.y/2.0, pos.z)).xyz) / resolution.y;
    hess[2] = (texture(gradient, vec3(pos.x, pos.y, pos.z + resolution.z/2.0)).xyz - texture(gradient, vec3(pos.x, pos.y, pos.z - resolution.z/2.0)).xyz) / resolution.z;
    return hess;
}

float curvature(vec3 pos, vec3 grad){
    float qL = length(grad);
    mat3 h = hessian(pos);
    vec3 tx = normalize(cross(vec3(1, 0, 0), grad));
    vec3 ty = normalize(cross(tx, grad));

    return (0.5 * dot(tx, h * tx) + 0.5 * dot(ty, h * ty)) / qL;
}

layout (local_size_x = 4, local_size_y = 4, local_size_z = 4) in;
void main() {
    if( int(gl_GlobalInvocationID.x) < dim.x &&
        int(gl_GlobalInvocationID.y) < dim.y &&
        int(gl_GlobalInvocationID.z) < dim.z ){

        vec3 pos = (vec3(gl_GlobalInvocationID) + 0.5) / vec3(dim);
        if(abs(samplePhi(pos)) < SURFACE_EPS){
            vec3 nor = vec3(0);
            float curv = 0.0;
            vec3 grad = texture(gradient, pos).xyz;
            nor = normalize(grad);
            if(any(isnan(nor)) || any(isinf(nor))){
                nor = vec3(0);
            } else {
                curv = clamp(curvature(pos, grad), 0.0, maxCurvature);
            }
            imageStore(res, ivec3(gl_GlobalInvocationID), vec4(nor, curv));
        }
    }
}