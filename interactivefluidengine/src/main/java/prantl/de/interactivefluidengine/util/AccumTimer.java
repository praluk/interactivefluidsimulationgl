package prantl.de.interactivefluidengine.util;

import android.opengl.GLES10Ext;
import android.opengl.GLES31;
import android.opengl.GLES31Ext;
import android.util.Log;

import java.util.concurrent.CountDownLatch;

import prantl.de.interactivefluidengine.RenderEngine;
import prantl.de.interactivefluidengine.scene.Camera;

/**
 * Created by Lukas on 25.10.17.
 */

public class AccumTimer {
    static{
        try{
            System.loadLibrary("gl_timer");
            System.out.println("Loaded gl_timer");
        }catch(UnsatisfiedLinkError e){
            //nothing to do
            System.out.println("Couldn't load gl_timer");
            System.out.println(e.getMessage());
        }
    }
    public native int createTimeQuery();
    public native void beginTimeQuery(int q);
    public native void endTimeQuery(int q);
    public native int getTime(int q);

    private static volatile boolean started = false;

    private enum State{
        OFF,
        ON,
        FINISHED,
        EVAL
    }

    private volatile State mState = State.OFF;

    private long mEvalPollingTime = 1;
    public interface OnTimeAccum{
        void timeAccum(final float accumTime);
    }
    private int mItersAccum = 0;
    private float mDurationAccum = 0.f;
    private int mMaxIterAccum = -1;
    private float mMaxDurationAccum = -1.f;

    private float mStartTime;
    private float mEndTime;

    private int mQuery = 0;
    private Camera mCamera = null;

    public AccumTimer(float maxDurationAccum){
        mMaxDurationAccum = maxDurationAccum;
    }
    public AccumTimer (int maxIterAccum){
        mMaxIterAccum = maxIterAccum;
    }

    public AccumTimer(float maxDurationAccum, Camera cam){
        mMaxDurationAccum = maxDurationAccum;
        mQuery = createTimeQuery();
        mCamera = cam;
    }
    public AccumTimer (int maxIterAccum, Camera cam){
        mMaxIterAccum = maxIterAccum;
        mQuery = createTimeQuery();
        mCamera = cam;
    }
    public AccumTimer(float maxDurationAccum, Camera cam, long pollingTime){
        mMaxDurationAccum = maxDurationAccum;
        mQuery = createTimeQuery();
        mCamera = cam;
        mEvalPollingTime = pollingTime;
    }
    public AccumTimer (int maxIterAccum, Camera cam, long pollingTime){
        mMaxIterAccum = maxIterAccum;
        mQuery = createTimeQuery();
        mCamera = cam;
        mEvalPollingTime = pollingTime;
    }

    public void setEvalPollingTime(long ms){
        mEvalPollingTime = ms;
    }

    public void start(){
        if(mQuery > 0){
            if(mState != State.OFF){
                return;
            }
            if(started){
                Log.e("AccumTimer", "GPU timer already running! Only one timer at a time allowed.");
                throw new RuntimeException("hoi");
                //return;
            }
            started = true;
            mState = State.ON;
            beginTimeQuery(mQuery);
        } else {
            mStartTime = System.nanoTime();
        }
    }

    public void stop(){
        if(mQuery > 0){
            if(mState != State.ON){
                return;
            }
            if(!started){
                Log.e("AccumTimer", "GPU timer already stopped! Only one timer at a time allowed.");
                return;
            }
            started = false;
            mState = State.FINISHED;
            endTimeQuery(mQuery);

        } else {
            mEndTime = System.nanoTime();
        }
    }

    class EvalTimeThread extends SynchableThread{

        public int t = -1;
        public EvalTimeThread(CountDownLatch endSignal) {
            super(endSignal);
        }

        @Override
        public void _run() {
            t = getTime(mQuery);
        }
    }

    public void eval(final OnTimeAccum onTimeAccum){
        if(mQuery > 0) {
            if(mState != State.FINISHED){
                return;
            }
            mState = State.EVAL;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    int t = -1;
                    EvalTimeThread thread = new EvalTimeThread(new CountDownLatch(1));

                    while (thread.t < 0) {
                        mCamera.queueEvent(thread);
                        try {
                            thread.GetEndSignal().await();
                            Thread.sleep(mEvalPollingTime);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            return;
                        }
                    }
                    mState = State.OFF;
                    update(thread.t / 1e6f, onTimeAccum);
                }
            }).start();
        } else {
            update((mEndTime - mStartTime) / 1e6f, onTimeAccum);
        }
    }
    public void eval(final OnTimeAccum onTimeAccum, boolean blocking){
        if(mQuery > 0) {
            if(mState != State.FINISHED){
                return;
            }
            mState = State.EVAL;

            if(blocking) {
                int t = -1;
                while (t < 0) {
                    t = getTime(mQuery);
                }
                mState = State.OFF;
                update(t / 1e6f, onTimeAccum);
            } else {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int t = -1;
                        EvalTimeThread thread = new EvalTimeThread(new CountDownLatch(1));

                        while (thread.t < 0) {
                            mCamera.queueEvent(thread);
                            try {
                                thread.GetEndSignal().await();
                                Thread.sleep(mEvalPollingTime);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                                return;
                            }
                        }
                        mState = State.OFF;
                        update(thread.t / 1e6f, onTimeAccum);
                    }
                }).start();
            }
        } else {
            update((mEndTime - mStartTime) / 1e6f, onTimeAccum);
        }
    }

    public void tryEval(final OnTimeAccum onTimeAccum){
        if(mQuery > 0) {
            if(mState != State.FINISHED && mState != State.EVAL){
                return;
            }
            mState = State.EVAL;

            int t = getTime(mQuery);
            if(t >= 0){
                mState = State.OFF;
                update(t/1e6f, onTimeAccum);
            }
        } else {
            update((mEndTime - mStartTime) / 1e6f, onTimeAccum);
        }
    }

    public void stopAndEval(final OnTimeAccum onTimeAccum){
        stop();
        eval(onTimeAccum);
    }

    public void stopAndEval(final OnTimeAccum onTimeAccum, boolean blocking){
        stop();
        eval(onTimeAccum, blocking);
    }

    public void stopAndTryEval(final OnTimeAccum onTimeAccum){
        stop();
        tryEval(onTimeAccum);
    }

    public void update(float dt, OnTimeAccum onTimeAccum) {
        mDurationAccum += dt;
        mItersAccum++;

        if (mDurationAccum > mMaxDurationAccum && mItersAccum > mMaxIterAccum) {
            final float dur = mDurationAccum / mItersAccum;
            mDurationAccum = 0;
            mItersAccum = 0;

            onTimeAccum.timeAccum(dur);
        }
    }
}
