package prantl.de.interactivefluidengine.scene.Component;

import android.opengl.GLES31;
import android.util.Log;
import android.util.Pair;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Vector;

import prantl.de.interactivefluidengine.PhysicEngine;
import prantl.de.interactivefluidengine.resources.Shader;
import prantl.de.interactivefluidengine.resources.Texture;
import prantl.de.interactivefluidengine.resources.Texture3D;
import prantl.de.interactivefluidengine.util.GLHelper;
import prantl.de.interactivefluidengine.util.GLMath;
import prantl.de.interactivefluidengine.util.IO;
import prantl.de.interactivefluidengine.util.Location;
import prantl.de.interactivefluidengine.util.MeshGenerator;

/**
 * Created by Lukas on 30.05.17.
 */

public class ParticlePhysicComponent extends IPhysicComponent {

    private static Location VOLUME_TEX_LOC = new Location(0, 0);
    private static Location OLD_PHI_TEX_LOC = new Location(1, 1);
    private static Location NOR_AND_CURV_LOC = new Location(2, 2);

    private static Location PARTICLE_POS_IN_LOC = new Location(1, 1);
    private static Location PARTICLE_VEL_IN_LOC = new Location(2, 2);

    private static Location PARTICLE_POS_LOC = new Location(3, 3);
    private static Location PARTICLE_VEL_LOC = new Location(4, 4);
    private static Location OLD_PHI_OUT_LOC = new Location(5, 5);

    private static Location BUFFER_HEAD_LOC = new Location(0, 0);

    private static final Location PROP_BUFFER_LOC = new Location(1, 1);
    private static final Location DYN_PROP_BUFFER_LOC = new Location(2, 2);

    private static final int PROP_BYTE_SIZE = 48;
    private static final int DYN_PROP_BYTE_SIZE = 32;


    private static Shader mInitProgram;
    private static Shader mUpdateProgram;

    private static Vector<IPhysicComponent> mPhysicComponents = new Vector<>();

    private boolean mGenParticles = true;
    private boolean mValidOld = false;
    private int mParticleCount;
    private Texture mParticlePos;
    private Texture mParticleVel;
    private Texture3D mVolume;
    private Texture3D mOldVolume;
    private Texture3D mNorAndCurv;
    private int mBufferHeadHandle;

    int mPropBuffer;
    int mDynamicPropBuffer;

    private float mCurvThres;
    private float mCurvRange;
    private float mEnergyThres;
    private float mEnergyRange;
    private int mMaxCount;
    private float mMaxScale;

    private float mLiveStep;
    private float mAccDamping;
    private float mBuoyancy;
    private float mSpeedFactor;

    private int[] mBoundary;

    public ParticlePhysicComponent(Texture3D volume, Texture3D norAndCurv, int particleCount) {
        super();
        mParticleCount = particleCount;
        mVolume = volume;
        mNorAndCurv = norAndCurv;

        int[] dim = mVolume.getDim();
        mOldVolume = new Texture3D(GLES31.GL_RGBA16F, dim[0], dim[1], dim[2]);

        int w = Math.min(mParticleCount, GLHelper.MAX_TEX_SIZE_2D);
        int h = GLMath.roundUp(particleCount, GLHelper.MAX_TEX_SIZE_2D);

        mParticlePos = new Texture(GLES31.GL_RGBA16F, w, h);
        mParticleVel = new Texture(GLES31.GL_RGBA16F, w, h);

        FloatBuffer fb = FloatBuffer.allocate(w * h * 4);
        for(int i = 0; i < w * h * 4; i++){
            fb.put(0);
        }

        mParticlePos.bindTexture();
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_MIN_FILTER, GLES31.GL_NEAREST);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_MAG_FILTER, GLES31.GL_NEAREST);

        fb.rewind();
        GLES31.glTexSubImage2D(GLES31.GL_TEXTURE_2D, 0, 0, 0, w, h, GLES31.GL_RGBA, GLES31.GL_FLOAT, fb);
        mParticlePos.unbindTexture();

        mParticleVel.bindTexture();
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_MIN_FILTER, GLES31.GL_NEAREST);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_MAG_FILTER, GLES31.GL_NEAREST);

        fb.rewind();
        GLES31.glTexSubImage2D(GLES31.GL_TEXTURE_2D, 0, 0, 0, w, h, GLES31.GL_RGBA, GLES31.GL_FLOAT, fb);
        mParticleVel.unbindTexture();

        int[] res = new int[1];
        GLES31.glGenBuffers(1, res, 0);
        mBufferHeadHandle = res[0];

        GLES31.glBindBuffer(GLES31.GL_ATOMIC_COUNTER_BUFFER, mBufferHeadHandle);
        GLES31.glBufferData(GLES31.GL_ATOMIC_COUNTER_BUFFER, 4, null, GLES31.GL_DYNAMIC_DRAW);
        GLES31.glBindBuffer(GLES31.GL_ATOMIC_COUNTER_BUFFER, 0);

        mBoundary = mVolume.getBoundary();

        final int[] ubo = new int[2];
        GLES31.glGenBuffers(2, ubo, 0);
        mPropBuffer = ubo[0];
        mDynamicPropBuffer = ubo[1];

        setCurvProperties(3, 5);
        setEnergyProperties(.1f, .5f);
        setParticleProperties(4000, 0.05f);

        setLiveStep(0.04f);
        setBuoyancy(-PhysicEngine.GRAVITY * 0.5f);
        setAccDamping(0.04f);
        setSpeedFactor(.4f);
    }

    protected void updatePropBuffer(){
        ByteBuffer init = ByteBuffer.allocate(PROP_BYTE_SIZE).order(ByteOrder.nativeOrder());
        init.putInt(mParticleCount);
        init.putFloat(mLiveStep);
        init.putFloat(PhysicEngine.GRAVITY);
        init.putFloat(mBuoyancy);
        init.putFloat(mAccDamping);
        init.putFloat(mSpeedFactor);
        init.putFloat(mCurvThres);
        init.putFloat(mCurvRange);
        init.putFloat(mEnergyThres);
        init.putFloat(mEnergyRange);
        init.putInt(mMaxCount);
        init.putFloat(mMaxScale);
        init.rewind();

        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, mPropBuffer);
        GLES31.glBufferData(GLES31.GL_UNIFORM_BUFFER, PROP_BYTE_SIZE, init, GLES31.GL_DYNAMIC_DRAW);
        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, 0);

        GLHelper.CheckGLError();
    }

    protected void setDynamicPropBuffer(float dt, int[] dim, int[] boundary){
        ByteBuffer init = ByteBuffer.allocate(DYN_PROP_BYTE_SIZE).order(ByteOrder.nativeOrder());
        for(int i = 0; i < 3; i++) init.putInt(dim[i]);
        init.putFloat(dt);
        for(int i = 0; i < 3; i++) init.putInt(boundary[i]);
        init.putFloat(0);
        init.rewind();

        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, mDynamicPropBuffer);
        GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, DYN_PROP_BUFFER_LOC.bind, mDynamicPropBuffer);
        GLES31.glBufferData(GLES31.GL_UNIFORM_BUFFER, DYN_PROP_BYTE_SIZE, init, GLES31.GL_DYNAMIC_DRAW);
        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, 0);

        GLHelper.CheckGLError();
    }


    public void setCurvProperties(float thres, float range) {
        mCurvThres = thres;
        mCurvRange = range;
        updatePropBuffer();
    }

    public void setEnergyProperties(float thres, float range) {
        mEnergyThres = thres;
        mEnergyRange = range;
        updatePropBuffer();
    }

    public void setParticleProperties(int maxCount, float maxScale) {
        mMaxCount = maxCount;
        mMaxScale = maxScale;
        updatePropBuffer();
    }

    public void setLiveStep(float liveStep) {
        mLiveStep = liveStep;
        updatePropBuffer();
    }

    public void setBuoyancy(float buoyancy) {
        mBuoyancy = buoyancy;
        updatePropBuffer();
    }

    public void setAccDamping(float accDamping) {
        mAccDamping = accDamping;
        updatePropBuffer();
    }

    public void setSpeedFactor(float speedFactor) {
        mSpeedFactor = speedFactor;
        updatePropBuffer();
    }

    public void setBoundary(int[] boundary) {
        mBoundary = boundary;
        updatePropBuffer();
    }

    public Texture getParticlePos() {
        return mParticlePos;
    }

    public Texture getParticleVel() {
        return mParticleVel;
    }

    public int getParticleCount() {
        return mParticleCount;
    }

    public static void init() {
        ArrayList<Pair<Integer, String>> shaders = new ArrayList<>();
        shaders.add(Pair.create(GLES31.GL_COMPUTE_SHADER, "particle_init"));
        mInitProgram = IO.loadShaderFromFile(shaders, false);

        shaders.clear();
        shaders.add(Pair.create(GLES31.GL_COMPUTE_SHADER, "particle_update"));
        mUpdateProgram = IO.loadShaderFromFile(shaders, false);

        mPhysicComponents.clear();
    }

    public static void updateAll(float dt) {
        for (IPhysicComponent pc : mPhysicComponents) {
            if (pc.isEnabled()) {
                pc.update(dt);
            }
        }
    }

    public void genParticles(boolean active) {
        mGenParticles = active;
        if (!active) mValidOld = false;
    }

    @Override
    public void update(float dt) {
        dt /= 1000.f;

        // bind images
        GLES31.glBindImageTexture(PARTICLE_POS_LOC.bind, mParticlePos.getTextureHandle(), 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);
        GLES31.glBindImageTexture(PARTICLE_VEL_LOC.bind, mParticleVel.getTextureHandle(), 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);

        int[] dim = mVolume.getDim();
        setDynamicPropBuffer(dt, dim, mBoundary);

        GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, PROP_BUFFER_LOC.bind, mPropBuffer);

        if (mGenParticles) {
            mInitProgram.useProgram();

            // bind volume textures
            GLES31.glBindImageTexture(VOLUME_TEX_LOC.bind, mVolume.getTextureHandle(), 0, true, 0, GLES31.GL_READ_ONLY, GLES31.GL_RGBA16F);
            GLES31.glBindImageTexture(OLD_PHI_TEX_LOC.bind, mValidOld ? mOldVolume.getTextureHandle() : mVolume.getTextureHandle(), 0, true, 0, GLES31.GL_READ_ONLY, GLES31.GL_RGBA16F);
            GLES31.glBindImageTexture(NOR_AND_CURV_LOC.bind, mNorAndCurv.getTextureHandle(), 0, true, 0, GLES31.GL_READ_ONLY, GLES31.GL_RGBA16F);

            GLES31.glBindImageTexture(OLD_PHI_OUT_LOC.bind, mOldVolume.getTextureHandle(), 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);

            GLES31.glBindBufferBase(GLES31.GL_ATOMIC_COUNTER_BUFFER, BUFFER_HEAD_LOC.bind, mBufferHeadHandle);

            // compute
            GLES31.glDispatchCompute(GLMath.roundUp(dim[0], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[1], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[2], GLHelper.DISPATCH_GROUP_SIZE));

            // unbind
            GLES31.glBindBufferBase(GLES31.GL_ATOMIC_COUNTER_BUFFER, BUFFER_HEAD_LOC.bind, 0);
            GLES31.glBindImageTexture(OLD_PHI_OUT_LOC.bind, 0, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);

            GLES31.glBindImageTexture(VOLUME_TEX_LOC.bind, 0, 0, true, 0, GLES31.GL_READ_ONLY, GLES31.GL_RGBA16F);
            GLES31.glBindImageTexture(OLD_PHI_TEX_LOC.bind, 0, 0, true, 0, GLES31.GL_READ_ONLY, GLES31.GL_RGBA16F);
            GLES31.glBindImageTexture(NOR_AND_CURV_LOC.bind, 0, 0, true, 0, GLES31.GL_READ_ONLY, GLES31.GL_RGBA16F);

            GLES31.glMemoryBarrier(GLES31.GL_COMPUTE_SHADER_BIT);

            mValidOld = true;
        }

        mUpdateProgram.useProgram();

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[VOLUME_TEX_LOC.bind]);
        mVolume.bindTexture();
        GLES31.glUniform1i(VOLUME_TEX_LOC.handle, VOLUME_TEX_LOC.bind);

        GLES31.glBindImageTexture(PARTICLE_POS_IN_LOC.bind, mParticlePos.getTextureHandle(), 0, true, 0, GLES31.GL_READ_ONLY, GLES31.GL_RGBA16F);
        GLES31.glBindImageTexture(PARTICLE_VEL_IN_LOC.bind, mParticleVel.getTextureHandle(), 0, true, 0, GLES31.GL_READ_ONLY, GLES31.GL_RGBA16F);

        // compute
        GLES31.glDispatchCompute(GLMath.roundUp(Math.min(mParticleCount, GLHelper.MAX_TEX_SIZE_2D), GLHelper.DISPATCH_GROUP_SIZE_2D), GLMath.roundUp(GLMath.roundUp(mParticleCount, GLHelper.MAX_TEX_SIZE_2D), GLHelper.DISPATCH_GROUP_SIZE_2D), 1);

        // unbind
        GLES31.glBindImageTexture(PARTICLE_POS_IN_LOC.bind, 0, 0, true, 0, GLES31.GL_READ_ONLY, GLES31.GL_RGBA16F);
        GLES31.glBindImageTexture(PARTICLE_VEL_IN_LOC.bind, 0, 0, true, 0, GLES31.GL_READ_ONLY, GLES31.GL_RGBA16F);
        GLES31.glBindImageTexture(PARTICLE_POS_LOC.bind, 0, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);
        GLES31.glBindImageTexture(PARTICLE_VEL_LOC.bind, 0, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);
        mVolume.unbindTexture();

        GLES31.glMemoryBarrier(GLES31.GL_COMPUTE_SHADER_BIT);

        GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, mPropBuffer, 0);
        GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, mDynamicPropBuffer, 0);
    }

    @Override
    protected Vector<IPhysicComponent> getIPhysicComponentVector() {
        return mPhysicComponents;
    }
}
