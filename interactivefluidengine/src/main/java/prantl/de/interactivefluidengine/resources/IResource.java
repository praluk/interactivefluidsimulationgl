package prantl.de.interactivefluidengine.resources;

import prantl.de.interactivefluidengine.RenderEngine;
import prantl.de.interactivefluidengine.scene.Camera;
import prantl.de.interactivefluidengine.util.IO;

/**
 * Created by Lukas on 09.06.17.
 */

// graphics resource, which will be initialized after the first OnSurfaceCreate
public interface IResource {
    public void destroy();
}
