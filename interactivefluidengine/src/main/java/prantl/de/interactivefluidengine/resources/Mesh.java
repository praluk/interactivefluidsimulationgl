package prantl.de.interactivefluidengine.resources;

import android.opengl.GLES31;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import prantl.de.interactivefluidengine.scene.Camera;
import prantl.de.interactivefluidengine.util.GLHelper;
import prantl.de.interactivefluidengine.util.IO;

/**
 * Created by Lukas on 09.06.17.
 */

public class Mesh implements IResource{
    private static final int POSITION_HANDLE = 0;
    private static final int TEX_COORD_HANDLE = 1;
    private static final int NORMAL_HANDLE = 2;
    private static final int TANGENT_HANDLE = 3;

    private static final int POSITION_ELEM_SIZE = 3;
    private static final int TEX_COORD_ELEM_SIZE = 2;
    private static final int NORMAL_ELEM_SIZE = 3;
    private static final int TANGENT_ELEM_SIZE = 3;

    private static final int BYTE_STRIDE = (POSITION_ELEM_SIZE + NORMAL_ELEM_SIZE + TEX_COORD_ELEM_SIZE + TANGENT_ELEM_SIZE) * 4;

    public int mVertexBuffer;
    public int mIndexBuffer;

    private int mIndexCount;

    // have to be created in an opengl thread!
    public Mesh(final float[] vertices, final short[] indices){
        FloatBuffer vBuffer = ByteBuffer
                .allocateDirect(vertices.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        vBuffer.put(vertices);
        vBuffer.position(0);

        ShortBuffer iBuffer = ByteBuffer
                .allocateDirect(indices.length * 2)
                .order(ByteOrder.nativeOrder())
                .asShortBuffer();
        iBuffer.put(indices);
        iBuffer.position(0);

        mIndexCount = iBuffer.capacity();

        final int buffers[] = new int[2];
        GLES31.glGenBuffers(2, buffers, 0);

        mVertexBuffer = buffers[0];
        mIndexBuffer = buffers[1];

        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER, mVertexBuffer);
        GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER, vBuffer.capacity() * 4, vBuffer, GLES31.GL_STATIC_DRAW);
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER, 0);

        GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
        GLES31.glBufferData(GLES31.GL_ELEMENT_ARRAY_BUFFER, iBuffer.capacity() * 2, iBuffer, GLES31.GL_STATIC_DRAW);
        GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER, 0);
        GLHelper.CheckGLError();
    }

    public void draw(){
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER, mVertexBuffer);

        GLES31.glEnableVertexAttribArray(POSITION_HANDLE);
        GLES31.glVertexAttribPointer(POSITION_HANDLE, POSITION_ELEM_SIZE, GLES31.GL_FLOAT, false, BYTE_STRIDE, 0);

        GLHelper.CheckGLError();
        GLES31.glEnableVertexAttribArray(TEX_COORD_HANDLE);
        GLES31.glVertexAttribPointer(TEX_COORD_HANDLE, TEX_COORD_ELEM_SIZE, GLES31.GL_FLOAT, false, BYTE_STRIDE, POSITION_ELEM_SIZE * 4);

        GLHelper.CheckGLError();
        GLES31.glEnableVertexAttribArray(NORMAL_HANDLE);
        GLES31.glVertexAttribPointer(NORMAL_HANDLE, NORMAL_ELEM_SIZE, GLES31.GL_FLOAT, false, BYTE_STRIDE, (POSITION_ELEM_SIZE + TEX_COORD_ELEM_SIZE) * 4);

        GLHelper.CheckGLError();
        GLES31.glEnableVertexAttribArray(TANGENT_HANDLE);
        GLES31.glVertexAttribPointer(TANGENT_HANDLE, TANGENT_ELEM_SIZE, GLES31.GL_FLOAT, false, BYTE_STRIDE, (POSITION_ELEM_SIZE + TEX_COORD_ELEM_SIZE + NORMAL_ELEM_SIZE) * 4);

        GLHelper.CheckGLError();
        GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);

        GLES31.glDrawElements(GLES31.GL_TRIANGLES, mIndexCount, GLES31.GL_UNSIGNED_SHORT, 0);

        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER, 0);
        GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER, 0);

        GLHelper.CheckGLError();
    }

    public void drawInstanced(int count){
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER, mVertexBuffer);

        GLES31.glEnableVertexAttribArray(POSITION_HANDLE);
        GLES31.glVertexAttribPointer(POSITION_HANDLE, POSITION_ELEM_SIZE, GLES31.GL_FLOAT, false, BYTE_STRIDE, 0);

        GLHelper.CheckGLError();
        GLES31.glEnableVertexAttribArray(TEX_COORD_HANDLE);
        GLES31.glVertexAttribPointer(TEX_COORD_HANDLE, TEX_COORD_ELEM_SIZE, GLES31.GL_FLOAT, false, BYTE_STRIDE, POSITION_ELEM_SIZE * 4);

        GLHelper.CheckGLError();
        GLES31.glEnableVertexAttribArray(NORMAL_HANDLE);
        GLES31.glVertexAttribPointer(NORMAL_HANDLE, NORMAL_ELEM_SIZE, GLES31.GL_FLOAT, false, BYTE_STRIDE, (POSITION_ELEM_SIZE + TEX_COORD_ELEM_SIZE) * 4);

        GLHelper.CheckGLError();
        GLES31.glEnableVertexAttribArray(TANGENT_HANDLE);
        GLES31.glVertexAttribPointer(TANGENT_HANDLE, TANGENT_ELEM_SIZE, GLES31.GL_FLOAT, false, BYTE_STRIDE, (POSITION_ELEM_SIZE + TEX_COORD_ELEM_SIZE + NORMAL_ELEM_SIZE) * 4);

        GLHelper.CheckGLError();
        GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);

        GLES31.glDrawElementsInstanced(GLES31.GL_TRIANGLES, mIndexCount, GLES31.GL_UNSIGNED_SHORT, 0, count);

        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER, 0);
        GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER, 0);

        GLHelper.CheckGLError();
    }

    public void destroy(){
        GLES31.glDeleteBuffers(1, new int[]{mVertexBuffer, mIndexBuffer}, 0);
    }
}
