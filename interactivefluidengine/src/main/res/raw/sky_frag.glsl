#version 310 es

precision mediump float;

layout(location = 32, binding = 10) uniform samplerCube sky;

vec4 sampleSkyMap(vec3 dir){
    return vec4(texture(sky, dir).rgb, 1.0);
}

in vec3 uv;

out vec4 color;

void main() {
    color = sampleSkyMap(uv);
}
