#version 310 es

// TODO: also for 4D?

precision highp float;
precision mediump image3D;
precision mediump sampler3D;

#define SURFACE_EPS 2.0

layout(binding = 0, location = 0) uniform sampler3D phi;
layout(rgba16f, binding = 0, location = 2) uniform writeonly image3D res;

layout(std140, binding = 2) uniform properties
{
    vec3 offset;
    vec3 nextOffset;
    float offsetFrac;
    vec3 downScale;
    vec3 resolution;
    ivec3 dim;
};

float sampleTexture(vec3 pos){
    return texture(phi, pos*downScale + offset).r * (1.0 - offsetFrac) + texture(phi, pos*downScale + nextOffset).r * offsetFrac;
}

vec3 gradient(vec3 pos){
    vec3 grad;
    grad.x = (sampleTexture(vec3(pos.x + resolution.x/2.0, pos.y, pos.z)) - sampleTexture(vec3(pos.x - resolution.x/2.0, pos.y, pos.z))) / resolution.x;
    grad.y = (sampleTexture(vec3(pos.x, pos.y + resolution.y/2.0, pos.z)) - sampleTexture(vec3(pos.x, pos.y - resolution.y/2.0, pos.z))) / resolution.y;
    grad.z = (sampleTexture(vec3(pos.x, pos.y, pos.z + resolution.z/2.0)) - sampleTexture(vec3(pos.x, pos.y, pos.z - resolution.z/2.0))) / resolution.z;
    return grad;
}

layout (local_size_x = 4, local_size_y = 4, local_size_z = 4) in;
void main() {
    if( int(gl_GlobalInvocationID.x) < dim.x &&
        int(gl_GlobalInvocationID.y) < dim.y &&
        int(gl_GlobalInvocationID.z) < dim.z ){

        vec3 pos = (vec3(gl_GlobalInvocationID) + 0.5) / vec3(dim);
        if(abs(sampleTexture(pos)) < SURFACE_EPS){
            imageStore(res, ivec3(gl_GlobalInvocationID), vec4(gradient(pos), 0));
        }
    }
}