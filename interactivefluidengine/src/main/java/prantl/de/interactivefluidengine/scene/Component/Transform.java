package prantl.de.interactivefluidengine.scene.Component;

import android.opengl.Matrix;

import prantl.de.interactivefluidengine.util.GLMath;

/**
 * Created by Lukas on 30.05.17.
 */

public class Transform {
    // position/rotation/scale matrices
    private float[] mPosition = new float[]{ 0, 0, 0 };
    private float[] mRotation = new float[16];
    private float[] mScale = new float[]{ 1, 1, 1 };

    public final static float[] FWD = new float[]{ 0, 0, -1 };
    public final static float[] UP = new float[]{ 0, 1, 0 };
    public final static float[] RIGHT = new float[]{ 1, 0, 0 };

    private float[] mForward = FWD.clone();
    private float[] mUp = UP.clone();
    private float[] mRight = RIGHT.clone();

    // model matrices
    private float[] mLocalToWorld = new float[16];
    private float[] mWorldToLocal = new float[16];

    public Transform(){
        Matrix.setIdentityM(mRotation, 0);
        updateMatrices();
    }

    public final float[] getForward(){return mForward;}
    public final float[] getUp(){return mUp;}
    public final float[] getRight(){return mRight;}

    public final float[] getPosition(){
        return mPosition;
    }

    public final float[] getScale(){
        return mScale;
    }

    public void setPosition(float x, float y, float z){
        mPosition[0] = x;
        mPosition[1] = y;
        mPosition[2] = z;
        updateMatrices();
    }

    public void setRotation(float x, float y, float z){
        Matrix.setIdentityM(mRotation, 0);
        // rotation order: x -> z -> y
        yaw(y);
        pitch(x);
        roll(z);
    }

    public void setScale(float x, float y, float z){
        mScale[0] = x;
        mScale[1] = y;
        mScale[2] = z;
        updateMatrices();
    }

    public void lookAt(float x, float y, float z){
        mRotation = GLMath.lookAt(x, y, z, mPosition[0], mPosition[1], mPosition[2], UP[0], UP[1], UP[2]);
        updateMatrices();
    }

    public void lookAt(final Transform other){
        final float[] p = other.getPosition();
        lookAt(p[0], p[1], p[2]);
    }

    public void translate(float x, float y, float z){
        setPosition(mPosition[0] + x, mPosition[1] + y, mPosition[2] + z);
    }

    public void rotate(float angle, float x, float y, float z){
        Matrix.rotateM(mRotation, 0, angle, x, y, z);
        updateMatrices();
    }

    public void scale(float x, float y, float z){
        setScale(mScale[0] * x, mScale[1] * y, mScale[2] * z);
    }

    public void walk(float step){
        translate(mForward[0] * step, mForward[1] * step, mForward[2] * step);
    }
    public void strafe(float step){
        translate(mRight[0] * step, mRight[1] * step, mRight[2] * step);
    }
    public void fly(float step){
        translate(mUp[0] * step, mUp[1] * step, mUp[2] * step);
    }

    public void roll(float angle){
        rotate(angle, mForward[0], mForward[1], mForward[2]);
    }
    public void pitch(float angle){
        rotate(angle, mRight[0], mRight[1], mRight[2]);
    }
    public void yaw(float angle){
        rotate(angle, mUp[0], mUp[1], mUp[2]);
    }

    public final float[] getLocalToWorld(){
        return mLocalToWorld;
    }

    public final float[] getWorldToLocal(){
        return mWorldToLocal;
    }

    private float[] translateM(){
        float[] m = new float[16];
        Matrix.setIdentityM(m, 0);
        Matrix.translateM(m, 0, mPosition[0], mPosition[1], mPosition[2]);
        return m;
    }
    private float[] scaleM(){
        float[] m = new float[16];
        Matrix.setIdentityM(m, 0);
        Matrix.scaleM(m, 0, mScale[0], mScale[1], mScale[2]);
        return m;
    }
    private float[] invTranslateM(){
        float[] m = new float[16];
        Matrix.setIdentityM(m, 0);
        Matrix.translateM(m, 0, -mPosition[0], -mPosition[1], -mPosition[2]);
        return m;
    }
    private float[] invScaleM(){
        float[] m = new float[16];
        Matrix.setIdentityM(m, 0);
        Matrix.scaleM(m, 0, 1/mScale[0], 1/mScale[1], 1/mScale[2]);
        return m;
    }
    private void updateMatrices(){
        float[] tmp = new float[16];
        Matrix.multiplyMM(tmp, 0, translateM(), 0, mRotation, 0);
        Matrix.multiplyMM(mLocalToWorld, 0, tmp, 0, scaleM(), 0);

        float[] m = new float[16];
        Matrix.transposeM(m, 0, mRotation, 0);
        Matrix.multiplyMM(tmp, 0, invScaleM(), 0, m, 0);
        Matrix.multiplyMM(mWorldToLocal, 0, tmp, 0, invTranslateM(), 0);

        for(int i = 0; i < 3; i++){ mRight[i] = mRotation[i]; }
        mRight[2] *= -1;
        for(int i = 0; i < 3; i++){ mUp[i] = mRotation[i+4]; }
        mUp[2] *= -1;
        for(int i = 0; i < 3; i++){ mForward[i] = mRotation[i+8]; }
        mForward[2] *= -1;
    }
}
