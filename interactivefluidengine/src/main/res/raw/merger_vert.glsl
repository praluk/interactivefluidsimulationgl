#version 310 es

layout(location = 0) in vec4 pos;

out vec2 uv;

void main() {
   uv = vec2(pos.x + 0.5, -pos.z + 0.5);
   gl_Position = vec4(pos.x * 2.0, -pos.z * 2.0, 0.0, 1.0);
}