#version 310 es

#define MAX_LIGHT_COUNT 10

struct LightData{
    vec3 pos;
    vec3 col;
};

layout(location = 0) in vec4 pos;
layout(location = 1) in vec2 tex_coord;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec3 tangent;

layout(location = 2) uniform mat4 mv;

layout(std140, binding = 0) uniform cam_data
{
    mat4 proj;
    int lightCount;
    LightData light[MAX_LIGHT_COUNT];
};

out vec3 uv;
out vec3 view_pos;

void main() {
   view_pos = vec3(mv * pos).xyz;
   gl_Position = proj * vec4(view_pos, 1);

   uv = pos.xyz + 0.5;
}