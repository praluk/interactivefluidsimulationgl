package prantl.de.interactivefluidengine.util;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import prantl.de.interactivefluidengine.resources.Mesh;
import prantl.de.interactivefluidengine.scene.Camera;

/**
 * Created by Lukas on 09.06.17.
 */

public class MeshGenerator {
    private static final float CUBE_VERTICES[] = {
            // front
            -.5f, -.5f, -.5f,
            +0.25f, +0.25f,
            +0.f, +0.f, -1.f,
            +1.f, +0.f, +0.f,

            -.5f, +.5f, -.5f,
            +0.25f, +0.5f,
            +0.f, +0.f, -1.f,
            +1.f, +0.f, +0.f,

            +.5f, -.5f, -.5f,
            +0.5f, +0.25f,
            +0.f, +0.f, -1.f,
            +1.f, +0.f, +0.f,

            +.5f, +.5f, -.5f,
            +0.5f, +0.5f,
            +0.f, +0.f, -1.f,
            +1.f, +0.f, +0.f,

            // back
            -.5f, -.5f, +.5f,
            +1.0f, +0.25f,
            +0.f, +0.f, +1.f,
            -1.f, +0.f, +0.f,

            +.5f, -.5f, +.5f,
            +0.75f, +0.25f,
            +0.f, +0.f, +1.f,
            -1.f, +0.f, +0.f,

            -.5f, +.5f, +.5f,
            +1.0f, +0.5f,
            +0.f, +0.f, +1.f,
            -1.f, +0.f, +0.f,

            +.5f, +.5f, +.5f,
            +0.75f, +0.5f,
            +0.f, +0.f, +1.f,
            -1.f, +0.f, +0.f,

            // left
            -.5f, +.5f, +.5f,
            +0.f, +0.5f,
            -1.f, +0.f, +0.f,
            +0.f, +0.f, -1.f,

            -.5f, +.5f, -.5f,
            +0.25f, +0.5f,
            -1.f, +0.f, +0.f,
            +0.f, +0.f, -1.f,

            -.5f, -.5f, +.5f,
            +0.f, +0.25f,
            -1.f, +0.f, +0.f,
            +0.f, +0.f, -1.f,

            -.5f, -.5f, -.5f,
            +0.25f, +0.25f,
            -1.f, +0.f, +0.f,
            +0.f, +0.f, -1.f,

            // right
            +.5f, +.5f, +.5f,
            +0.75f, +0.5f,
            +1.f, +0.f, +0.f,
            +0.f, +0.f, +1.f,

            +.5f, -.5f, +.5f,
            +0.75f, +0.25f,
            +1.f, +0.f, +0.f,
            +0.f, +0.f, +1.f,

            +.5f, +.5f, -.5f,
            +0.5f, +0.5f,
            +1.f, +0.f, +0.f,
            +0.f, +0.f, +1.f,

            +.5f, -.5f, -.5f,
            +0.5f, +0.25f,
            +1.f, +0.f, +0.f,
            +0.f, +0.f, +1.f,

            // bottom
            +.5f, -.5f, +.5f,
            +0.5f, +0.f,
            +0.f, -1.f, +0.f,
            +1.f, +0.f, +0.f,

            -.5f, -.5f, +.5f,
            +0.25f, +0.f,
            +0.f, -1.f, +0.f,
            +1.f, +0.f, +0.f,

            +.5f, -.5f, -.5f,
            +0.5f, +0.25f,
            +0.f, -1.f, +0.f,
            +1.f, +0.f, +0.f,

            -.5f, -.5f, -.5f,
            +0.25f, +0.25f,
            +0.f, -1.f, +0.f,
            +1.f, +0.f, +0.f,

            // top
            +.5f, +.5f, +.5f,
            +0.5f, +0.75f,
            +0.f, +1.f, +0.f,
            +1.f, +0.f, +0.f,

            +.5f, +.5f, -.5f,
            +0.5f, +0.5f,
            +0.f, +1.f, +0.f,
            +1.f, +0.f, +0.f,

            -.5f, +.5f, +.5f,
            +0.25f, +0.75f,
            +0.f, +1.f, +0.f,
            +1.f, +0.f, +0.f,

            -.5f, +.5f, -.5f,
            +0.25f, +0.5f,
            +0.f, +1.f, +0.f,
            +1.f, +0.f, +0.f,
    };

    private static final short CUBE_INDICES[] = {
            // back
            0, 1, 2,
            1, 3, 2,

            // front
            4, 5, 6,
            5, 7, 6,

            // left
            8, 9, 10,
            9, 11, 10,

            // right
            12, 13, 14,
            13, 15, 14,

            // bottom
            16, 17, 18,
            17, 19, 18,

            //top
            20, 21, 22,
            21, 23, 22
    };

    private static final float PLANE_VERTICES[] = {
            +.5f, +0.f, +.5f,
            +1.f, +0.f,
            +0.f, +1.f, +0.f,
            +1.f, 0.f, 0.f,

            +.5f, +0.f, -.5f,
            +1.f, +1.f,
            +0.f, +1.f, +0.f,
            +1.f, 0.f, 0.f,

            -.5f, +0.f, +.5f,
            +0.f, +0.f,
            +0.f, +1.f, +0.f,
            +1.f, 0.f, 0.f,

            -.5f, +0.f, -.5f,
            +0.f, +1.f,
            +0.f, +1.f, +0.f,
            +1.f, 0.f, 0.f,
    };

    private static final short PLANE_INDICES[] = {
            0, 1, 2,
            1, 3, 2
    };

    // TODO: make cache?!
    public static Mesh GetCubeMesh(){ return new Mesh(CUBE_VERTICES, CUBE_INDICES); }
    public static Mesh GetPlaneMesh(){ return new Mesh(PLANE_VERTICES, PLANE_INDICES); }
}
