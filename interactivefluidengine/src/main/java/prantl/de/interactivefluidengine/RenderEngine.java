package prantl.de.interactivefluidengine;

import android.opengl.GLES31;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;
import android.util.Pair;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Vector;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import prantl.de.interactivefluidengine.resources.CubeTexture;
import prantl.de.interactivefluidengine.resources.Mesh;
import prantl.de.interactivefluidengine.resources.Shader;
import prantl.de.interactivefluidengine.resources.Texture;
import prantl.de.interactivefluidengine.resources.VolumeTexture;
import prantl.de.interactivefluidengine.scene.Camera;
import prantl.de.interactivefluidengine.scene.Component.IRenderComponent;
import prantl.de.interactivefluidengine.scene.Component.SolidRenderComponent;
import prantl.de.interactivefluidengine.scene.Component.ParticleRenderComponent;
import prantl.de.interactivefluidengine.scene.Component.VolumeRenderComponent;
import prantl.de.interactivefluidengine.util.AccumTimer;
import prantl.de.interactivefluidengine.util.GLHelper;
import prantl.de.interactivefluidengine.util.IO;
import prantl.de.interactivefluidengine.util.Location;
import prantl.de.interactivefluidengine.util.MeshGenerator;

/**
 * Created by Lukas on 30.05.17.
 */

public class RenderEngine implements GLSurfaceView.Renderer {

    public interface Updateable{
        void awake();
        void update(float dt);
    }

    public static boolean TIME_MEASUREMENT = false;
    private AccumTimer mAccumTimer;
    private AccumTimer mParticleTimer;

    Updateable mSceneUpdateCallback;

    private PhysicEngine mPhysicEngine;

    private long prevFrameTimestamp = -1;

    float mDt = 1.f;

    float mZoom = 1.f;

    private static final int MV_HANDLE = 0;

    private static final Location CAM_BUFFER_LOC = new Location(0, 0);

    private static final Location SOLID_COL_LOC = new Location(0, 0);
    private static final Location VOLUME_COL_LOC = new Location(1, 1);
    private static final Location VOLUME_DEP_LOC = new Location(2, 2);
    private static final Location VOLUME_DISP_LOC = new Location(3, 3);

    public static final Location SKY_MAP_LOC = new Location(32, 10);

    private static final int CAM_BYTE_SIZE = 80 + Camera.MAX_LIGHT_COUNT * 32;

    private float[] mProjMatrix = new float[16];

    private CubeTexture mSkyTexture;

    private Shader mMerger;
    private Shader mSky;

    private Mesh mScreenPlane;
    private Mesh mSkyBox;

    private boolean mSkyEnabled = true;

    private boolean mEnabled = true;

    private Camera mCamera;

    private int mCamBuffer;

    public RenderEngine(Camera camera){
        mCamera = camera;
    }

    public void setEnabled(boolean enabled){
        mEnabled = enabled;
    }

    public boolean isEnabled(){
        return mEnabled;
    }

    public void setSceneUpdateCallback(Updateable sceneUpdateCallback){
        mSceneUpdateCallback = sceneUpdateCallback;
    }

    public float[] getProjection(){
        return mProjMatrix;
    }

    public void setSkyMap(CubeTexture skyMap){
        mSkyTexture = skyMap;
    }

    public void setSkyBoxEnabled(boolean enable){
        mSkyEnabled = enable;
    }

    public float getZoom(){
        return mZoom;
    }
    public void setZoom(float zoom){
        mZoom = zoom;
        updateProjMatrix();
    }

    private void updateCamBuffer(){
        final Vector<Camera.Light> lights = mCamera.getLights();

        ByteBuffer init = ByteBuffer.allocate(CAM_BYTE_SIZE).order(ByteOrder.nativeOrder());
        for (int i = 0; i < 16; i++) init.putFloat(mProjMatrix[i]);
        init.putInt(lights.size());
        for (int i = 0; i < 3; i++) init.putInt(0);
        for (Camera.Light l : lights){
            final float[] p = new float[4];
            Matrix.multiplyMV(p, 0, mCamera.getTransform().getWorldToLocal(), 0, new float[]{l.pos[0], l.pos[1], l.pos[2], 1}, 0);
            for(int i = 0; i < 3; i++) init.putFloat(p[i]);
            init.putFloat(1.f);
            for(int i = 0; i < 3; i++) init.putFloat(l.col[i]);
            init.putFloat(1.f);
        }
        init.rewind();

        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, mCamBuffer);
        GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, CAM_BUFFER_LOC.bind, mCamBuffer);
        GLES31.glBufferData(GLES31.GL_UNIFORM_BUFFER, CAM_BYTE_SIZE, init, GLES31.GL_DYNAMIC_DRAW);
        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, 0);

        GLHelper.CheckGLError();
    }

    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        GLES31.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES31.glEnable(GLES31.GL_CULL_FACE);
        GLES31.glEnable(GLES31.GL_DEPTH_TEST);
        mAccumTimer = new AccumTimer(500.f, mCamera);
        mParticleTimer = new AccumTimer(1.f, mCamera);

        ArrayList<Pair<Integer, String>> shaders = new ArrayList<>();
        shaders.add(Pair.create(GLES31.GL_VERTEX_SHADER, "merger_vert"));
        shaders.add(Pair.create(GLES31.GL_FRAGMENT_SHADER, "merger_frag"));
        mMerger = IO.loadShaderFromFile(shaders, false);

        shaders.clear();
        shaders.add(Pair.create(GLES31.GL_VERTEX_SHADER, "sky_vert"));
        shaders.add(Pair.create(GLES31.GL_FRAGMENT_SHADER, "sky_frag"));
        mSky = IO.loadShaderFromFile(shaders, false);

        mScreenPlane = MeshGenerator.GetPlaneMesh();

        mSkyBox = MeshGenerator.GetCubeMesh();

        mSkyTexture = IO.loadCubeTextureFromFile(new String[] {
                "right",
                "left",
                "top",
                "bottom",
                "back",
                "front"
        }, false);

        final int[] ubo = new int[1];
        GLES31.glGenBuffers(1, ubo, 0);
        mCamBuffer = ubo[0];

        SolidRenderComponent.init();
        VolumeRenderComponent.init();
        ParticleRenderComponent.init();

        VolumeTexture.init();

        mPhysicEngine = new PhysicEngine();

        if(mSceneUpdateCallback != null) mSceneUpdateCallback.awake();
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int width, int height) {
        GLES31.glViewport(0, 0, width, height);
        updateProjMatrix();

        SolidRenderComponent.resize(width, height);
        VolumeRenderComponent.resize(width, height);
    }

    private void updateProjMatrix(){
        float ratio = (float)mCamera.getWidth() / mCamera.getHeight();
        if(ratio > 1.f){
            ratio = 1.f / ratio;
            Matrix.frustumM(mProjMatrix, 0, -mZoom, mZoom, -ratio * mZoom, ratio * mZoom, 1.f, 100);
        } else {
            Matrix.frustumM(mProjMatrix, 0, -ratio * mZoom, ratio * mZoom, -mZoom, mZoom, 1.f, 100);
        }
    }

    @Override
    public void onDrawFrame(GL10 gl10) {
        if(!mEnabled) return;

        if(mSceneUpdateCallback != null) mSceneUpdateCallback.update(mDt);

        if(TIME_MEASUREMENT) mParticleTimer.start();

        mPhysicEngine.update(mDt);

        if(TIME_MEASUREMENT) mParticleTimer.stopAndTryEval(new AccumTimer.OnTimeAccum() {
            @Override
            public void timeAccum(float accumTime) {
                Log.d("Avg Particle Time", accumTime + " ms");
            }
        });

        if(TIME_MEASUREMENT) mAccumTimer.start();

        GLES31.glClear(GLES31.GL_DEPTH_BUFFER_BIT | GLES31.GL_COLOR_BUFFER_BIT);

        updateCamBuffer();

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[SKY_MAP_LOC.bind]);
        mSkyTexture.bindTexture();

        // render only water + object behind water for refraction effect
        VolumeRenderComponent.bindFramebuffer();
        VolumeRenderComponent.drawAll();
        GLHelper.CheckGLError();

        SolidRenderComponent.bindFramebuffer();
        SolidRenderComponent.drawAll(VolumeRenderComponent.getRendererDepth());
        GLHelper.CheckGLError();

        ParticleRenderComponent.drawAll(VolumeRenderComponent.getRendererDepth());
        GLHelper.CheckGLError();

        GLES31.glBindFramebuffer(GLES31.GL_FRAMEBUFFER, 0);

        mMerger.useProgram();

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[SOLID_COL_LOC.bind]);
        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, SolidRenderComponent.getRendererColor());
        GLES31.glUniform1i(SOLID_COL_LOC.handle, SOLID_COL_LOC.bind);

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[VOLUME_COL_LOC.bind]);
        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, VolumeRenderComponent.getRendererColor());
        GLES31.glUniform1i(VOLUME_COL_LOC.handle, VOLUME_COL_LOC.bind);

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[VOLUME_DEP_LOC.bind]);
        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, VolumeRenderComponent.getRendererDepth());
        GLES31.glUniform1i(VOLUME_DEP_LOC.handle, VOLUME_DEP_LOC.bind);

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[VOLUME_DISP_LOC.bind]);
        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, VolumeRenderComponent.getRendererDisplace());
        GLES31.glUniform1i(VOLUME_DISP_LOC.handle, VOLUME_DISP_LOC.bind);

        mScreenPlane.draw();
        GLHelper.CheckGLError();

        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, 0);

        SolidRenderComponent.drawAll();
        GLHelper.CheckGLError();

        if(mSkyEnabled) {
            GLES31.glDepthFunc(GLES31.GL_LEQUAL);
            GLES31.glDisable(GLES31.GL_CULL_FACE);

            mSky.useProgram();

            GLES31.glUniformMatrix4fv(MV_HANDLE, 1, false, mCamera.getTransform().getWorldToLocal(), 0);

            GLES31.glUniform1i(SKY_MAP_LOC.handle, SKY_MAP_LOC.bind);

            mSkyBox.draw();

            GLES31.glDepthFunc(GLES31.GL_LESS);
            GLES31.glEnable(GLES31.GL_CULL_FACE);
        }

        ParticleRenderComponent.drawAll();
        GLHelper.CheckGLError();

        //ParticleRenderComponent.drawAllFoam();
        GLHelper.CheckGLError();

        if(TIME_MEASUREMENT) mAccumTimer.stopAndTryEval(new AccumTimer.OnTimeAccum() {
            @Override
            public void timeAccum(float accumTime) {
                Log.d("Avg Render Time", accumTime + " ms");
            }
        });

        calcDt();
    }

    private void calcDt()
    {
        long curFrameTimestamp = System.nanoTime();

        if(prevFrameTimestamp > 0)
        {
            // Calculate the current frame duration value.
            long frameDuration = curFrameTimestamp - prevFrameTimestamp;
            mDt = frameDuration / 1e6f;
        }

        prevFrameTimestamp = curFrameTimestamp;
    }
}
