package prantl.de.interactivefluidengine.util;

import android.opengl.GLES31;
import android.opengl.GLU;

/**
 * Created by Lukas on 02.07.17.
 */

public class GLHelper {

    public final static int DISPATCH_GROUP_SIZE = 4;
    public final static int MAX_TEX_SIZE = 512;

    public final static int DISPATCH_GROUP_SIZE_2D = 8;
    public final static int MAX_TEX_SIZE_2D = 1024;

    public final static int[] TEXTURE_BIND_MAP = new int[]{
            GLES31.GL_TEXTURE0,
            GLES31.GL_TEXTURE1,
            GLES31.GL_TEXTURE2,
            GLES31.GL_TEXTURE3,
            GLES31.GL_TEXTURE4,
            GLES31.GL_TEXTURE5,
            GLES31.GL_TEXTURE6,
            GLES31.GL_TEXTURE7,
            GLES31.GL_TEXTURE8,
            GLES31.GL_TEXTURE9,
            GLES31.GL_TEXTURE10
    };
    public static void CheckGLError() {
        int err = GLES31.glGetError();
        if (err != GLES31.GL_NO_ERROR) {
            throw new RuntimeException(GLU.gluErrorString(err));
        }
    }
}
