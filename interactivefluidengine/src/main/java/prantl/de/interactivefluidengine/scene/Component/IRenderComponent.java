package prantl.de.interactivefluidengine.scene.Component;

import android.opengl.GLES31;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import prantl.de.interactivefluidengine.resources.Shader;
import prantl.de.interactivefluidengine.scene.Camera;
import prantl.de.interactivefluidengine.scene.Entity;
import prantl.de.interactivefluidengine.util.GLHelper;

/**
 * Created by Lukas on 30.05.17.
 */

// a rendercomponent for each graphics shader used for rendering
public abstract class IRenderComponent {
    static protected class RenderTargetHandle{
        public int mRenderTarget = -1;
        public int mRenderTargetColor;
        public int mRenderTargetDepth;

        public void resize(int width, int height){
            final int[] rtv = new int[2];
            if(mRenderTarget > 0){
                rtv[0] = mRenderTarget;
                GLES31.glDeleteFramebuffers(1, rtv, 0);
                rtv[0] = mRenderTargetColor;
                rtv[1] = mRenderTargetDepth;
                GLES31.glDeleteTextures(2, rtv, 0);
            }
            GLES31.glGenFramebuffers(1, rtv, 0);
            mRenderTarget = rtv[0];
            GLES31.glBindFramebuffer(GLES31.GL_FRAMEBUFFER, mRenderTarget);

            GLES31.glGenTextures(2, rtv, 0);
            mRenderTargetColor = rtv[0];
            mRenderTargetDepth = rtv[1];
            GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, mRenderTargetColor);
            GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_MAG_FILTER, GLES31.GL_NEAREST);
            GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_MIN_FILTER, GLES31.GL_NEAREST);
            GLES31.glTexStorage2D(GLES31.GL_TEXTURE_2D, 1, GLES31.GL_RGBA8, width, height);

            GLES31.glFramebufferTexture2D(GLES31.GL_FRAMEBUFFER, GLES31.GL_COLOR_ATTACHMENT0, GLES31.GL_TEXTURE_2D, mRenderTargetColor, 0);

            GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, mRenderTargetDepth);
            GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_MAG_FILTER, GLES31.GL_NEAREST);
            GLES31.glTexParameteri(GLES31.GL_TEXTURE_2D, GLES31.GL_TEXTURE_MIN_FILTER, GLES31.GL_NEAREST);
            GLES31.glTexStorage2D(GLES31.GL_TEXTURE_2D, 1, GLES31.GL_DEPTH_COMPONENT24, width, height);

            GLES31.glFramebufferTexture2D(GLES31.GL_FRAMEBUFFER, GLES31.GL_DEPTH_ATTACHMENT, GLES31.GL_TEXTURE_2D, mRenderTargetDepth, 0);

            GLHelper.CheckGLError();

            GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, 0);
            GLES31.glBindFramebuffer(GLES31.GL_FRAMEBUFFER, 0);
        }
    }
    protected Entity mEntity;
    protected final Camera mCamera;

    private boolean mEnabled = true;

    public IRenderComponent(final Camera camera) {
        mCamera = camera;
        getIRenderComponentVector().add(this);
    }
    public void setEntity(Entity entity) { mEntity = entity; }

    public void setEnabled(boolean enabled){ mEnabled = enabled; }
    public boolean isEnabled(){ return mEnabled; }

    protected abstract void draw();
    protected abstract Vector<IRenderComponent> getIRenderComponentVector();
}
