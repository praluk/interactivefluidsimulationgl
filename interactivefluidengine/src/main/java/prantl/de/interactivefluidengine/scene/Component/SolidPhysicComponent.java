package prantl.de.interactivefluidengine.scene.Component;

import java.util.Vector;

/**
 * Created by Lukas on 30.05.17.
 */

public class SolidPhysicComponent extends IPhysicComponent {
    private static Vector<IPhysicComponent> mPhysicComponents = new Vector<>();

    @Override
    public void update(float dt) {

    }

    @Override
    protected Vector<IPhysicComponent> getIPhysicComponentVector() {
        return mPhysicComponents;
    }
}
