package prantl.de.interactivefluidengine.scene.Component;

import android.opengl.GLES31;
import android.opengl.GLES32;
import android.opengl.Matrix;
import android.util.Pair;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Vector;

import prantl.de.interactivefluidengine.RenderEngine;
import prantl.de.interactivefluidengine.resources.Mesh;
import prantl.de.interactivefluidengine.resources.Shader;
import prantl.de.interactivefluidengine.resources.Texture;
import prantl.de.interactivefluidengine.resources.VolumeTexture;
import prantl.de.interactivefluidengine.scene.Camera;
import prantl.de.interactivefluidengine.util.GLHelper;
import prantl.de.interactivefluidengine.util.IO;
import prantl.de.interactivefluidengine.util.Location;
import prantl.de.interactivefluidengine.util.MeshGenerator;

/**
 * Created by Lukas on 30.05.17.
 */

public class SolidRenderComponent extends IRenderComponent {
    // static uniform positions
    private static final int MV_HANDLE = 0;
    private static final int INV_MV_HANDLE = 1;

    private static final Location MATERIAL_BUFFER_LOC = new Location(1, 1);
    private static final Location TEX_LOC = new Location(3, 0);
    private static final Location NORMAL_LOC = new Location(4, 1);
    private static final Location VOLUME_DEPTH_LOC = new Location(5, 2);

    private final Mesh mMesh;
    private Texture mTexture;
    private Texture mNormalTex;

    private static Texture DEFAULT_TEXTURE;
    private static Texture DEFAULT_NORMAL_TEX;

    private static Shader mProgram;
    private static Shader mRefractProgram;

    private static RenderTargetHandle mRenderTargetHandle;

    private final int mMaterial;

    private static final int MATERIAL_ELEM_SIZE = 20;
    private static final int MATERIAL_BYTE_SIZE = 80;

    private static Vector<IRenderComponent> mRenderComponents = new Vector<>();

    public SolidRenderComponent(final Mesh mesh, final Camera camera){
        super(camera);

        mMesh = mesh;

        final int[] ubo = new int[1];
        GLES31.glGenBuffers(1, ubo, 0);
        mMaterial = ubo[0];

        setMaterial(new float[]{1, 1, 1, 0.f}, new float[]{1.f, 1.f, 1.f, .6f}, new float[]{1, 1, 1, 0.3f}, new float[]{1, 1, 1, 0.1f}, 32);
    }

    public static void init(){
        ArrayList<Pair<Integer, String>> shaders = new ArrayList<>();
        shaders.add(Pair.create(GLES31.GL_VERTEX_SHADER, "solid_vert"));
        shaders.add(Pair.create(GLES31.GL_FRAGMENT_SHADER, "solid_frag"));
        mProgram = IO.loadShaderFromFile(shaders, false);

        shaders.set(1, Pair.create(GLES31.GL_FRAGMENT_SHADER, "solid_frag_vol_dep"));
        mRefractProgram = IO.loadShaderFromFile(shaders, false);

        mRenderTargetHandle = new RenderTargetHandle();
        DEFAULT_TEXTURE = IO.loadTextureFromFile("missing", false);
        DEFAULT_NORMAL_TEX = IO.loadTextureFromFile("default_nor", false);

        mRenderComponents.clear();
    }

    public void setTexture(final Texture texture){
        mTexture = texture;
    }
    public void setNormalTexture(final Texture texture) { mNormalTex = texture; }

    public void setMaterial(float[] reflCol, float[] diffCol, float[] specCol, float[] ambientCol, int shinniness){
        ByteBuffer init = ByteBuffer.allocate(MATERIAL_BYTE_SIZE).order(ByteOrder.nativeOrder());
        for(int i = 0; i < 4; i++) init.putFloat(reflCol[i]);
        for(int i = 0; i < 4; i++) init.putFloat(diffCol[i]);
        for(int i = 0; i < 4; i++) init.putFloat(specCol[i]);
        for(int i = 0; i < 4; i++) init.putFloat(ambientCol[i]);
        init.putInt(shinniness);
        init.rewind();

        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, mMaterial);
        GLES31.glBufferData(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BYTE_SIZE, init, GLES31.GL_DYNAMIC_DRAW);
        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, 0);
    }

    public static int getRendererColor(){
        return mRenderTargetHandle.mRenderTargetColor;
    }

    public static int getRendererDepth(){
        return mRenderTargetHandle.mRenderTargetDepth;
    }

    public static void resize(int width, int height){
        mRenderTargetHandle.resize(width, height);
    }

    public static void bindFramebuffer(){
        GLES31.glBindFramebuffer(GLES31.GL_FRAMEBUFFER, mRenderTargetHandle.mRenderTarget);
        GLES31.glClear(GLES31.GL_DEPTH_BUFFER_BIT | GLES31.GL_COLOR_BUFFER_BIT);
    }

    public static void drawAll(int volumeDepthHandle){
        mRefractProgram.useProgram();

        GLES31.glUniform1i(RenderEngine.SKY_MAP_LOC.handle, RenderEngine.SKY_MAP_LOC.bind);
        GLHelper.CheckGLError();

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[VOLUME_DEPTH_LOC.bind]);
        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, volumeDepthHandle);
        GLES31.glUniform1i(VOLUME_DEPTH_LOC.handle, VOLUME_DEPTH_LOC.bind);

        for(IRenderComponent vrc : mRenderComponents){
            if(vrc.isEnabled()) {
                vrc.draw();
            }
        }

        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, 0);
    }

    public static void drawAll(){
        mProgram.useProgram();

        GLES31.glUniform1i(RenderEngine.SKY_MAP_LOC.handle, RenderEngine.SKY_MAP_LOC.bind);
        GLHelper.CheckGLError();

        for(IRenderComponent vrc : mRenderComponents){
            if(vrc.isEnabled()) {
                vrc.draw();
            }
        }
    }

    @Override
    protected void draw(){
        float[] mm = new float[16];
        Matrix.multiplyMM(mm, 0, mCamera.getTransform().getWorldToLocal(), 0, mEntity.getTransform().getLocalToWorld(), 0);
        GLES31.glUniformMatrix4fv(MV_HANDLE, 1, false, mm, 0);

        Matrix.multiplyMM(mm, 0, mEntity.getTransform().getWorldToLocal(), 0, mCamera.getTransform().getLocalToWorld(), 0);
        GLES31.glUniformMatrix4fv(INV_MV_HANDLE, 1, false, mm, 0);

        GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BUFFER_LOC.bind, mMaterial);

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[TEX_LOC.bind]);
        if(mTexture != null){
            mTexture.bindTexture();
        } else {
            DEFAULT_TEXTURE.bindTexture();
        }
        GLES31.glUniform1i(TEX_LOC.handle, TEX_LOC.bind);

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[NORMAL_LOC.bind]);
        if(mNormalTex != null){
            mNormalTex.bindTexture();
        } else {
            DEFAULT_NORMAL_TEX.bindTexture();
        }
        GLES31.glUniform1i(NORMAL_LOC.handle, NORMAL_LOC.bind);

        GLHelper.CheckGLError();

        mMesh.draw();

        GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BUFFER_LOC.bind, 0);
    }

    @Override
    protected Vector<IRenderComponent> getIRenderComponentVector() {
        return mRenderComponents;
    }
}
