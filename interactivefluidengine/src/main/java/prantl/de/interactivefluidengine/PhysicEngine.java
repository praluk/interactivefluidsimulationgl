package prantl.de.interactivefluidengine;

import prantl.de.interactivefluidengine.scene.Component.ParticlePhysicComponent;

/**
 * Created by Lukas on 30.05.17.
 */

public class PhysicEngine {

    public final static float GRAVITY = -2.f;
    private static float mFixedDt = -1.f;

    public PhysicEngine() {
        ParticlePhysicComponent.init();
    }

    public static void setFixedDt(float dt){ mFixedDt = dt; }

    public void update(float dt) {
        dt = mFixedDt > 0 ? mFixedDt : dt;
        ParticlePhysicComponent.updateAll(dt);
    }
}
