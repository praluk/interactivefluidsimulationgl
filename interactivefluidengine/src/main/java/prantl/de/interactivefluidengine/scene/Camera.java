package prantl.de.interactivefluidengine.scene;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.AttributeSet;
import android.util.Log;

import java.util.Vector;

import prantl.de.interactivefluidengine.RenderEngine;
import prantl.de.interactivefluidengine.resources.CubeTexture;
import prantl.de.interactivefluidengine.scene.Component.IRenderComponent;
import prantl.de.interactivefluidengine.scene.Component.Transform;

/**
 * Created by Lukas on 09.06.17.
 */

public class Camera extends GLSurfaceView {
    Transform mTransform;
    RenderEngine mRenderer;

    public final static int MAX_LIGHT_COUNT = 10;

    public class Light{
        public float[] pos = new float[]{0,0,0};
        public float[] col = new float[]{1,1,1};
    }

    final Vector<Light> mLights = new Vector<>();

    public Camera(Context context) {
        super(context);
        init();
    }

    public Camera(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setEnabled(boolean enabled){
        mRenderer.setEnabled(enabled);
    }

    public boolean isEnabled(){
        return mRenderer.isEnabled();
    }

    public void setSkyMap(CubeTexture skyMap){
        mRenderer.setSkyMap(skyMap);
    }

    public void setSkyBoxEnabled(boolean enable){
        mRenderer.setSkyBoxEnabled(enable);
    }

    private void init(){
        setEGLContextClientVersion(3);
        mRenderer = new RenderEngine(this);
        setRenderer(mRenderer);
        mTransform = new Transform();
    }

    public Transform getTransform() { return mTransform; }

    public float[] getProjection() { return mRenderer.getProjection(); }

    public void addLight(float[] pos, float[] col){
        if(mLights.size() < MAX_LIGHT_COUNT - 1) {
            final Light l = new Light();
            l.pos = pos;
            l.col = col;
            mLights.add(l);
        } else {
            Log.e("add lights", "no more lights possible");
        }
    }

    public void setLight(int idx, float[] pos, float[] col){
        mLights.get(idx).pos = pos;
        mLights.get(idx).col = col;
    }

    public float getZoom(){
        return mRenderer.getZoom();
    }
    public void setZoom(float zoom){
        mRenderer.setZoom(zoom);
    }

    public final Vector<Light> getLights() { return mLights; }

    public void setSceneUpdateCallback(RenderEngine.Updateable updateCallback){
        mRenderer.setSceneUpdateCallback(updateCallback);
    }
}
