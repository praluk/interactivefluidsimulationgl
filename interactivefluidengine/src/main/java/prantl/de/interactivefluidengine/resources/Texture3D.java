package prantl.de.interactivefluidengine.resources;

import android.opengl.GLES31;
import android.util.Log;

import java.nio.FloatBuffer;

import prantl.de.interactivefluidengine.scene.Camera;
import prantl.de.interactivefluidengine.util.GLHelper;
import prantl.de.interactivefluidengine.util.IO;

/**
 * Created by Lukas on 09.06.17.
 */

public class Texture3D extends VolumeTexture {
    int[] mDim = new int[3];

    public Texture3D(int textureFormat, int x, int y, int z){
        super(textureFormat);
        mDim[0] = x;
        mDim[1] = y;
        mDim[2] = z;

        bindTexture();
        GLES31.glTexStorage3D(GLES31.GL_TEXTURE_3D, 1, mTextureFormat, mDim[0], mDim[1], mDim[2]);
        GLHelper.CheckGLError();
        unbindTexture();
    }

    public final void fillGrid(float x){
        bindTexture();
        if(isPhiGrid()) {
            FloatBuffer fb = FloatBuffer.allocate(mDim[0] * mDim[1] * mDim[2]);
            for(int i = 0; i < mDim[0] * mDim[1] * mDim[2]; i++) {
                fb.put(x);
            }
            fb.rewind();
            GLES31.glTexSubImage3D(GLES31.GL_TEXTURE_3D, 0, 0, 0, 0, mDim[0], mDim[1], mDim[2], GLES31.GL_RGBA, GLES31.GL_FLOAT, fb);
            GLHelper.CheckGLError();
        } else {
            Log.e("fill grid", "wrong texture!");
        }
        unbindTexture();
    }

    public final void fillGrid(float x, float y, float z, float w) {
        bindTexture();
        if (isVecGrid()) {
            FloatBuffer fb = FloatBuffer.allocate(mDim[0] * mDim[1] * mDim[2] * 4);
            for (int i = 0; i < mDim[0] * mDim[1] * mDim[2]; i++) {
                fb.put(x);
                fb.put(y);
                fb.put(z);
                fb.put(w);
            }
            fb.rewind();
            GLES31.glTexSubImage3D(GLES31.GL_TEXTURE_3D, 0, 0, 0, 0, mDim[0], mDim[1], mDim[2], GLES31.GL_RGBA, GLES31.GL_FLOAT, fb);
            GLHelper.CheckGLError();

        } else {
            Log.e("fill grid", "wrong texture!");
        }
        unbindTexture();
    }

    @Override
    public int[] getDim() {
        return mDim;
    }

    @Override
    public int[] getBoundary() {
        return mBoundary;
    }

    @Override
    public void setBoundary(int[] boundary) {
        mBoundary = boundary;
    }

    @Override
    public void bindTexture() {
        GLES31.glBindTexture(GLES31.GL_TEXTURE_3D, mTextureHandle);
    }

    @Override
    public void unbindTexture() {
        GLES31.glBindTexture(GLES31.GL_TEXTURE_3D, 0);
    }

    @Override
    public float[] getOffset(int t) {
        return new float[]{0,0,0};
    }

    @Override
    public int[] getArraySize() {
        return new int[]{1,1,1};
    }

    @Override
    public int[] getTextureSize() {
        return getDim();
    }
}
