package prantl.de.interactivefluidengine.util;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.opengl.GLES31;
import android.opengl.GLU;
import android.opengl.GLUtils;
import android.os.Environment;
import android.os.Messenger;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.Pair;

import com.android.vending.expansion.zipfile.ZipResourceFile;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import prantl.de.interactivefluidengine.resources.CubeTexture;
import prantl.de.interactivefluidengine.resources.Mesh;
import prantl.de.interactivefluidengine.resources.Shader;
import prantl.de.interactivefluidengine.resources.Texture;
import prantl.de.interactivefluidengine.resources.Texture3D;
import prantl.de.interactivefluidengine.resources.Texture4D;

/**
 * Created by Lukas on 22.09.16.
 */
public class IO {

    private static Context mCt = null;
    private static File mSdcard;
    private static String mResFolder = "";
    private static boolean mZipped = false;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static void setContext(Context ct){
        mCt = ct;
    }

    public static void setResFolder(String resFolder, boolean zipped){
        mResFolder = resFolder;
        mZipped = zipped;
        mSdcard = Environment.getExternalStorageDirectory();
        verifyStoragePermissions();
    }
    public static String getResFolder(){return mResFolder;}

    public static AssetManager getAssets() { return mCt.getAssets(); }

    public static void verifyStoragePermissions() {
        if (ActivityCompat.checkSelfPermission(mCt, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    (Activity)mCt,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public static void awaitPermission(){
        try {
            while (ActivityCompat.checkSelfPermission(mCt, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static InputStream getInputStream(String path, boolean external){
        if(!external) {
            return mCt.getResources().openRawResource(
                    mCt.getResources().getIdentifier("raw/" + path, "raw", mCt.getPackageName()));
        } else {
            try {
                if(mZipped){
                    return new ZipResourceFile(mSdcard + mResFolder).getInputStream(path);
                } else {
                    return new FileInputStream(new File(mSdcard, mResFolder + path));
                }
            } catch(IOException e){
                e.printStackTrace();
                return null;
            }
        }
    }

    private static void getFixedData(byte[] buffer, int size, GZIPInputStream in) throws IOException {
        int l = 0;
        do{
            l += in.read(buffer, l, size - l);
        }while(l < size);
    }

    @Nullable
    public static String loadTextFile(String path, boolean external)
    {
        final InputStream inputStream = getInputStream(path, external);

        if(inputStream == null) return "";

        final InputStreamReader inputStreamReader = new InputStreamReader(
                inputStream);
        final BufferedReader bufferedReader = new BufferedReader(
                inputStreamReader);

        String nextLine;
        final StringBuilder body = new StringBuilder();

        try
        {
            while ((nextLine = bufferedReader.readLine()) != null)
            {
                body.append(nextLine);
                body.append('\n');
            }
        }
        catch (IOException e)
        {
            return null;
        }

        return body.toString();
    }

    public static Shader loadShaderFromFile(final ArrayList<Pair<Integer, String>> shaders, boolean externalSrc){
        ArrayList<Pair<Integer, String>> programs = new ArrayList<>();
        for(Pair<Integer, String> s : shaders) {
            programs.add(Pair.create(s.first, loadTextFile(s.second, externalSrc)));
        }

        return new Shader(programs);
    }

    private static void parseObjCmd(String cmd, StringTokenizer values, ArrayList<Float> v, ArrayList<Float> vt, ArrayList<Float> vn, ArrayList<Float> vertices, ArrayList<Short> indices, HashMap<String, Short> idxMap){
        if(cmd.equals("v")) {
            v.add(Float.parseFloat(values.nextToken()));
            v.add(Float.parseFloat(values.nextToken()));
            v.add(Float.parseFloat(values.nextToken()));
        } else if(cmd.equals("vt")) {
            vt.add(Float.parseFloat(values.nextToken()));
            vt.add(1.f - Float.parseFloat(values.nextToken()));
        } else if(cmd.equals("vn")) {
            vn.add(Float.parseFloat(values.nextToken()));
            vn.add(Float.parseFloat(values.nextToken()));
            vn.add(Float.parseFloat(values.nextToken()));
        } else if(cmd.equals("f")) {
            short[] face = new short[3];
            for(int i = 0; i < face.length; i++) {
                String k = values.nextToken();
                if(idxMap.containsKey(k)){
                    indices.add(idxMap.get(k));
                } else {
                    StringTokenizer idx_tok = new StringTokenizer(k, "/", true);

                    int idx = Integer.parseInt(idx_tok.nextToken()) - 1;
                    vertices.add(v.get(idx * 3));
                    vertices.add(v.get(idx * 3 + 1));
                    vertices.add(v.get(idx * 3 + 2));

                    idx_tok.nextToken();
                    if (idx_tok.hasMoreTokens()) {
                        String str = idx_tok.nextToken();
                        if (!str.equals("/")) {
                            idx = Integer.parseInt(str) - 1;
                            vertices.add(vt.get(idx * 2));
                            vertices.add(vt.get(idx * 2 + 1));
                            idx_tok.nextToken();
                        } else {
                            vertices.add(0.f);
                            vertices.add(0.f);
                        }
                    } else {
                        vertices.add(0.f);
                        vertices.add(0.f);
                    }

                    if (idx_tok.hasMoreTokens()) {
                        idx = Integer.parseInt(idx_tok.nextToken()) - 1;
                        vertices.add(vn.get(idx * 3));
                        vertices.add(vn.get(idx * 3 + 1));
                        vertices.add(vn.get(idx * 3 + 2));
                    } else {
                        vertices.add(0.f);
                        vertices.add(1.f);
                        vertices.add(0.f);
                    }

                    vertices.add(0.f);
                    vertices.add(0.f);
                    vertices.add(0.f);

                    indices.add((short)idxMap.size());
                    idxMap.put(k, (short)idxMap.size());
                }
                face[i] = indices.get(indices.size()-1);
            }

            // calc tangent
            float[] deltaPos0 = new float[3];
            float[] deltaPos1 = new float[3];

            for(int i = 0; i < 3; i++){
                float v0 = vertices.get(face[0] * 11 + i);
                float v1 = vertices.get(face[1] * 11 + i);
                float v2 = vertices.get(face[2] * 11 + i);

                deltaPos0[i] = v1 - v0;
                deltaPos1[i] = v2 - v0;
            }

            float[] deltaUV0 = new float[2];
            float[] deltaUV1 = new float[2];

            for(int i = 0; i < 2; i++){
                float uv0 = vertices.get(face[0] * 11 + i + 3);
                float uv1 = vertices.get(face[1] * 11 + i + 3);
                float uv2 = vertices.get(face[2] * 11 + i + 3);

                deltaUV0[i] = uv1 - uv0;
                deltaUV1[i] = uv2 - uv0;
            }

            for(int i = 0; i < 3; i++){
                float t = (deltaPos0[i] * deltaUV1[1] - deltaPos1[i] * deltaUV0[1]) / (deltaUV0[0] * deltaUV1[1] - deltaUV0[1] * deltaUV1[0]);
                for(int j = 0; j < 3; j++){
                    int idx = face[j] * 11 + i + 8;
                    vertices.set(idx, vertices.get(idx) + t);
                }
            }
        }
    }

    private static Mesh generateNewObj(ArrayList<Float> vertices, ArrayList<Short> indices){

        float[] vArray = new float[vertices.size()];
        int i = 0;

        for (Float f : vertices) {
            vArray[i++] = (f != null ? f : Float.NaN);
        }

        short[] iArray = new short[indices.size()];
        i = 0;

        for(Short s : indices){
            iArray[i++] = s;
        }

        return new Mesh(vArray, iArray);
    }

    public static Mesh loadMeshFromFile(String path, boolean external){
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(getInputStream(path, external)));

            ArrayList<Float> v = new ArrayList<>();
            ArrayList<Float> vt = new ArrayList<>();
            ArrayList<Float> vn = new ArrayList<>();

            ArrayList<Float> vertices = new ArrayList<>();
            ArrayList<Short> indices = new ArrayList<>();

            HashMap<String, Short> idxMap = new HashMap<>();

            while(in.ready()){
                String l = in.readLine();
                if(l == null) break;
                StringTokenizer st = new StringTokenizer(l);
                parseObjCmd(st.nextToken(), st, v, vt, vn, vertices, indices, idxMap);
            }

            return generateNewObj(vertices, indices);

        } catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    public static HashMap<String, Mesh> loadMeshesFromFile(String path, boolean external) {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(getInputStream(path, external)));

            HashMap<String, Mesh> models = new HashMap<>();
            String key = "";

            ArrayList<Float> v = new ArrayList<>();
            ArrayList<Float> vt = new ArrayList<>();
            ArrayList<Float> vn = new ArrayList<>();

            ArrayList<Float> vertices = new ArrayList<>();
            ArrayList<Short> indices = new ArrayList<>();

            HashMap<String, Short> idxMap = new HashMap<>();

            while(in.ready()){
                String l = in.readLine();
                if(l == null) break;

                StringTokenizer st = new StringTokenizer(l);
                String type = st.nextToken();

                if(type.equals("o")){
                    if(!key.isEmpty()){
                        models.put(key, generateNewObj(vertices, indices));

                        vertices.clear();
                        indices.clear();
                        idxMap.clear();
                    }
                    key = st.nextToken();
                } else {
                    parseObjCmd(type, st, v, vt, vn, vertices, indices, idxMap);
                }
            }

            models.put(key, generateNewObj(vertices, indices));

            return models;

        } catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    public static Texture loadTextureFromFile(String path, boolean external){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        Bitmap bitmap;
        if(external){
            bitmap = BitmapFactory.decodeFile(mSdcard + "/" + mResFolder + path, options);
        } else {
            bitmap = BitmapFactory.decodeResource(mCt.getResources(), mCt.getResources().getIdentifier("drawable/" + path, "drawable", mCt.getPackageName()), options);
        }

        Texture tex = new Texture(bitmap);

        // Recycle the bitmap, since its data has been loaded into OpenGL.
        bitmap.recycle();

        return tex;
    }

    public static CubeTexture loadCubeTextureFromFile(String[] path, boolean external){
        if(path.length != 6){
            Log.e("load cube texture", "wrong amount of paths! have to be 6");
            return null;
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        Bitmap[] bitmap = new Bitmap[6];

        for(int i = 0; i < bitmap.length; i++) {
            if (external) {
                bitmap[i] = BitmapFactory.decodeFile(mSdcard + "/" + mResFolder + path[i], options);
            } else {
                bitmap[i] = BitmapFactory.decodeResource(mCt.getResources(), mCt.getResources().getIdentifier("drawable/" + path[i], "drawable", mCt.getPackageName()), options);
            }
        }

        CubeTexture tex = new CubeTexture(bitmap);

        // Recycle the bitmap, since its data has been loaded into OpenGL.
        for(int i = 0; i < bitmap.length; i++) bitmap[i].recycle();

        return tex;
    }

    @Nullable
    public static Texture3D load3DTextureFromFile(String path, boolean external){
        try {
            Texture3D tex;

            GZIPInputStream in = new GZIPInputStream(getInputStream(path, external));

            int bufferSize = 65536;
            byte[] buffer = new byte[bufferSize];
            int l = 0;
            int stride = 1;

            getFixedData(buffer, 4, in);
            String type = new String(buffer, 0, 4, "UTF-8");

            if(!type.equals("MNT2") && !type.equals("MNT3")) {
                Log.e("loadfromfile", "uni format not supported, have to be MNT2 or MNT3!");
                return null;
            }

            //get dim (3x4), gridtype (4), element type (4), bytes per element (4),
            getFixedData(buffer, 24, in);

            final int dim[] = new int[4];
            IntBuffer ib = ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer();
            dim[0] = ib.get();
            dim[1] = ib.get();
            dim[2] = ib.get();
            ib.get();

            if (ib.get() == 2) {
                stride = 4;
                tex = new Texture3D(GLES31.GL_RGBA16F, dim[0], dim[1], dim[2]);
            } else {
                tex = new Texture3D(GLES31.GL_R16F, dim[0], dim[1], dim[2]);
            }

            //skip info (256), timestamp (8)
            if(type.equals("MNT2")) {
                in.skip(264);
            } else {
                // skip also dimt!
                in.skip(268);
            }

            tex.bindTexture();

            Log.i("loadfromfile", "load " + path + " from file " + dim[0] + "x" + dim[1] + "x" + dim[2]);

            FloatBuffer fb = FloatBuffer.allocate(dim[0] * dim[1] * dim[2] * stride);
            while ((l += in.read(buffer, l, bufferSize - l)) > 0) {
                if (l % (4 * stride) == 0) {
                    float[] res = new float[l / 4];
                    ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).asFloatBuffer().get(res, 0, res.length);
                    for(int i = 0; i < res.length; i++){
                        fb.put(res[i]);
                    }
                    l = 0;
                }
            }
            in.close();

            if(tex.isPhiGrid()) {
                GLES31.glTexSubImage3D(GLES31.GL_TEXTURE_3D, 0, 0, 0, 0, dim[0], dim[1], dim[2], GLES31.GL_RED, GLES31.GL_FLOAT, fb);
            } else if(tex.isVecGrid()){
                GLES31.glTexSubImage3D(GLES31.GL_TEXTURE_3D, 0, 0, 0, 0, dim[0], dim[1], dim[2], GLES31.GL_RGBA, GLES31.GL_FLOAT, fb);
            } else {
                Log.e("loadfromfile", "texture format not supported!");
            }

            tex.unbindTexture();

            GLHelper.CheckGLError();

            return tex;

        }catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public static Texture4D load4DTextureFromFile(String path, boolean external){
        try{
            Texture4D tex;

            GZIPInputStream in = new GZIPInputStream(getInputStream(path, external));

            int bufferSize = 65536;
            byte[] buffer = new byte[bufferSize];
            int l = 0;
            int stride = 1;

            getFixedData(buffer, 4, in);
            String type = new String(buffer, 0, 4, "UTF-8");

            if(!type.equals("M4T2") && !type.equals("M4T3")) {
                Log.e("loadfromfile", "uni format not supported, have to be M4T2 or M4T3!");
                return null;
            }

            //get dim (3x4), gridtype (4), element type (4), bytes per element (4),
            getFixedData(buffer, 24, in);

            final int[] dim = new int[4];
            IntBuffer ib = ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer();
            dim[0] = ib.get();
            dim[1] = ib.get();
            dim[2] = ib.get();
            ib.get();

            if (ib.get() == 2) {
                stride = 4;
            }

            if(type.equals("M4T2")) {
                //skip info (256), timestamp (8)
                in.skip(264);
            } else {
                //skip info (252)
                in.skip(252);
            }

            // get fourth factor
            getFixedData(buffer, 4, in);
            ib = ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer();

            dim[3] = ib.get();

            if (stride == 4) {
                tex = new Texture4D(GLES31.GL_RGBA16F, dim[0], dim[1], dim[2], dim[3]);
            } else {
                tex = new Texture4D(GLES31.GL_R16F, dim[0], dim[1], dim[2], dim[3]);
            }

            // timestamp (8)
            if(type.equals("M4T3")){
                in.skip(8);
            }

            final int[] arraySize = tex.getArraySize();

            tex.bindTexture();

            Log.i("loadfromfile", "load " + path + " from file " + dim[0] + "x" + dim[1] + "x" + dim[2] + "x" + dim[3]);

            int xOff = 0;
            int yOff = 0;
            int zOff = 0;
            FloatBuffer fb = FloatBuffer.allocate(dim[0] * dim[1] * dim[2] * stride);
            while ((l += in.read(buffer, l, bufferSize - l)) > 0) {
                if (l % (4 * stride) == 0) {
                    float[] res = new float[l / 4];
                    ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).asFloatBuffer().get(res, 0, res.length);
                    for(int i = 0; i < res.length; i++){
                        fb.put(res[i]);
                        if(fb.position() == fb.limit()){
                            fb.rewind();
                            if(tex.isPhiGrid()) {
                                GLES31.glTexSubImage3D(GLES31.GL_TEXTURE_3D, 0, xOff * dim[0], yOff * dim[1], zOff * dim[2], dim[0], dim[1], dim[2], GLES31.GL_RED, GLES31.GL_FLOAT, fb);
                            } else if(tex.isVecGrid()){
                                GLES31.glTexSubImage3D(GLES31.GL_TEXTURE_3D, 0, xOff * dim[0], yOff * dim[1], zOff * dim[2], dim[0], dim[1], dim[2], GLES31.GL_RGBA, GLES31.GL_FLOAT, fb);
                            } else {
                                Log.e("loadfromfile", "texture format not supported!");
                            }
                            if (++xOff == arraySize[0]) {
                                xOff = 0;
                                if (++yOff == arraySize[1]) {
                                    yOff = 0;
                                    zOff++;
                                }
                            }
                        }
                    }
                    l = 0;
                }
            }
            in.close();

            tex.unbindTexture();

            return tex;

        }catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    public static void storeFrameToFile(int width, int height, String path, boolean external){
        if(!external)
        {
            Log.e("IO saveInFile", "not allowed to write to sd card!");
            return;
        }

        ByteBuffer bb = ByteBuffer.allocate(width * height * 4);
        GLES31.glReadPixels(0, 0, width, height, GLES31.GL_RGBA, GLES31.GL_UNSIGNED_BYTE, bb);
        GLHelper.CheckGLError();

        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bmp.copyPixelsFromBuffer(bb);

        try {
            bmp.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(new File(mSdcard, mResFolder + path)));
        } catch (Exception e) {
            Log.e("store frame", e.getMessage());
        }

        bmp.recycle();
    }

    public static void store4DVecTextureToFile(Texture4D texture, String path, boolean external){
        store4DTextureToFile(texture, 4, path, external);
    }
    public static void store4DTextureToFile(Texture4D texture, String path, boolean external){
        store4DTextureToFile(texture, 1, path, external);
    }

    private static void store4DTextureToFile(Texture4D texture, int stride, String path, boolean external){
        if(!external)
        {
            Log.e("IO saveInFile", "not allowed to write to sd card!");
            return;
        }

        ArrayList<Pair<Integer, String>> shaders = new ArrayList<>();

        shaders.add(Pair.create(GLES31.GL_COMPUTE_SHADER, "tex_to_buff"));
        Shader texToBuff = loadShaderFromFile(shaders, false);

        final int[] dim = texture.getDim();

        try{
            GZIPOutputStream out;

            out = new GZIPOutputStream(new FileOutputStream(new File(mSdcard, mResFolder + path)));

            int bufferSize = 65536;

            ByteBuffer byteBuffer = ByteBuffer.allocate(bufferSize).order(ByteOrder.LITTLE_ENDIAN);

            byteBuffer.put("M4T3".getBytes("UTF-8"));
            byteBuffer.putInt(dim[0]);
            byteBuffer.putInt(dim[1]);
            byteBuffer.putInt(dim[2]);

            if(stride == 4){
                byteBuffer.putInt(8);
                byteBuffer.putInt(2);
            } else {
                byteBuffer.putInt(1);
                byteBuffer.putInt(0);
            }

            byteBuffer.putInt(stride * 4);

            String info = "Android Generated UNI File";

            // padding
            for(int i = info.length(); i < 252; i++){
                info += "\0";
            }

            byteBuffer.put(info.getBytes("UTF-8"));
            byteBuffer.putInt(dim[3]);
            byteBuffer.putLong(System.currentTimeMillis()/1000);

            out.write(byteBuffer.array(), 0, byteBuffer.position());


            texToBuff.useProgram();
            GLES31.glActiveTexture(GLES31.GL_TEXTURE0);
            texture.bindTexture();
            GLES31.glUniform1i(0, 0);

            GLES31.glUniform4i(1, dim[0], dim[1], dim[2], dim[3]);

            final int[] b = new int[1];
            GLES31.glGenBuffers(1, b, 0);
            GLES31.glBindBuffer(GLES31.GL_SHADER_STORAGE_BUFFER, b[0]);
            GLES31.glBufferData(GLES31.GL_SHADER_STORAGE_BUFFER, dim[0] * dim[1] * dim[2] * dim[3] * 4 * 4, null, GLES31.GL_DYNAMIC_READ | GLES31.GL_MAP_READ_BIT);
            GLES31.glBindBuffer(GLES31.GL_SHADER_STORAGE_BUFFER, 0);

            GLES31.glBindBufferBase(GLES31.GL_SHADER_STORAGE_BUFFER, 0, b[0]);

            GLES31.glDispatchCompute(GLMath.roundUp(dim[0] * dim[3], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[1], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[2], GLHelper.DISPATCH_GROUP_SIZE));

            int s = dim[0] * dim[1] * dim[2] * dim[3] * 4 * 4;

            for(int i = 0; i < s; i += bufferSize){
                int l = Math.min(bufferSize, s - i);
                byte[] res = new byte[l*stride/4];

                ByteBuffer texData = ((ByteBuffer) GLES31.glMapBufferRange(GLES31.GL_SHADER_STORAGE_BUFFER, i, l, GLES31.GL_MAP_READ_BIT)).order(ByteOrder.LITTLE_ENDIAN);

                for(int j = 0; j < res.length; j+=4){
                    texData.get(res, j, 4);
                    texData.position(texData.position() + (4 - stride) * 4);
                }
                GLES31.glUnmapBuffer(GLES31.GL_SHADER_STORAGE_BUFFER);

                out.write(res);
            }
            GLES31.glBindBufferBase(GLES31.GL_SHADER_STORAGE_BUFFER, 0, 0);
            texture.unbindTexture();

            GLES31.glDeleteBuffers(1, b, 0);

            GLHelper.CheckGLError();

            out.close();

        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
