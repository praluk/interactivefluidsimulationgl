import tensorflow as tf
from tensorflow.python.tools import freeze_graph
from tensorflow.python.framework import ops

import sys

if len(sys.argv) != 4:
	print("Pass the path to the graph, the checkpoint and the output file!");
	exit()

dc_module = tf.load_op_library('./deconv4d.so')

@ops.RegisterGradient("Deconv4dOp")
def _deconv2d_grad(op, grad):
	#print(grad)
	grad0, grad1 = dc_module.deconv4d_grad(op.inputs[0], op.inputs[1], grad, strides=op.get_attr("strides"))
	return (grad0, grad1)


input_binary = True
input_graph_path = sys.argv[1]

input_saver_def_path = ""

output_graph_path = sys.argv[3]

input_checkpoint_path = sys.argv[2]

output_node_names = "out_param,out_defx"
restore_op_name = ""
initializer_nodes = ""

restore_op_name = "save/restore_all"
filename_tensor_name = "save/Const:0"

clear_devices = False

freeze_graph.freeze_graph(  input_graph_path, input_saver_def_path, input_binary, input_checkpoint_path, output_node_names, restore_op_name, filename_tensor_name, output_graph_path, clear_devices, initializer_nodes)
