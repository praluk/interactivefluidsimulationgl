#version 310 es

precision mediump sampler2D;

#define MAX_TEX_SIZE 1024
#define MAX_LIGHT_COUNT 10

struct LightData{
    vec3 pos;
    vec3 col;
};

layout(location = 0) uniform mat4 mv;

layout(std140, binding = 0) uniform cam_data
{
    mat4 proj;
    int lightCount;
    LightData light[MAX_LIGHT_COUNT];
};

layout(location = 0) in vec4 pos;
layout(location = 1) in vec2 tex_coord;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec3 tangent;

// saved as: vec4(pos.xyz,scale)  (-vec4(vel.xyz,state)...)
layout(binding = 0, location = 2) uniform sampler2D particlePos;
layout(binding = 1, location = 3) uniform sampler2D particleVel;

out vec2 uv;
out vec3 view_pos;
out flat float state;
out flat float scale;

void main() {
    vec2 idx = (vec2((gl_InstanceID) % MAX_TEX_SIZE, (gl_InstanceID) / MAX_TEX_SIZE)+0.5) / vec2(textureSize(particlePos, 0));
    vec4 posScale = texture(particlePos, idx);
    state = texture(particleVel, idx).w;

    scale = state == 1.0 ? posScale.w * 0.5 : posScale.w;

    view_pos = (mv * vec4(posScale.xyz, 1) + vec4(pos.x, -pos.z, 0, 0) * scale).xyz;
    gl_Position = proj * vec4(view_pos, 1);
    uv = tex_coord;
}