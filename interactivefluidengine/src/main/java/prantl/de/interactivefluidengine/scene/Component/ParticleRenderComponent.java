package prantl.de.interactivefluidengine.scene.Component;

import android.opengl.GLES31;
import android.opengl.Matrix;
import android.util.Pair;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Vector;

import prantl.de.interactivefluidengine.RenderEngine;
import prantl.de.interactivefluidengine.resources.Mesh;
import prantl.de.interactivefluidengine.resources.Shader;
import prantl.de.interactivefluidengine.resources.Texture;
import prantl.de.interactivefluidengine.scene.Camera;
import prantl.de.interactivefluidengine.util.GLHelper;
import prantl.de.interactivefluidengine.util.IO;
import prantl.de.interactivefluidengine.util.Location;
import prantl.de.interactivefluidengine.util.MeshGenerator;

/**
 * Created by Lukas on 30.05.17.
 */

public class ParticleRenderComponent extends IRenderComponent {

    private static final int MV_HANDLE = 0;

    private static final Location PARTICLE_POS_LOC = new Location(2, 0);
    private static final Location PARTICLE_VEL_LOC = new Location(3, 1);
    private static final Location VOLUME_DEPTH_LOC = new Location(4, 2);

    private static final Location MATERIAL_BUFFER_LOC = new Location(1, 1);

    private static final int MATERIAL_BYTE_SIZE = 32;

    private static Shader mProgram;
    private static Shader mRefractProgram;
    private static Shader mShadingProgram;

    private Texture mParticlePos;
    private Texture mParticleVel;
    private int mParticleCount;

    private static Mesh mParticleMesh;

    private final int mMaterial;

    private static Vector<IRenderComponent> mRenderComponents = new Vector<>();

    public ParticleRenderComponent(final Texture particlePos, final Texture particleVel, final int particleCount, final Camera camera) {
        super(camera);
        mParticlePos = particlePos;
        mParticleVel = particleVel;
        mParticleCount = particleCount;

        final int[] ubo = new int[1];
        GLES31.glGenBuffers(1, ubo, 0);
        mMaterial = ubo[0];

        setMaterial(new float[]{1.f, 1.f, 1.f, 0.2f}, new float[]{1, 1, 1, 0.1f});
    }

    @Override
    protected Vector<IRenderComponent> getIRenderComponentVector() {
        return mRenderComponents;
    }

    public static void init() {
        ArrayList<Pair<Integer, String>> shaders = new ArrayList<>();
        shaders.add(Pair.create(GLES31.GL_VERTEX_SHADER, "particle_vert"));
        shaders.add(Pair.create(GLES31.GL_FRAGMENT_SHADER, "particle_frag_intens"));
        mProgram = IO.loadShaderFromFile(shaders, false);

        shaders.set(1, Pair.create(GLES31.GL_FRAGMENT_SHADER, "particle_frag_vol_dep"));
        mRefractProgram = IO.loadShaderFromFile(shaders, false);

        shaders.set(1, Pair.create(GLES31.GL_FRAGMENT_SHADER, "particle_frag_col"));
        mShadingProgram = IO.loadShaderFromFile(shaders, false);

        mParticleMesh = MeshGenerator.GetPlaneMesh();

        mRenderComponents.clear();
    }

    public void setMaterial(float[] diffCol, float[] ambientCol) {
        ByteBuffer init = ByteBuffer.allocate(MATERIAL_BYTE_SIZE).order(ByteOrder.nativeOrder());
        for (int i = 0; i < 4; i++) init.putFloat(diffCol[i]);
        for (int i = 0; i < 4; i++) init.putFloat(ambientCol[i]);
        init.rewind();

        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, mMaterial);
        GLES31.glBufferData(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BYTE_SIZE, init, GLES31.GL_DYNAMIC_DRAW);
        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, 0);
    }

    public static void drawAll(int volumeDepthHandle) {
        mRefractProgram.useProgram();

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[VOLUME_DEPTH_LOC.bind]);
        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, volumeDepthHandle);
        GLES31.glUniform1i(VOLUME_DEPTH_LOC.handle, VOLUME_DEPTH_LOC.bind);

        GLES31.glEnable(GLES31.GL_BLEND);
        GLES31.glBlendFunc(GLES31.GL_SRC_ALPHA, GLES31.GL_ONE_MINUS_SRC_ALPHA);
        GLES31.glDepthMask(false);

        for (IRenderComponent rc : mRenderComponents) {
            ParticleRenderComponent prc = (ParticleRenderComponent) rc;
            GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BUFFER_LOC.bind, prc.mMaterial);
            rc.draw();
        }

        GLES31.glEnable(GLES31.GL_DEPTH_TEST);
        GLES31.glDisable(GLES31.GL_BLEND);
        GLES31.glDepthMask(true);

        GLES31.glBindTexture(GLES31.GL_TEXTURE_2D, 0);
    }

    public static void drawAll() {
        mProgram.useProgram();

        GLES31.glEnable(GLES31.GL_BLEND);
        GLES31.glBlendFunc(GLES31.GL_SRC_ALPHA, GLES31.GL_ONE_MINUS_SRC_ALPHA);
        GLES31.glDepthMask(false);

        for (IRenderComponent rc : mRenderComponents) {
            if(rc.isEnabled()) {
                ParticleRenderComponent prc = (ParticleRenderComponent) rc;
                GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BUFFER_LOC.bind, prc.mMaterial);
                rc.draw();
            }
        }

        GLES31.glEnable(GLES31.GL_DEPTH_TEST);
        GLES31.glDisable(GLES31.GL_BLEND);
        GLES31.glDepthMask(true);
    }


    public static void drawAllFoam() {
        mShadingProgram.useProgram();

        GLES31.glEnable(GLES31.GL_BLEND);
        GLES31.glBlendFunc(GLES31.GL_SRC_ALPHA, GLES31.GL_ONE_MINUS_SRC_ALPHA);
        GLES31.glDepthMask(false);

        for (IRenderComponent rc : mRenderComponents) {
            ParticleRenderComponent prc = (ParticleRenderComponent) rc;

            GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BUFFER_LOC.bind, prc.mMaterial);

            prc.draw();

            GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, MATERIAL_BUFFER_LOC.bind, 0);
        }

        GLES31.glEnable(GLES31.GL_DEPTH_TEST);
        GLES31.glDisable(GLES31.GL_BLEND);
        GLES31.glDepthMask(true);
    }

    @Override
    protected void draw() {
        float[] mv = new float[16];
        Matrix.multiplyMM(mv, 0, mCamera.getTransform().getWorldToLocal(), 0, mEntity.getTransform().getLocalToWorld(), 0);
        GLES31.glUniformMatrix4fv(MV_HANDLE, 1, false, mv, 0);

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[PARTICLE_POS_LOC.bind]);
        mParticlePos.bindTexture();

        GLES31.glUniform1i(PARTICLE_POS_LOC.handle, PARTICLE_POS_LOC.bind);

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[PARTICLE_VEL_LOC.bind]);
        mParticleVel.bindTexture();

        GLES31.glUniform1i(PARTICLE_VEL_LOC.handle, PARTICLE_VEL_LOC.bind);

        mParticleMesh.drawInstanced(mParticleCount);

        mParticlePos.unbindTexture();
        mParticleVel.unbindTexture();
    }
}
