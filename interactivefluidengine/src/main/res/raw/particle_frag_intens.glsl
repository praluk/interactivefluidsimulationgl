#version 310 es

precision mediump float;

in vec2 uv;
in vec3 view_pos;
in flat float state;
in flat float scale;

layout(std140, binding = 1) uniform blinn_phong_data
{
    vec4 diffCol;
    vec4 ambientCol;
} material;

out vec4 col;

float falloff(float x, float n, float m){
    return pow((1.0 - pow(x, n)), m);
}

void main() {
    float l = length(uv-0.5);
    if(state < 0.0 || l > 0.5){
        discard;
    }

    float w = sqrt(0.25 - l * l);
    gl_FragDepth = -(view_pos.z + scale * w) / 10.0;

    if(state == 0.0){
        col = vec4(material.diffCol.xyz,falloff(l*2.0, 1.5, 1.0)*material.diffCol.w);
    } else if(state == 1.0){
        col = vec4(material.diffCol.xyz,(1.0 - falloff(l*2.0, 2.0, 1.0))*material.diffCol.w);
    } else {
        col = vec4(material.diffCol.xyz,falloff(l*2.0, 2.25, 1.0) * falloff(2.0 - state, 2.0, 0.4)*material.diffCol.w);
    }
}

