import tensorflow as tf

from tensorflow.python.framework import ops
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import sparse_ops

import numpy as np

dc_module = tf.load_op_library('./deconv4d.so')

my_seed = 0
np.random.seed(my_seed)

@ops.RegisterGradient("Deconv4dOp")
def _deconv2d_grad(op, grad):
    grad0, grad1 = dc_module.deconv4d_grad(op.inputs[0], op.inputs[1], grad, strides=op.get_attr("strides"))
    return (grad0, grad1)

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1, dtype=tf.float32, seed=my_seed)
    #initial = tf.constant(10.0, shape=shape, dtype=tf.float32)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape, dtype=tf.float32)
    return tf.Variable(initial)

graph = tf.Graph()
with graph.as_default():
    alpha = tf.placeholder(tf.float32, [None,3], name="in_param")

    W_fc1 = weight_variable([3,8])
    b_fc1 = bias_variable([8])
    hidden1 = tf.nn.relu(tf.matmul(alpha, W_fc1) + b_fc1)

    W_fc2 = weight_variable([8,8])
    b_fc2 = bias_variable([8])
    hidden2 = tf.nn.relu(tf.matmul(hidden1, W_fc2) + b_fc2)

    W_out = weight_variable([8,3])
    b_out = bias_variable([3])
    param = tf.nn.relu(tf.matmul(hidden2, W_out) + b_out, name="out_param")

    W_fc3 = weight_variable([3,16])
    b_fc3 = bias_variable([16])
    hidden3 = tf.nn.relu(tf.matmul(alpha, W_fc3) + b_fc3, name="l1")

    numFms4 = 32
    W_fc4 = weight_variable([16,numFms4*3**4])
    b_fc4 = bias_variable([numFms4*3**4])
    hidden4_flat = tf.nn.tanh(tf.matmul(hidden3, W_fc4) + b_fc4)
    hidden4 = tf.reshape(hidden4_flat, [-1,3,3,3,3,numFms4], name="l2")

    numFms5 = 16
    W_conv1 = weight_variable([2,2,2,2,numFms5,numFms4])
    hidden5 = dc_module.deconv4d_op(value=hidden4, filter=W_conv1, strides=[1,1,1,1,1,1], name="l3")

    W_conv2 = weight_variable([4,4,4,4,4,numFms5])
    defx = dc_module.deconv4d_op(value=hidden5, filter=W_conv2, strides=[1,2,2,2,2,1], name="out_defx")

    saver = tf.train.Saver(write_version=tf.train.SaverDef.V2)

    sess = tf.Session()

    init = tf.global_variables_initializer()
    sess.run(init)

    #saver.restore(sess, "./training.ckpt-49")
    #print(sess.run(defx, {alpha:[[1.,1.,1.]]}))

    graph_def = graph.as_graph_def()
    tf.train.write_graph(graph_def,'./', "4d_defx_graph.pb", as_text=False)
