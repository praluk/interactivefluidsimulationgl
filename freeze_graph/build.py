import os
import tensorflow as tf
from subprocess import Popen, PIPE

tf_inc = tf.sysconfig.get_include()
tf_lib = tf.sysconfig.get_lib()

cc_file = "deconv4d.cc"
so_file = "deconv4d.so"

#g++ -std=c++11 -shared deconv4d.cc -o deconv4d.so -I/Library/Frameworks/Python.framework/Versions/3.5/lib/python3.5/site-packages/tensorflow/include -fPIC -undefined dynamic_lookup
build_command = ["g++", "-std=c++11",  "-shared", cc_file, "-o", so_file, "-I"+tf_inc, "-fPIC", "-undefined", "dynamic_lookup"]


print(" ".join(build_command) + "\n")

proc = Popen(build_command, stdin=None, stdout=PIPE, stderr=PIPE)

for line in proc.stderr:
	print(line)
