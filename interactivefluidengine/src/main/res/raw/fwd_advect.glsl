#version 310 es

precision mediump float;
precision mediump image3D;
precision mediump sampler3D;

struct unnormalizedDefo{
    ivec4 value;
    int weight;
};

layout(binding = 0, location = 0) uniform sampler3D phi;
layout(binding = 1, location = 1) uniform sampler3D defo;

layout(std430, binding = 0) buffer destBuffer
{
    unnormalizedDefo data[];
} outBuffer;

layout(std140, binding = 1) uniform properties
{
    float alpha;
    float t_off0;
    float t_off1;
    float ratio;
    ivec4 dim;
    ivec4 boundary;
    float fac0;
    float fac1;
};

float prec = 1000.0;

vec3 getOffset(int time, ivec3 aSize){
    vec3 off;
    off.x = float(int(time) % aSize.x);
    off.y = float(int(time) / aSize.x % aSize.y);
    off.z = float(int(time) / (aSize.x * aSize.y) % aSize.z);
    return off;
}

// helper for sampling texture, aSize is the array size of the 3D array
vec4 sampleTexture(sampler3D tex, vec4 pos, ivec3 aSize){
    vec3 offset = getOffset(int(pos.w), aSize) / vec3(aSize);
    vec3 nextOffset = getOffset(int(pos.w) + 1, aSize) / vec3(aSize);
    return texture(tex, pos.xyz + offset) * (1.0 - fract(pos.w)) + texture(tex, pos.xyz + nextOffset) * fract(pos.w);
}

layout (local_size_x = 4, local_size_y = 4, local_size_z = 4) in;
void main() {
    ivec4 index;
    index.x = int(gl_GlobalInvocationID.x) % dim.x;
    index.y = int(gl_GlobalInvocationID.y);
    index.z = int(gl_GlobalInvocationID.z);
    index.w = int(gl_GlobalInvocationID.x) / dim.x;
    if( index.x < dim.x &&
        index.y < dim.y &&
        index.z < dim.z &&
        index.w < dim.w ){
            ivec3 phiTexSize = textureSize(phi, 0);
            ivec3 aSize = phiTexSize / dim.xyz;

            if( index.x < boundary.x || index.x >= dim.x - boundary.x ||
                index.y < boundary.y || index.y >= dim.y - boundary.y ||
                index.z < boundary.z || index.z >= dim.z - boundary.z ||
                index.w < boundary.w || index.w >= dim.w - boundary.w ){

                return;
            }

            // normalized 3d tex position in slice (centered)
            vec4 pos = vec4((vec3(index.xyz) + 0.5) / vec3(phiTexSize), float(index.w) + t_off0);

            vec4 displace = sampleTexture(defo, pos, aSize) * alpha;
            vec4 src_v = sampleTexture(phi, pos, aSize);

            vec4 dst_pos = clamp(vec4(index) - displace, vec4(0), vec4(dim) - 1.0);

            ivec4 offset;
            for(offset.w = 0; offset.w < 2; offset.w++){
                for(offset.z = 0; offset.z < 2; offset.z++){
                    for(offset.y = 0; offset.y < 2; offset.y++){
                        for(offset.x = 0; offset.x < 2; offset.x++){
                            index = clamp(ivec4(dst_pos) + offset, ivec4(0), dim - 1);

                            float w =
                                (1.0 - abs(dst_pos.x - float(index.x))) *
                                (1.0 - abs(dst_pos.y - float(index.y))) *
                                (1.0 - abs(dst_pos.z - float(index.z))) *
                                (1.0 - abs(dst_pos.w - float(index.w))) * prec;

                            int flat_idx = index.x + index.y * dim.x + index.z * dim.x * dim.y + index.w * dim.x * dim.y * dim.z;
                            atomicAdd(outBuffer.data[flat_idx].value.x, int(src_v.x * w));
                            atomicAdd(outBuffer.data[flat_idx].value.y, int(src_v.y * w));
                            atomicAdd(outBuffer.data[flat_idx].value.z, int(src_v.z * w));
                            atomicAdd(outBuffer.data[flat_idx].value.w, int(src_v.w * w));

                            atomicAdd(outBuffer.data[flat_idx].weight, int(w));
                        }
                    }
                }
            }
        }
    }
