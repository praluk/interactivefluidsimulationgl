#version 310 es

precision mediump float;
precision mediump image3D;
precision lowp sampler3D;

layout(binding = 0, location = 0) uniform sampler3D phi0;
layout(binding = 1, location = 1) uniform sampler3D phi1;
layout(rgba16f, binding = 0, location = 3) uniform writeonly image3D res;

layout(std140, binding = 1) uniform properties
{
    float alpha;
    float t_off0;
    float t_off1;
    float ratio;
    ivec4 dim;
    ivec4 boundary;
    float fac0;
    float fac1;
};

vec3 getOffset(int time, ivec3 aSize){
    vec3 off;
    off.x = float(int(time) % aSize.x);
    off.y = float(int(time) / aSize.x % aSize.y);
    off.z = float(int(time) / (aSize.x * aSize.y) % aSize.z);
    return off;
}

// helper for sampling texture, aSize is the array size of the 3D array
vec4 sampleTexture(sampler3D tex, vec4 pos, ivec3 aSize){
    vec3 offset = getOffset(int(pos.w), aSize) / vec3(aSize);
    vec3 nextOffset = getOffset(int(pos.w) + 1, aSize) / vec3(aSize);
    return texture(tex, pos.xyz + offset) * (1.0 - fract(pos.w)) + texture(tex, pos.xyz + nextOffset) * fract(pos.w);
}

layout (local_size_x = 4, local_size_y = 4, local_size_z = 4) in;
void main() {
    ivec4 index;
    index.x = int(gl_GlobalInvocationID.x) % dim.x;
    index.y = int(gl_GlobalInvocationID.y);
    index.z = int(gl_GlobalInvocationID.z);
    index.w = int(gl_GlobalInvocationID.x) / dim.x;
    if( index.x < dim.x &&
        index.y < dim.y &&
        index.z < dim.z &&
        index.w < dim.w ){

        ivec3 phiTexSize = textureSize(phi0, 0);
        ivec3 aSize = phiTexSize / dim.xyz;
        if( index.x < boundary.x || index.x >= dim.x - boundary.x ||
            index.y < boundary.y || index.y >= dim.y - boundary.y ||
            index.z < boundary.z || index.z >= dim.z - boundary.z ||
            index.w < boundary.w || index.w >= dim.w - boundary.w ){

            imageStore(res, index.xyz + ivec3(getOffset(index.w, aSize)) * dim.xyz, vec4(1));
            return;
        }

        vec3 pos = (vec3(index.xyz) + 0.5) / vec3(phiTexSize);

        imageStore(res, index.xyz + ivec3(getOffset(index.w, aSize)) * dim.xyz, sampleTexture(phi0, vec4(pos, float(index.w) + t_off0), aSize) * fac0 + sampleTexture(phi1, vec4(pos, float(index.w) + t_off1), aSize) * fac1);
    }

}
