package prantl.de.interactivefluidengine.resources;

import android.opengl.GLES31;
import android.util.Log;
import android.util.Pair;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import prantl.de.interactivefluidengine.scene.Camera;
import prantl.de.interactivefluidengine.util.GLHelper;
import prantl.de.interactivefluidengine.util.GLMath;
import prantl.de.interactivefluidengine.util.IO;
import prantl.de.interactivefluidengine.util.Location;

/**
 * Created by Lukas on 12.06.17.
 */

class UnnormalizedDeformation{
    public final static int ELEM_SIZE = 8;
    public final static int BYTE_SIZE = 32;
}

public abstract class VolumeTexture implements IResource, ITexture {

    int mTextureHandle;
    int mTextureFormat;
    int mTmpBuffer;

    int mPropBuffer;

    int[] mBoundary = new int[]{0, 0, 0, 0};

    public VolumeTexture(int textureFormat){
        mTextureFormat = textureFormat;

        final int[] t = new int[1];
        GLES31.glGenTextures(1, t, 0);
        mTextureHandle = t[0];

        GLES31.glBindTexture(GLES31.GL_TEXTURE_3D, mTextureHandle);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_3D, GLES31.GL_TEXTURE_MIN_FILTER, GLES31.GL_LINEAR);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_3D, GLES31.GL_TEXTURE_MAG_FILTER, GLES31.GL_LINEAR);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_3D, GLES31.GL_TEXTURE_WRAP_R, GLES31.GL_CLAMP_TO_EDGE);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_3D, GLES31.GL_TEXTURE_WRAP_S, GLES31.GL_CLAMP_TO_EDGE);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_3D, GLES31.GL_TEXTURE_WRAP_T, GLES31.GL_CLAMP_TO_EDGE);
        GLHelper.CheckGLError();

        GLES31.glBindTexture(GLES31.GL_TEXTURE_3D, 0);

        if(mTextureHandle == 0){
            throw new RuntimeException("Error loading texture.");
        }

        mTmpBuffer = 0;

        final int[] ubo = new int[1];
        GLES31.glGenBuffers(1, ubo, 0);
        mPropBuffer = ubo[0];
    }

    public abstract float[] getOffset(final int t);
    public abstract int[] getArraySize();
    public abstract int[] getTextureSize();

    protected static Shader mAdvectProgramHandle;
    protected static Shader mAdvectCorrProgramHandle;
    protected static Shader mAdvectCorrContProgramHandle;
    protected static Shader mForwardAdvectProgramHandle;
    protected static Shader mNormalizeProgramHandle;
    protected static Shader mBlendProgramHandle;
    protected static Shader m3DSliceProgramHandle;

    protected final static Location TEXTURE_LOC = new Location(0, 0);
    protected final static Location DEFORMATION_LOC = new Location(1, 1);
    protected final static Location CORR_IN_LOC = new Location(2, 2);
    protected final static Location RESULT_LOC = new Location(3, 0);
    protected final static Location CORR_OUT_LOC = new Location(4, 1);
    protected final static Location DEST_BUFFER_LOC = new Location(0, 0);

    private static final Location PROP_BUFFER_LOC = new Location(5, 1);
    private static final int PROP_BYTE_SIZE = 64;

    protected void setPropBuffer(float alpha, float tOff0, float tOff1, int[] dim, float ratio, int[] boundary, float fac0, float fac1){
        ByteBuffer init = ByteBuffer.allocate(PROP_BYTE_SIZE).order(ByteOrder.nativeOrder());
        init.putFloat(alpha);
        init.putFloat(tOff0);
        init.putFloat(tOff1);
        init.putFloat(ratio);
        for(int i = 0; i < 4; i++) init.putInt(i < dim.length ? dim[i] : 1);
        for(int i = 0; i < 4; i++) init.putInt(i < boundary.length ? boundary[i] : 0);
        init.putFloat(fac0);
        init.putFloat(fac1);
        init.putFloat(0);
        init.putFloat(0);
        init.rewind();

        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, mPropBuffer);
        GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, PROP_BUFFER_LOC.bind, mPropBuffer);
        GLES31.glBufferData(GLES31.GL_UNIFORM_BUFFER, PROP_BYTE_SIZE, init, GLES31.GL_DYNAMIC_DRAW);
        GLES31.glBindBuffer(GLES31.GL_UNIFORM_BUFFER, 0);

        GLHelper.CheckGLError();
    }

    public static void init(){
        ArrayList<Pair<Integer, String>> shaders = new ArrayList<>();

        shaders.add(Pair.create(GLES31.GL_COMPUTE_SHADER, "advection"));
        mAdvectProgramHandle = IO.loadShaderFromFile(shaders, false);

        shaders.set(0, Pair.create(GLES31.GL_COMPUTE_SHADER, "advect_combine"));
        mAdvectCorrProgramHandle = IO.loadShaderFromFile(shaders, false);

        shaders.set(0, Pair.create(GLES31.GL_COMPUTE_SHADER, "advect_combine_cont"));
        mAdvectCorrContProgramHandle = IO.loadShaderFromFile(shaders, false);

        shaders.set(0, Pair.create(GLES31.GL_COMPUTE_SHADER, "fwd_advect"));
        mForwardAdvectProgramHandle = IO.loadShaderFromFile(shaders, false);

        shaders.set(0, Pair.create(GLES31.GL_COMPUTE_SHADER, "normalize"));
        mNormalizeProgramHandle = IO.loadShaderFromFile(shaders, false);

        shaders.set(0, Pair.create(GLES31.GL_COMPUTE_SHADER, "blend"));
        mBlendProgramHandle = IO.loadShaderFromFile(shaders, false);

        shaders.set(0, Pair.create(GLES31.GL_COMPUTE_SHADER, "slice_3d"));
        m3DSliceProgramHandle = IO.loadShaderFromFile(shaders, false);
    }

    public void destroy(){
        GLES31.glDeleteTextures(1, new int[]{mTextureHandle}, 0);
    }

    @Override
    public int getTextureHandle() { return mTextureHandle; }
    @Override
    public int getTextureFormat() { return mTextureFormat; }

    public boolean isVecGrid(){ return mTextureFormat == GLES31.GL_RGBA16F || mTextureFormat == GLES31.GL_RGBA32F; }
    public boolean isPhiGrid(){ return mTextureFormat == GLES31.GL_R16F || mTextureFormat == GLES31.GL_R32F; }

    public final void advect(VolumeTexture result, VolumeTexture deformation, float alpha){
        advect(result, deformation, alpha, 0);
    }
    public final void advect(VolumeTexture result, VolumeTexture deformation, float alpha, float tOffset){
        advect(result, deformation, alpha, tOffset, -1);
    }
    public final void advect(VolumeTexture result, VolumeTexture deformation, float alpha, float tOffset, int tSize){
        if(!deformation.isVecGrid()){
            Log.e("volumetexture", "you have to pass a vecgrid as deformation for the advection!");
            return;
        }

        mAdvectProgramHandle.useProgram();

        // bind Textures + Images
        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[TEXTURE_LOC.bind]);
        bindTexture();

        GLES31.glUniform1i(TEXTURE_LOC.handle, TEXTURE_LOC.bind);

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[DEFORMATION_LOC.bind]);
        deformation.bindTexture();

        GLES31.glUniform1i(DEFORMATION_LOC.handle, DEFORMATION_LOC.bind);

        GLES31.glBindImageTexture(RESULT_LOC.bind, result.mTextureHandle, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);

        // setup uniform buffer
        int[] dim = getDim();
        tSize = tSize > 0 ? Math.min(result.getDim().length > 3 ? dim[3] : 1, tSize) : (result.getDim().length > 3 ? dim[3] : 1);

        setPropBuffer(alpha, tOffset, 0, dim, (float)deformation.getDim()[0] / dim[0], result.mBoundary, 0, 0);

        // compute
        GLES31.glDispatchCompute(GLMath.roundUp(dim[0] * tSize, GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[1], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[2], GLHelper.DISPATCH_GROUP_SIZE));

        // unbind
        GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, PROP_BUFFER_LOC.bind, 0);

        

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[TEXTURE_LOC.bind]);
        unbindTexture();

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[DEFORMATION_LOC.bind]);
        deformation.unbindTexture();

        GLES31.glBindImageTexture(RESULT_LOC.bind, 0, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);

        GLES31.glMemoryBarrier(GLES31.GL_COMPUTE_SHADER_BIT);
        GLHelper.CheckGLError();
    }

    public final void advectCombine(VolumeTexture result, VolumeTexture deformation, float factor){
        advectCombine(result, deformation, 1.f, factor);
    }
    public final void advectCombine(VolumeTexture result, VolumeTexture deformation, float factor0, float factor1){
        advectCombine(result, deformation, factor0, factor1, 0.f, -1);
    }
    public final void advectCombine(VolumeTexture result, VolumeTexture deformation, float factor0, float factor1, float tOffset, int tSize){
        int[] dim = getDim();
        // TODO: make own shader for this scenario
        Texture4D dummy = new Texture4D(GLES31.GL_RGBA16F, dim[0], dim[1], dim[2], dim[3]);
        advectCombine(result, deformation, null, dummy, factor1, factor0, factor1, tOffset, tSize);
        dummy.destroy();
    }

    public final void advectCombine(VolumeTexture result, VolumeTexture deformation, VolumeTexture corrOut, float factor0, float factor1){
        advectCombine(result, deformation, corrOut, factor0, factor1, 0.f, -1);
    }
    public final void advectCombine(VolumeTexture result, VolumeTexture deformation, VolumeTexture corrOut, float factor0, float factor1, float tOffset, int tSize){
        advectCombine(result, deformation, null, corrOut, 1, factor0, factor1, tOffset, tSize);
    }

    public final void advectCombine(VolumeTexture result, VolumeTexture deformation, VolumeTexture corrIn, VolumeTexture corrOut, float factor){
        advectCombine(result, deformation, corrIn, corrOut, factor, 0.f, -1);
    }
    public final void advectCombine(VolumeTexture result, VolumeTexture deformation, VolumeTexture corrIn, VolumeTexture corrOut, float factor, float tOffset, int tSize){
        advectCombine(result, deformation, corrIn, corrOut, 1.f, 1.f, factor, tOffset, tSize);
    }

    public final void advectCombine(VolumeTexture result, VolumeTexture deformation, VolumeTexture corrIn, VolumeTexture corrOut, float alpha, float factor0, float factor1, float tOffset, int tSize){
        if(!deformation.isVecGrid()){
            Log.e("volumetexture", "you have to pass a vecgrid as deformation for the advection!");
            return;
        }
        if(corrIn != null) {
            mAdvectCorrContProgramHandle.useProgram();

            GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[CORR_IN_LOC.bind]);
            corrIn.bindTexture();
            GLES31.glUniform1i(CORR_IN_LOC.handle, CORR_IN_LOC.bind);
        } else {
            mAdvectCorrProgramHandle.useProgram();
        }

        // bind Textures + Images
        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[TEXTURE_LOC.bind]);
        bindTexture();

        GLES31.glUniform1i(TEXTURE_LOC.handle, TEXTURE_LOC.bind);

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[DEFORMATION_LOC.bind]);
        deformation.bindTexture();

        GLES31.glUniform1i(DEFORMATION_LOC.handle, DEFORMATION_LOC.bind);

        GLES31.glBindImageTexture(RESULT_LOC.bind, result.mTextureHandle, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);
        GLES31.glBindImageTexture(CORR_OUT_LOC.bind, corrOut.mTextureHandle, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);
        GLHelper.CheckGLError();

        // setup uniform buffer
        int[] dim = getDim();
        tSize = tSize > 0 ? Math.min(result.getDim().length > 3 ? dim[3] : 1, tSize) : (result.getDim().length > 3 ? dim[3] : 1);

        setPropBuffer(alpha, tOffset, 0, dim, (float)deformation.getDim()[0] / dim[0], result.mBoundary, factor0, factor1);

        // compute
        GLES31.glDispatchCompute(GLMath.roundUp(dim[0] * tSize, GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[1], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[2], GLHelper.DISPATCH_GROUP_SIZE));

        // unbind
        GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, PROP_BUFFER_LOC.bind, 0);


        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[TEXTURE_LOC.bind]);
        unbindTexture();

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[DEFORMATION_LOC.bind]);
        deformation.unbindTexture();

        if(corrIn != null) {
            GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[CORR_IN_LOC.bind]);
            corrIn.unbindTexture();
        }

        GLES31.glBindImageTexture(RESULT_LOC.bind, 0, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);
        GLES31.glBindImageTexture(CORR_OUT_LOC.bind, 0, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);

        GLES31.glMemoryBarrier(GLES31.GL_COMPUTE_SHADER_BIT);
        GLHelper.CheckGLError();
    }

    public final void forwardAdvect(VolumeTexture result, VolumeTexture deformation){
        forwardAdvect(result, deformation, 0, -1);
    }
    public final void forwardAdvect(VolumeTexture result, VolumeTexture deformation, float tOffset, int tSize){
        forwardAdvect(result, deformation, 1, tOffset, tSize);
    }
    public final void forwardAdvect(VolumeTexture result, VolumeTexture deformation, float alpha, float tOffset, int tSize){
        if(!deformation.isVecGrid()){
            Log.e("volumetexture", "you have to pass a vecgrid as deformation for the advection!");
            return;
        }
        mForwardAdvectProgramHandle.useProgram();

        // bind Textures + Images
        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[TEXTURE_LOC.bind]);
        bindTexture();

        GLES31.glUniform1i(TEXTURE_LOC.handle, 0);

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[DEFORMATION_LOC.bind]);
        deformation.bindTexture();

        GLES31.glUniform1i(DEFORMATION_LOC.handle, 1);

        // setup uniform buffers
        int[] dim = getDim();
        tSize = tSize > 0 ? Math.min(result.getDim().length > 3 ? dim[3] : 1, tSize) : (result.getDim().length > 3 ? dim[3] : 1);

        setPropBuffer(alpha, tOffset, tSize, dim, (float)deformation.getDim()[0] / dim[0], result.mBoundary, 0, 0);

        // bind tmp storage buffer
        if(mTmpBuffer == 0){
            IntBuffer init = IntBuffer.allocate(dim[0] * dim[1] * dim[2] * dim[3] * UnnormalizedDeformation.ELEM_SIZE);
            for(int i = 0; i < init.limit(); i++) init.put(0);
            init.rewind();

            final int[] b = new int[1];
            GLES31.glGenBuffers(1, b, 0);

            mTmpBuffer = b[0];
            GLES31.glBindBuffer(GLES31.GL_SHADER_STORAGE_BUFFER, mTmpBuffer);
            GLES31.glBufferData(GLES31.GL_SHADER_STORAGE_BUFFER, dim[0] * dim[1] * dim[2] * dim[3] * UnnormalizedDeformation.BYTE_SIZE, init, GLES31.GL_STATIC_COPY);
            GLES31.glBindBuffer(GLES31.GL_SHADER_STORAGE_BUFFER, 0);
        }

        GLES31.glBindBufferBase(GLES31.GL_SHADER_STORAGE_BUFFER, DEST_BUFFER_LOC.bind, mTmpBuffer);

        // compute
        GLES31.glDispatchCompute(GLMath.roundUp(dim[0] * tSize, GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[1], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[2], GLHelper.DISPATCH_GROUP_SIZE));

        // unbind
        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[TEXTURE_LOC.bind]);
        unbindTexture();

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[DEFORMATION_LOC.bind]);
        deformation.unbindTexture();

        GLES31.glMemoryBarrier(GLES31.GL_COMPUTE_SHADER_BIT);

        mNormalizeProgramHandle.useProgram();

        // bind Textures + Images
        GLES31.glBindImageTexture(RESULT_LOC.bind, result.mTextureHandle, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);
        GLHelper.CheckGLError();

        // compute
        GLES31.glDispatchCompute(GLMath.roundUp(dim[0] * (dim.length > 3 ? dim[3] : 1), GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[1], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[2], GLHelper.DISPATCH_GROUP_SIZE));

        // unbind
        GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, PROP_BUFFER_LOC.bind, 0);


        GLES31.glBindImageTexture(RESULT_LOC.bind, 0, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);
        GLHelper.CheckGLError();

        GLES31.glBindBufferBase(GLES31.GL_SHADER_STORAGE_BUFFER, DEST_BUFFER_LOC.bind, 0);

        GLES31.glMemoryBarrier(GLES31.GL_COMPUTE_SHADER_BIT);
        GLHelper.CheckGLError();
    }

    public final void blend(VolumeTexture result, VolumeTexture volumeTexture, float factor0){
        blend(result, volumeTexture, factor0, 1 - factor0);
    }
    public final void blend(VolumeTexture result, VolumeTexture volumeTexture, float factor0, float factor1){
        blend(result, volumeTexture, factor0, factor1, 0, 0, -1);
    }
    public final void blend(VolumeTexture result, VolumeTexture volumeTexture, float factor0, float factor1, float tOffset0, float tOffset1, int tSize){
        mBlendProgramHandle.useProgram();

        // bind Textures + Images
        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[TEXTURE_LOC.bind]);
        bindTexture();

        GLES31.glUniform1i(TEXTURE_LOC.handle, TEXTURE_LOC.bind);

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[DEFORMATION_LOC.bind]);
        volumeTexture.bindTexture();

        GLES31.glUniform1i(DEFORMATION_LOC.handle, DEFORMATION_LOC.bind);

        GLES31.glBindImageTexture(RESULT_LOC.bind, result.mTextureHandle, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);
        GLHelper.CheckGLError();

        // setup uniform buffer
        int[] dim = getDim();
        tSize = tSize > 0 ? Math.min(result.getDim().length > 3 ? dim[3] : 1, tSize) : (result.getDim().length > 3 ? dim[3] : 1);

        setPropBuffer(0, tOffset0, tOffset1, dim, 0, result.mBoundary, factor0, factor1);

        // compute
        GLES31.glDispatchCompute(GLMath.roundUp(dim[0] * tSize, GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[1], GLHelper.DISPATCH_GROUP_SIZE), GLMath.roundUp(dim[2], GLHelper.DISPATCH_GROUP_SIZE));

        // unbind
        GLES31.glBindBufferBase(GLES31.GL_UNIFORM_BUFFER, PROP_BUFFER_LOC.bind, 0);


        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[TEXTURE_LOC.bind]);
        unbindTexture();

        GLES31.glActiveTexture(GLHelper.TEXTURE_BIND_MAP[DEFORMATION_LOC.bind]);
        volumeTexture.unbindTexture();

        GLES31.glBindImageTexture(RESULT_LOC.bind, 0, 0, true, 0, GLES31.GL_WRITE_ONLY, GLES31.GL_RGBA16F);

        GLES31.glMemoryBarrier(GLES31.GL_COMPUTE_SHADER_BIT);
        GLHelper.CheckGLError();
    }
}
