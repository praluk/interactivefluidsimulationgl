#version 310 es

precision mediump float;
precision mediump image3D;

layout(rgba16f, binding = 0, location = 3) uniform writeonly image3D res;

struct unnormalizedDefo{
    ivec4 value;
    int weight;
};
layout(std430, binding = 0) buffer unnormBuffer
{
    unnormalizedDefo result[];
} inBuffer;

layout(std140, binding = 1) uniform properties
{
    float alpha;
    float t_off0;
    float t_off1;
    float ratio;
    ivec4 dim;
    ivec4 boundary;
    float fac0;
    float fac1;
};

vec3 getOffset(int time, ivec3 aSize){
    vec3 off;
    off.x = float(int(time) % aSize.x);
    off.y = float(int(time) / aSize.x % aSize.y);
    off.z = float(int(time) / (aSize.x * aSize.y) % aSize.z);
    return off;
}

layout (local_size_x = 4, local_size_y = 4, local_size_z = 4) in;
void main() {
    ivec4 index;
    index.x = int(gl_GlobalInvocationID.x) % dim.x;
    index.y = int(gl_GlobalInvocationID.y);
    index.z = int(gl_GlobalInvocationID.z);
    index.w = int(gl_GlobalInvocationID.x) / dim.x;
    if( index.x < dim.x &&
        index.y < dim.y &&
        index.z < dim.z &&
        index.w < dim.w ){

        ivec3 resTexSize = imageSize(res);
        int flat_idx = index.x + index.y * dim.x + index.z * dim.x * dim.y + index.w * dim.x * dim.y * dim.z;
        unnormalizedDefo bufV = inBuffer.result[flat_idx];

        if(index.w < int(t_off1)){
            if( index.x < boundary.x || index.x >= dim.x - boundary.x ||
                index.y < boundary.y || index.y >= dim.y - boundary.y ||
                index.z < boundary.z || index.z >= dim.z - boundary.z ||
                index.w < boundary.w || index.w >= int(t_off1) - boundary.w ){
                imageStore(res, index.xyz + ivec3(getOffset(index.w, resTexSize / dim.xyz)) * dim.xyz, vec4(0));
                return;
            }

            vec4 v = bufV.weight > 0 ? vec4(bufV.value) / float(bufV.weight) : vec4(0);
            imageStore(res, index.xyz + ivec3(getOffset(index.w, resTexSize / dim.xyz)) * dim.xyz, v);
        }

        // reset buffer!
        bufV.value = ivec4(0);
        bufV.weight = 0;
        inBuffer.result[flat_idx] = bufV;
    }
}
