//
// Created by Lukas Prantl on 05.11.17.
//

#include "gl_timer.h"

JNIEXPORT int JNICALL
Java_prantl_de_interactivefluidengine_util_AccumTimer_createTimeQuery( JNIEnv* env, jobject thiz ){
    GLuint q;
    glGenQueriesEXT(1, &q);

    return q;
}

JNIEXPORT void JNICALL
Java_prantl_de_interactivefluidengine_util_AccumTimer_beginTimeQuery( JNIEnv* env, jobject thiz, int q ){
    glBeginQueryEXT(TIME_ELAPSED_EXT, q);
}

JNIEXPORT void JNICALL
Java_prantl_de_interactivefluidengine_util_AccumTimer_endTimeQuery( JNIEnv* env, jobject thiz, int q ){
    glEndQueryEXT(TIME_ELAPSED_EXT);
}

JNIEXPORT int JNICALL
Java_prantl_de_interactivefluidengine_util_AccumTimer_getTime( JNIEnv* env, jobject thiz, int q ){
    GLuint available;
    glGetQueryObjectuiv((GLuint)q, GL_QUERY_RESULT_AVAILABLE, &available);
    if(available){
        GLuint time;
        glGetQueryObjectuivEXT((GLuint)q, GL_QUERY_RESULT, &time);
        return time;
    }
    return -1;
}

JNIEXPORT void JNICALL
Java_prantl_de_interactivefluidengine_util_AccumTimer_deleteTimeQuery( JNIEnv* env, jobject thiz, int q ){
    glDeleteQueriesEXT(1, (GLuint*)&q);
}
