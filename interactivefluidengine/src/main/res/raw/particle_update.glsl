#version 310 es

#define MAX_TEX_SIZE 1024u
#define SURFACE 0.5

precision mediump float;
precision mediump sampler3D;
precision mediump image2D;

layout(binding = 0, location = 0) uniform sampler3D phi;
layout(rgba16f, binding = 1, location = 1) uniform readonly image2D particlePosIn;
layout(rgba16f, binding = 2, location = 2) uniform readonly image2D particleVelIn;

layout(rgba16f, binding = 3, location = 3) uniform writeonly image2D particlePosOut;
layout(rgba16f, binding = 4, location = 4) uniform writeonly image2D particleVelOut;

layout(std140, binding = 1) uniform properties
{
    uint particleCount;
    float live_step;
    float gravity;
    float buoyancy;
    float acc_damping;
    float speed_factor;
    float curv_thres;
    float curv_range;
    float energy_thres;
    float energy_range;
    int max_count;
    float max_scale;
};

layout(std140, binding = 2) uniform dyn_properties
{
    ivec3 dim;
    float dt;
    ivec3 boundary;
};

layout (local_size_x = 8, local_size_y = 8, local_size_z = 1) in;
void main() {
    if( gl_GlobalInvocationID.x < min(particleCount, MAX_TEX_SIZE) &&
        gl_GlobalInvocationID.y * MAX_TEX_SIZE + gl_GlobalInvocationID.x < particleCount){

        ivec2 idx = ivec2(gl_GlobalInvocationID.xy);

        vec4 posScale = imageLoad(particlePosIn, idx);
        vec4 velState = imageLoad(particleVelIn, idx);

        velState.xyz = clamp(velState.xyz, -1.0, 1.0);
        posScale.xyz += velState.xyz * dt;

        if( velState.w >=  0.0 &&
            posScale.x >= -0.5 && posScale.x <= 0.5 &&
            posScale.y >= -0.5 && posScale.y <= 0.5 &&
            posScale.z >= -0.5 && posScale.z <= 0.5 ){

            float fluid = texture(phi, posScale.xyz+0.5).x;

            if(velState.w > 1.0 || (fluid > -SURFACE && velState.w > 0.0)){
                velState.y = 0.0;
                velState.xz *= 1.0 - acc_damping;

                velState.w += live_step;

                if(velState.w >= 2.0){
                    velState.w = -1.0;
                    posScale.w = 0.0;
                }
            } else if (fluid > 0.0) {
                velState.y += gravity * dt;
                velState.w = 0.0;
            } else {
                velState.y += buoyancy * dt;
                velState.xz *= 1.0 - acc_damping;
                velState.w = 1.0;
            }

            if(velState.w > 1.0){
                float oldV;
                float step = 1.0 / float(textureSize(phi, 0).y);
                if(fluid < 0.0){
                    do {
                        oldV = fluid;
                        posScale.y += step;

                        if(posScale.y >= 0.5){
                            velState.w = -1.0;
                            posScale.w = 0.0;
                            break;
                        }

                        fluid = texture(phi, posScale.xyz+0.5).x;
                    } while(fluid < 0.0 && posScale.y < 0.5);
                    if(oldV != fluid) posScale.y += step * fluid/(oldV - fluid);
                } else {
                    do {
                        oldV = fluid;
                        posScale.y -= step;

                        if(posScale.y <= -0.5){
                            velState.w = -1.0;
                            posScale.w = 0.0;
                            break;
                        }

                        fluid = texture(phi, posScale.xyz+0.5).x;
                    } while(fluid > 0.0);
                    if(oldV != fluid) posScale.y += step * (1.0 + oldV/(fluid - oldV));
                }
            }
        } else {
            velState.w = -1.0;
            posScale.w = 0.0;
        }
        imageStore(particlePosOut, idx, posScale);
        imageStore(particleVelOut, idx, velState);
    }
}