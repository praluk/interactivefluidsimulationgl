package prantl.de.interactivefluidengine.util;

/**
 * Created by Lukas on 29.06.17.
 */

public class Location {
    public int handle;
    public int bind;
    public Location(int h, int b){
        handle = h;
        bind = b;
    }
}
