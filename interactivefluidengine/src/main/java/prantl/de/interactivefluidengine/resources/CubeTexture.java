package prantl.de.interactivefluidengine.resources;

import android.graphics.Bitmap;
import android.opengl.GLES31;
import android.opengl.GLUtils;
import android.util.Log;

import prantl.de.interactivefluidengine.util.GLHelper;

/**
 * Created by Lukas on 06.09.17.
 */

public class CubeTexture implements ITexture {
    int[] mDim = new int[2];
    int mTextureHandle;

    public CubeTexture(Bitmap[] bitmap){
        if(bitmap.length != 6){
            Log.e("cube texture", "wrong amount of images! have to be 6");
            return;
        }

        mDim[0] = bitmap[0].getWidth();
        mDim[1] = bitmap[1].getHeight();

        final int[] t = new int[1];
        GLES31.glGenTextures(1, t, 0);
        mTextureHandle = t[0];

        bindTexture();

        GLES31.glTexParameteri(GLES31.GL_TEXTURE_CUBE_MAP, GLES31.GL_TEXTURE_MIN_FILTER, GLES31.GL_LINEAR);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_CUBE_MAP, GLES31.GL_TEXTURE_MAG_FILTER, GLES31.GL_LINEAR);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_CUBE_MAP, GLES31.GL_TEXTURE_WRAP_R, GLES31.GL_CLAMP_TO_EDGE);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_CUBE_MAP, GLES31.GL_TEXTURE_WRAP_S, GLES31.GL_CLAMP_TO_EDGE);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_CUBE_MAP, GLES31.GL_TEXTURE_WRAP_T, GLES31.GL_CLAMP_TO_EDGE);

        // allocate texture storage
        for(int i = 0; i < 6; i++){
            GLUtils.texImage2D(GLES31.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, bitmap[i], 0);
        }
        GLHelper.CheckGLError();

        unbindTexture();
    }

    public CubeTexture(int textureFormat, int x, int y){
        mDim[0] = x;
        mDim[1] = y;

        final int[] t = new int[1];
        GLES31.glGenTextures(1, t, 0);
        mTextureHandle = t[0];

        bindTexture();

        GLES31.glTexParameteri(GLES31.GL_TEXTURE_CUBE_MAP, GLES31.GL_TEXTURE_MIN_FILTER, GLES31.GL_LINEAR);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_CUBE_MAP, GLES31.GL_TEXTURE_MAG_FILTER, GLES31.GL_LINEAR);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_CUBE_MAP, GLES31.GL_TEXTURE_WRAP_R, GLES31.GL_CLAMP_TO_EDGE);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_CUBE_MAP, GLES31.GL_TEXTURE_WRAP_S, GLES31.GL_CLAMP_TO_EDGE);
        GLES31.glTexParameteri(GLES31.GL_TEXTURE_CUBE_MAP, GLES31.GL_TEXTURE_WRAP_T, GLES31.GL_CLAMP_TO_EDGE);

        // allocate texture storage
        for(int i = 0; i < 6; i++){
            GLES31.glTexStorage2D(GLES31.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 1, textureFormat, mDim[0], mDim[1]);
        }

        GLHelper.CheckGLError();

        unbindTexture();
    }

    @Override
    public int getTextureHandle(){
        return mTextureHandle;
    }

    @Override
    public int getTextureFormat() {
        return 0;
    }

    @Override
    public int[] getDim() {
        return mDim;
    }

    @Override
    public int[] getBoundary() {
        return null;
    }

    @Override
    public void setBoundary(int[] boundary) {

    }

    @Override
    public void bindTexture() {
        GLES31.glBindTexture(GLES31.GL_TEXTURE_CUBE_MAP, mTextureHandle);
    }

    @Override
    public void unbindTexture() {
        GLES31.glBindTexture(GLES31.GL_TEXTURE_CUBE_MAP, 0);
    }
}
