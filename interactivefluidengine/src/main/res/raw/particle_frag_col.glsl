#version 310 es

precision mediump float;

#define MAX_LIGHT_COUNT 10

struct LightData{
    vec3 pos;
    vec3 col;
};

layout(std140, binding = 0) uniform cam_data
{
    mat4 proj;
    int lightCount;
    LightData light[MAX_LIGHT_COUNT];
};

layout(std140, binding = 1) uniform blinn_phong_data
{
    vec4 diffCol;
    vec4 ambientCol;
} material;

in vec2 uv;
in vec3 view_pos;
in flat float state;
in flat float scale;

out vec4 col;

vec4 blinnPhong(vec3 pos, vec3 nor){
    if(state < 0.0) discard;
    vec4 col = vec4(0, 0, 0, 0);
    for(int i = 0; i < lightCount; i++){
        vec3 lightDir = normalize(light[i].pos - pos);
        float diff = clamp(dot(nor, lightDir), 0.0, 1.0);

        col +=
            (vec4(material.diffCol.xyz * diff, 1.0) * material.diffCol.w +
             vec4(material.ambientCol.xyz, 1.0) * material.ambientCol.w) * vec4(light[i].col, 1.0);
    }

	return col;
}

void main() {
    float l = length(uv-0.5);
    if(state <= 1.0 || l > 0.5){
        discard;
    }
    float w = sqrt(0.25 - l * l);
    gl_FragDepth = -(view_pos.z + scale * w) / 10.0;

    col = blinnPhong(view_pos, normalize(vec3(uv,w)));
}
