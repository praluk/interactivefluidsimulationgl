package prantl.de.interactivefluidengine.resources;

/**
 * Created by Lukas on 10.06.17.
 */

public interface ITexture {
    int[] getDim();
    int[] getBoundary();
    int getTextureHandle();
    int getTextureFormat();
    void setBoundary(int[] boundary);
    void bindTexture();
    void unbindTexture();
}
