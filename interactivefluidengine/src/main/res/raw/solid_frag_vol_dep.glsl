#version 310 es

precision mediump float;
precision mediump sampler2D;

#define MAX_LIGHT_COUNT 10
#define EPS 0.1

struct LightData{
    vec3 pos;
    vec3 col;
};

layout(location = 3, binding = 0) uniform sampler2D tex;
layout(location = 4, binding = 1) uniform sampler2D normal_tex;
layout(location = 5, binding = 2) uniform sampler2D vol_depth;

layout(std140, binding = 0) uniform cam_data
{
    mat4 proj;
    int lightCount;
    LightData light[MAX_LIGHT_COUNT];
};

layout(location = 32, binding = 10) uniform samplerCube sky;

layout(std140, binding = 1) uniform blinn_phong_data
{
    vec4 reflCol;
    vec4 diffCol;
    vec4 specCol;
    vec4 ambientCol;
    int shinniness;
} material;

in vec3 view_pos;
in vec3 nor;
in vec2 uv;
in vec3 tang;

layout(location = 0) out vec4 outColor;

vec4 sampleSkyMap(vec3 dir){
    return vec4(texture(sky, dir).rgb, 1.0);
}

vec3 viewNormal(){
    vec3 t = normalize(tang);
    vec3 n = normalize(nor);
    vec3 b = normalize(cross(t, n));

    return mat3(t, b, n) * normalize(texture(normal_tex, uv).rgb * 2.0 - 1.0);
}

vec4 blinnPhong(vec3 pos, vec3 n, vec3 vDir){
    vec4 col = vec4(0, 0, 0, 0);
    vec4 texCol = texture(tex, uv);
    for(int i = 0; i < lightCount && texCol.w > EPS; i++){
        vec3 lightDir = normalize(light[i].pos - pos);

        vec3 refl = reflect(vDir, n);

        float spec = (float(material.shinniness) + 2.0) / 6.2831853072 * pow(clamp(dot(refl, lightDir), 0.0, 1.0), float(material.shinniness));
        float diff = clamp(dot(n, lightDir), 0.0, 1.0);

        col +=
            (vec4(material.reflCol.xyz * sampleSkyMap(refl).xyz, 1.0) * material.reflCol.w +
            vec4(material.diffCol.xyz * diff, 1.0) * material.diffCol.w * texCol +
            vec4(material.specCol.xyz * spec, 1.0) * material.specCol.w +
            vec4(material.ambientCol.xyz, 1.0) * material.ambientCol.w) * vec4(light[i].col, 1.0);
    }

	return col;
}

void main() {
    gl_FragDepth = -view_pos.z / 10.0;
    if(gl_FragDepth < texture(vol_depth, gl_FragCoord.xy/vec2(textureSize(vol_depth,0))).x) discard;
    outColor = blinnPhong(view_pos, viewNormal(), normalize(view_pos));
    if(outColor.w < EPS) discard;
}