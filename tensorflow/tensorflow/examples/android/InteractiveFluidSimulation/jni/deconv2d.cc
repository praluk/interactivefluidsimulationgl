//
//	Implementation of a custom transpose convolution layer. Testing this to see how hard it will be to implement in 4D
//
//	Written by Boris Bonev, 21.10.2016
//	TU Munich
//

//#include "tensorflow/core/framework/tensor_shape.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/framework/op_kernel.h"
//#include "tfkernels.h"
//#include "fileio.h"
//#include "grid.h"

//using tensorflow::string;

using namespace tensorflow;

// to shorten syntax
using shape_inference::DimensionHandle;
using shape_inference::InferenceContext;
using shape_inference::ShapeHandle;

//
//	Custom Deconvolution Operator
//

REGISTER_OP("Deconv2dOp")
.Attr("strides: list(int) = [1, 1, 1, 1]")
.Input("value: float")
.Input("filter: float")
.Output("output: float")
.SetShapeFn([](InferenceContext* c) {
    
    ShapeHandle out = c->UnknownShape();
    c->set_output(0, out);
    return Status::OK();
});

class Deconv2dOp : public OpKernel {
    
public:
    
    explicit Deconv2dOp(OpKernelConstruction* context) : OpKernel(context) {
        // Get the attributes
        OP_REQUIRES_OK(context, context->GetAttr("strides", &strides_));
    }
    
    void Compute(OpKernelContext* context) override {
        
        std::clog << "Starting Deconv2d Compute " << std::endl;
        
        // Grab the input
        const Tensor& value = context->input(0);
        const Tensor& filter = context->input(1);
        
        //OP_REQUIRES(context, TensorShapeUtils::IsVector(output_shape.shape()), errors::InvalidArgument("Deconv2dOp expects a 1-D output_shape vector."));
        
        // Determine dimensions of the input tensor
        const int batch_size = value.dim_size(0);
        const int x_in = value.dim_size(1);
        const int y_in = value.dim_size(2);
        const int features_in = value.dim_size(3);
        
        // Determine the dimensions of the filter kernels
        const int x_filter = filter.dim_size(0);
        const int y_filter = filter.dim_size(1);
        const int filter_channels_out = filter.dim_size(2);
        const int filter_channels_in = filter.dim_size(3);
        
        // Get strides
        const int sx = strides_[1];
        const int sy = strides_[2];
        
        // Get the Eigen tensors with correct dimensions
        auto input = value.shaped<float, 4>({batch_size, x_in, y_in, features_in});
        auto weights = filter.shaped<float, 4>({x_filter, y_filter, filter_channels_out, filter_channels_in});
        
        // Calculate output dimensions
        const int x_out = (x_in - 1)*sx + x_filter;
        const int y_out = (y_in - 1)*sy + y_filter;
        std::clog << "Output dims are " << x_out << "x" << y_out << std::endl;
        
        // Create the output tensor
        Tensor* output_ = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(0, TensorShape({batch_size, x_out, y_out, filter_channels_out}), &output_));
        //std::clog << "here " << std::endl;
        auto output = output_->shaped<float, 4>({batch_size, x_out, y_out, filter_channels_out});
        
        
        // Initialize output tensor
        for (int b = 0; b < batch_size; b++) {
            for (int x = 0; x < x_out; x++) {
                for (int y = 0; y < y_out; y++) {
                    for (int f = 0; f < filter_channels_out; f++) {
                        output(b,x,y,f) = 0;
                    }
                }
            }
        }
        
        int m, n;
        // Iterate over batches
        for (int b = 0; b < batch_size; b++) {
            std::clog << "Processing batch " << b << std::endl;
            // Iterate over input channels
            for (int chi = 0; chi < filter_channels_in; chi++) {
                // Iterate over output channels
                for (int cho = 0; cho < filter_channels_out; cho++) {
                    
                    // Iterate over inputs
                    for (int i = 0; i < x_in; i++) {
                        for (int j = 0; j < y_in; j++) {
                            // Iterate over filter weights
                            for (int k = 0; k < x_filter; k++) {
                                for (int l = 0; l < y_filter; l++) {
                                    m = i*sx + k;
                                    n = j*sy + l;
                                    output(b,m,n,cho) += weights(k,l,cho,chi) * input(b,i,j,chi);
                                }
                            }
                        }
                    }
                    
                }
            }
        }
        }
        
    private:
        
        TensorShape output_shape_;
        std::vector<int32> strides_;
        };
        
        REGISTER_KERNEL_BUILDER(Name("Deconv2dOp").Device(DEVICE_CPU), Deconv2dOp);
        
        //
        //	Calculates the backprop delta of the Custom Deconvolution Operator
        //	this is not the gradient! it is the delta!
        //
        
        REGISTER_OP("Deconv2dGrad")
        .Attr("strides: list(int) = [1, 1, 1, 1]")
        .Input("value: float")
        .Input("filter: float")
        .Input("ini_grad: float")
        .Output("grad_inputs: float")
        .Output("grad_filters: float");
        
        class Deconv2dGrad : public OpKernel {
            
        public:
            
            explicit Deconv2dGrad(OpKernelConstruction* context) : OpKernel(context) {
                // Get the attributes
                OP_REQUIRES_OK(context, context->GetAttr("strides", &strides_));
            }
            
            void Compute(OpKernelContext* context) override {
                
                std::clog << "Starting Deconv2dGrad Compute " << std::endl;
                
                // Grab the input
                const Tensor& value = context->input(0);
                const Tensor& filter = context->input(1);
                // This is used to weigh the gradients??
                // see https://www.tensorflow.org/versions/r0.11/api_docs/python/train.html#gradient-computation
                // tf.gradients uses this.
                const Tensor& ini_grad_ = context->input(2);
                
                // Determine dimensions of the input tensor
                const int batch_size = value.dim_size(0);
                const int x_in = value.dim_size(1);
                const int y_in = value.dim_size(2);
                const int features_in = value.dim_size(3);
                
                // Determine the dimensions of the filter kernels
                const int x_filter = filter.dim_size(0);
                const int y_filter = filter.dim_size(1);
                const int filter_channels_out = filter.dim_size(2);
                const int filter_channels_in = filter.dim_size(3);
                
                // Get strides
                const int sx = strides_[1];
                const int sy = strides_[2];
                
                // Determine the dimensions of the output
                const int x_out = (x_in - 1)*sx + x_filter;
                const int y_out = (y_in - 1)*sy + y_filter;
                std::clog << "Output dims are " << x_out << "x" << y_out << std::endl;
                
                // check the dimensions align
                OP_REQUIRES(context, filter_channels_in==features_in, errors::InvalidArgument("Dimensions don't align'."));
                
                OP_REQUIRES(context, ini_grad_.dim_size(0)==batch_size, errors::InvalidArgument("Dimensions don't align'."));
                OP_REQUIRES(context, ini_grad_.dim_size(1)==x_out, errors::InvalidArgument("Dimensions don't align'."));
                OP_REQUIRES(context, ini_grad_.dim_size(2)==y_out, errors::InvalidArgument("Dimensions don't align'."));
                OP_REQUIRES(context, ini_grad_.dim_size(3)==filter_channels_out, errors::InvalidArgument("Dimensions don't align'."));
                
                // Get the Eigen tensors with correct dimensions
                auto input = value.shaped<float, 4>({batch_size, x_in, y_in, features_in});
                auto weights = filter.shaped<float, 4>({x_filter, y_filter, filter_channels_out, filter_channels_in});
                auto ini_grad = ini_grad_.shaped<float, 4>({batch_size, x_out, y_out, filter_channels_out});
                
                // Create the output tensors
                //std::clog << "Gradient dims are " << x_filter*y_filter*filter_channels_in*filter_channels_out << ", " << batch_size*x_out*y_out*filter_channels_out << std::endl;
                
                Tensor* grad_1_ = NULL;
                Tensor* grad_2_ = NULL;
                OP_REQUIRES_OK(context, context->allocate_output(0, TensorShape({batch_size, x_in, y_in, features_in}), &grad_1_));
                OP_REQUIRES_OK(context, context->allocate_output(1, TensorShape({x_filter, y_filter, filter_channels_out, filter_channels_in}), &grad_2_));
                // wrt Inputs
                auto grad_1 = grad_1_->shaped<float, 4>({batch_size, x_in, y_in, features_in});
                // wrt Filter Weights
                auto grad_2 = grad_2_->shaped<float, 4>({x_filter, y_filter, filter_channels_out, filter_channels_in});
                
                //std::clog << "dim 0 " << grad_2_->dim_size(0) << std::endl;
                //std::clog << "dim 1 " << grad_2_->dim_size(1) << std::endl;
                
                // initialize zeros
                for (int b = 0; b < batch_size; b++) {
                    for (int x = 0; x < x_in; x++) {
                        for (int y = 0; y < y_in; y++) {
                            for (int f = 0; f < features_in; f++) {
                                grad_1(b,x,y,f) = 0;
                            }
                        }
                    }
                }
                for (int x = 0; x < x_filter; x++) {
                    for (int y = 0; y < y_filter; y++) {
                        for (int fi = 0; fi < filter_channels_in; fi++) {
                            for (int fo = 0; fo < filter_channels_out; fo++) {
                                grad_2(x,y,fo,fi) = 0;
                            }
                        }
                    }
                }
                
                // determine weighting
                float sum = 0;
                for (int b = 0; b < batch_size; b++) {
                    for (int x = 0; x < x_out; x++) {
                        for (int y = 0; y < y_out; y++) {
                            for (int f = 0; f < filter_channels_out; f++) {
                                sum += ini_grad(b,x,y,f);
                            }
                        }
                    }
                }
                
                // write the gradient
                int m, n;
                // Iterate over batches
                for (int b = 0; b < batch_size; b++) {
                    std::clog << "Processing batch " << b << std::endl;
                    // Iterate over input channels
                    for (int chi = 0; chi < filter_channels_in; chi++) {
                        // Iterate over output channels
                        for (int cho = 0; cho < filter_channels_out; cho++) {
                            
                            // Iterate over inputs
                            for (int i = 0; i < x_in; i++) {
                                for (int j = 0; j < y_in; j++) {
                                    // Iterate over filter weights
                                    for (int k = 0; k < x_filter; k++) {
                                        for (int l = 0; l < y_filter; l++) {
                                            m = i*sx + k;
                                            n = j*sy + l;
                                            //grad_1(b,i,j,chi) += weights(k,l,chi,cho);
                                            //grad_2(k,l,chi,cho) += input(b,i,j,chi);
                                            //grad_1(b,i,j,chi) += ini_grad(b,m,n,cho)*weights(k,l,cho,chi)/sum;
                                            //grad_2(k,l,cho,chi) += ini_grad(b,m,n,cho)*input(b,i,j,chi)/sum;
                                            grad_1(b,i,j,chi) += ini_grad(b,m,n,cho)*weights(k,l,cho,chi);
                                            grad_2(k,l,cho,chi) += ini_grad(b,m,n,cho)*input(b,i,j,chi);
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                }
            }
            
        private:
            
            std::vector<int32> strides_;
        };
        
        REGISTER_KERNEL_BUILDER(Name("Deconv2dGrad").Device(DEVICE_CPU), Deconv2dGrad);
        
        
        // EOL
